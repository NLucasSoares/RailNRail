package railnrail.gui.simulation.footer.component.detail;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import railnrail.incident.Incident;
import railnrail.incident.rail.RailIncident;
import railnrail.incident.station.StationIncident;
import railnrail.res.img.ImageResource;
import railnrail.simulation.time.Helper;

public class IncidentDetailPanel extends BaseDetailPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -3634508601425409150L;
	
	/**
	 * Panel name
	 */
	public static final String PANEL_NAME = "IncidentDetail";
	
	/**
	 * Title font size
	 */
	private static final float TITLE_FONT_SIZE = 15.0f;
	
	/**
	 * Panel title
	 */
	private JLabel title = new JLabel( );
	
	/**
	 * Progress bar
	 */
	private JProgressBar progressBar;
	
	/**
	 * Progress bar legend
	 */
	private JLabel progressBarLegend = new JLabel( );
	
	/**
	 * Incident focused
	 */
	private Incident incident;
	
	/**
	 * Is activated?
	 */
	private boolean isActivated = true;
	
	/**
	 * Icons
	 */
	private ImageIcon railIcon,
		stationIcon,
		trainIcon;
	
	/**
	 * Center panel
	 */
	private JPanel centerPanel;
	
	/**
	 * Cancel button
	 */
	private JButton removeIncidentButton = new JButton( "Solve incident" );
	
	/**
	 * Build incident detail panel
	 * 
	 * @param detailPanel
	 * 		The parent detail panel
	 * 
	 * @throws IOException
	 */
	public IncidentDetailPanel( DetailPanel detailPanel ) throws IOException
	{
		// Parent constructor
		super( detailPanel );
		
		// Set background
		setBackground( Color.WHITE );
		
		// Set border
		setBorder( BorderFactory.createCompoundBorder( getBorder( ),
				BorderFactory.createEmptyBorder( 10,
						10,
						10,
						10 ) ) );
		
		// Load icons
		railIcon = ImageResource.loadIcon( ImageResource.IMAGE_RAIL_VERTICAL_SMALL );
		stationIcon = ImageResource.loadIcon( ImageResource.IMAGE_STATION );
		trainIcon = ImageResource.loadIcon( ImageResource.IMAGE_TRAIN_ICON );
		
		// Set layout
		setLayout( new BorderLayout( ) );
		
		// Set title font
		title.setFont( title.getFont( ).deriveFont( IncidentDetailPanel.TITLE_FONT_SIZE ) );
		
		// Set title margin
		title.setBorder( BorderFactory.createEmptyBorder( 0,
				0,
				10,
				0 ) );
				
		// Center title
		title.setHorizontalAlignment( SwingConstants.CENTER );
		
		// Center panel
		centerPanel = new JPanel( );
		
		// Set center panel background
		centerPanel.setBackground( Color.WHITE );
		
		// Set center panel layout
		centerPanel.setLayout( new BoxLayout( centerPanel,
				BoxLayout.PAGE_AXIS ) );
		
		// Create progress bar
		progressBar = new JProgressBar( 0,
				100 );
		
		// Set progress bar style
		progressBar.setForeground( Color.BLACK );
		progressBar.setStringPainted( true );
		
		// Add progress bar
		centerPanel.add( progressBar );
		
		// Add progress bar legend
		centerPanel.add( progressBarLegend );
		
		// Set legend color
		progressBarLegend.setForeground( Color.BLACK );
		
		// Center legend
		progressBarLegend.setHorizontalAlignment( SwingConstants.CENTER );
		progressBarLegend.setAlignmentX( Component.CENTER_ALIGNMENT );
		
		// Set progress bar legend margin
		progressBarLegend.setBorder( BorderFactory.createEmptyBorder( 0,
				0,
				10,
				0 ) );
		
		// Add center panel
		add( centerPanel,
				BorderLayout.CENTER );
		
		// Add title
		add( title,
				BorderLayout.NORTH );
	
		// Add button listener
		removeIncidentButton.addActionListener( new ActionListener( )
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				// Remove the incident
				removeIncident( );
			}
		} );
		// Add cancel button
		add( removeIncidentButton,
				BorderLayout.SOUTH );
	}

	/**
	 * Update
	 */
	@Override
	public void update( )
	{
		// Check
		if( incident == null )
			return;
		
		// Set title
		title.setText( incident.toString( ) );
		
		// Set icon
		if( incident instanceof RailIncident )
			title.setIcon( railIcon );
		else if( incident instanceof StationIncident )
			title.setIcon( stationIcon );
		else
			title.setIcon( trainIcon );
		
		// Create formatter
		NumberFormat formatter = new DecimalFormat( "#0" ); 
		
		// Update progress bar
		progressBar.setValue( (int)incident.getResolutionProgression( ) );
		progressBar.setString( Helper.convertSecondToText( incident.getTimeLeft( ) )
				+ " left :: "
				+ formatter.format( incident.getResolutionProgression( ) >= 100 ?
						100
						: incident.getResolutionProgression( ) )
				+ "%" );
		
		// Set legend text
		progressBarLegend.setText( incident.isForceFinish( ) ?
				"The problem is solved earlier than expected"
				: incident.isPassed( ) ?
					"The problem is solved"
					: "Employees are working on the problem..." );
		
		// Enable/Disable the cancel button
		removeIncidentButton.setEnabled( !( incident.isForceFinish( )
				|| incident.isPassed( ) ) );
			
		// Hide/Show
		title.setVisible( isActivated );
		progressBar.setVisible( isActivated );
		progressBarLegend.setVisible( isActivated );
		removeIncidentButton.setVisible( isActivated );
	}
	
	/**
	 * Activate
	 */
	public void activate( )
	{
		isActivated = true;
	}
	
	/**
	 * Deactivate
	 */
	public void deactivate( )
	{
		isActivated = false;
	}
	
	/**
	 * Focus on incident
	 * 
	 * @param incident
	 * 		The incident to focus on
	 */
	public void focus( Incident incident )
	{
		// Save
		this.incident = incident;
	}
	
	/**
	 * Get selected item
	 * 
	 * @return the selected item
	 */
	public Incident getSelectedItem( )
	{
		return incident;
	}
	
	/**
	 * Remove the incident
	 */
	public void removeIncident( )
	{
		// Is an incident occuring?
		if( incident != null )
			// Remove
			incident.finish( );
	}
}
