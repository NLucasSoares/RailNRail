package railnrail.gui.simulation.footer.component.incident;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.List;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import railnrail.gui.simulation.footer.SimulationFooterPanel;
import railnrail.gui.simulation.footer.component.detail.DetailContent;
import railnrail.incident.Incident;

/**
 * Incidents occuring panel
 * 
 * @author Lucas SOARES
 */
public class IncidentListPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 7727546782945581027L;

	/**
	 * Incident list font size
	 */
	private static final float INCIDENT_LIST_TITLE_FONT_SIZE = 24.0f;
	
	/**
	 * The footer panel
	 */
	private SimulationFooterPanel footerPanel;
	
	/**
	 * Incident list
	 */
	private List incidentList = new List( 5,
			false );
	
	/**
	 * Incident list title
	 */
	private JLabel incidentListTitle;
	
	/**
	 * Construct incident gesture panel
	 * 
	 * @param footerPanel
	 * 		The footer panel
	 */
	public IncidentListPanel( SimulationFooterPanel footerPanel )
	{
		// Save
		this.footerPanel = footerPanel;
		
		// Set background color
		setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Set layout
		setLayout( new BorderLayout( ) );
		
		// Set border
		setBorder( BorderFactory.createEmptyBorder( 0,
				0,
				10,
				10 ) );
		
		// Create title
		incidentListTitle = new JLabel( "Incident list" );
		
		// Set title size
		incidentListTitle.setFont( incidentListTitle.getFont( ).deriveFont( IncidentListPanel.INCIDENT_LIST_TITLE_FONT_SIZE ) );
	
		// Set title color
		incidentListTitle.setForeground( Color.BLACK );
		
		// Center title
		incidentListTitle.setHorizontalAlignment( SwingConstants.CENTER );
		
		// Add item selection listener
		incidentList.addItemListener( new ItemListener( )
		{
			@Override
			public void itemStateChanged( ItemEvent ie )
			{
				// Focus on train
				focusOnIncident( (String)ie.getItemSelectable( ).getSelectedObjects( )[ 0 ].toString( ) );
			}
		} );
		
		// Add title
		add( incidentListTitle,
				BorderLayout.NORTH );
		
		// Create center panel
		JPanel centerPanel = new JPanel( );
		
		// Set center panel layout
		centerPanel.setLayout( new BoxLayout( centerPanel,
				BoxLayout.PAGE_AXIS ) );
		
		// Add list to center panel
		centerPanel.add( incidentList );
		
		// Add center panel
		add( centerPanel,
				BorderLayout.CENTER );
	}
	
	/**
	 * Is incident already in list?
	 * 
	 * @param identifier
	 * 		The incident identifier
	 * 
	 * @return if in list
	 */
	private boolean isIncidentAlreadyListed( String identifier )
	{
		// Check
		for( String s : incidentList.getItems( ) )
			if( identifier.equals( s ) )
				return true;
		
		// Can't find
		return false;
	}
	
	/**
	 * Update
	 * 
	 * @param incident
	 *		The incident list
	 */
	public void update( ArrayList<Incident> incident )
	{
		// Empty current incident list?
		if( incident.size( ) == 0 )
		{
			// Remove all incident from list
			incidentList.removeAll( );
			
			// Quit
			return;
		}
		
		// Create array list of identifiers
		ArrayList<String> identifier = new ArrayList<>( );
		
		// Fill the array of identifier
		for( Incident i : incident )
			identifier.add( i.toString( ) );
		
		// Add incident which doesn't exists
		for( String i : identifier )
			// Check if already in list
			if( !isIncidentAlreadyListed( i ) )
				incidentList.add( i );
		
		// Remove incident which are not present anymore
		for( String i : incidentList.getItems( ) )
			if( !identifier.contains( i ) )
				incidentList.remove( i );
	}
	
	/**
	 * Focus on incident
	 * 
	 * @param incident
	 * 		The incident to focus on
	 */
	public void focusOnIncident( String incident )
	{
		footerPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_INCIDENT,
				incident );
	}
}
