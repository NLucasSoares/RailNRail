package railnrail.gui.simulation.footer.component.detail;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;

import railnrail.gui.simulation.footer.SimulationFooterPanel;
import railnrail.incident.IncidentType;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.res.img.ImageResource;
import railnrail.train.Train;

/**
 * Rail detail panel
 * 
 * @author Lucas SOARES
 */
public class RailDetailPanel extends BaseDetailPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 5882251562786505454L;
	
	/**
	 * href for station access
	 */
	private static final String HREF_STATION_ACCESS = "station";
	
	/**
	 * Margin top create incident button
	 */
	private static final int MARGIN_TOP_CREATE_INCIDENT_BUTTON = 5;
	
	/**
	 * Text for incident adding
	 */
	private static final String CREATE_INCIDENT_TEXT = "Create incident";
	
	/**
	 * Margin train state
	 */
	private static final int MARGIN_TRAIN_STATE = 2;
	
	/**
	 * Panel name
	 */
	public static final String PANEL_NAME = "Rail detail";
	
	/**
	 * No train text
	 */
	private static final String NO_TRAIN_TEXT = "No train";
	
	/**
	 * Train detail font size
	 */
	private static final float TRAIN_DETAIL_FONT_SIZE = 18.0f;
	
	/**
	 * Rail being focused on
	 */
	private Rail rail;

	/**
	 * Icons
	 */
	private ImageIcon redLightIcon,
		greenLightIcon;
	
	/**
	 * Content
	 */
	private JEditorPane title = new JEditorPane( "text/html",
			"" );
	
	/**
	 * Train state
	 */
	private JLabel trainState = new JLabel( );

	/**
	 * Incident happening at the moment
	 */
	private JLabel incidentDescription = new JLabel( );
	
	/**
	 * Create incident button
	 */
	private JButton createIncidentButton = new JButton( RailDetailPanel.CREATE_INCIDENT_TEXT );
	
	/**
	 * Is activated?
	 */
	private boolean isActivated = true;
	
	/**
	 * Construct rail detail
	 * 
	 * @param detailPanel
	 * 		The container for every detail panels
	 * 
	 * @throws IOException 
	 */
	public RailDetailPanel( DetailPanel detailPanel ) throws IOException
	{
		// Parent constructor
		super( detailPanel );
		
		// Set background color
		setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Set border
		setBorder( BorderFactory.createCompoundBorder( getBorder( ),
				BorderFactory.createEmptyBorder( 0,
						10,
						0,
						10 ) ) );
		
		// Load icons
		redLightIcon = ImageResource.loadIcon( ImageResource.IMAGE_RED_LIGHT );
		greenLightIcon = ImageResource.loadIcon( ImageResource.IMAGE_GREEN_LIGHT );

		// Set title properties
		title.setEditable( false );
		title.setOpaque( false );
		title.addHyperlinkListener( new HyperlinkListener( )
		{
			@Override
			public void hyperlinkUpdate( HyperlinkEvent event )
			{
				// Focus on station
				if( event.getEventType( ) == EventType.ACTIVATED )
					switch( event.getDescription( ) )
					{
						case RailDetailPanel.HREF_STATION_ACCESS:
							if( rail.getParentSection( ) instanceof Station )
								focusOnStation( (Station)rail.getParentSection( ) );
							break;
							
						default:
							break;
					}
			}
		} );
		
		// Set components properties
		trainState.setBorder( BorderFactory.createEmptyBorder( RailDetailPanel.MARGIN_TRAIN_STATE,
				RailDetailPanel.MARGIN_TRAIN_STATE,
				RailDetailPanel.MARGIN_TRAIN_STATE + RailDetailPanel.MARGIN_TOP_CREATE_INCIDENT_BUTTON,
				RailDetailPanel.MARGIN_TRAIN_STATE ) );
		trainState.setAlignmentX( Component.CENTER_ALIGNMENT );
		trainState.setFont( trainState.getFont( ).deriveFont( RailDetailPanel.TRAIN_DETAIL_FONT_SIZE ) );
		createIncidentButton.setAlignmentX( Component.CENTER_ALIGNMENT );
		
		// Add title
		add( title,
				BorderLayout.NORTH );
		
		// Create subpanel for center components
		JPanel centerPanel = new JPanel( );
		
		// Set layout for center panel
		centerPanel.setLayout( new BoxLayout( centerPanel,
				BoxLayout.PAGE_AXIS ) );

		// Add button listener
		createIncidentButton.addActionListener( new ActionListener( )
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				createIncident( );
			}
		} );
		
		// Add component to center
		centerPanel.add( trainState );
		centerPanel.add( incidentDescription );
		
		// Set center panel background color
		centerPanel.setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Add create incident button
		add( createIncidentButton,
				BorderLayout.SOUTH );
		
		// Add panel
		add( centerPanel,
				BorderLayout.CENTER );
	}
	
	/**
	 * Focus on new rail
	 * 
	 * @param rail
	 * 		The rail to focus on
	 */
	public void focus( Rail rail )
	{
		// Save
		this.rail = rail;
		
		// Content text
		StringBuffer titleText = new StringBuffer( );
		
		// Div balise
		titleText.append( "<div style='text-align: center; font-size: " );
		titleText.append( BaseDetailPanel.FONT_SIZE_TITLE );
		titleText.append( "pt;'>" );
		
		// Set title
		if( rail.getParentSection( ) instanceof Station )
		{
			titleText.append( "<img src=\"" );
			titleText.append( ImageResource.getImageURL( ImageResource.IMAGE_RAIL_VERTICAL_SMALL  ) );
			titleText.append( "\" />&nbsp;" );
			titleText.append( "<span style='font-family: Dialog, Arial, sans-serif;'>Rail " );
			titleText.append( rail.getID( ) );
			titleText.append( "&nbsp;(<img src='" );
			titleText.append( ImageResource.getImageURL( rail.getDirection( ) == RailDirection.RAIL_DIRECTION_ARRIVAL ?
					ImageResource.IMAGE_LEFT
					: ImageResource.IMAGE_RIGHT ).toString( ) );
			titleText.append( "' />) - <a style='color: 0xFFFFFFFF;' href='" );
			titleText.append( RailDetailPanel.HREF_STATION_ACCESS );
			titleText.append( "'>" );
			titleText.append( rail.getParentSection( ).getName( ) );
			titleText.append( "</a></span>" );
		}
		else
		{
			titleText.append( "<span style='font-family: Dialog, Arial, sans-serif;'>Rail " );
			titleText.append( rail.getID( ) );
			titleText.append( "&nbsp;(<img src='" );
			titleText.append( ImageResource.getImageURL( rail.getDirection( ) == RailDirection.RAIL_DIRECTION_ARRIVAL ?
					ImageResource.IMAGE_LEFT
					: ImageResource.IMAGE_RIGHT ).toString( ) );
			titleText.append( "' />) - " );
			titleText.append( rail.getParentSection( ).getName( ) );
			titleText.append( "</span>" );
		}
		
		// Close div
		titleText.append( "</div>" );
		
		// Set content text
		title.setText( titleText.toString( ) );
	}
	
	/**
	 * Change focus on station
	 * 
	 * @param stationName
	 * 		The station name
	 */
	public void focusOnStation( Station station )
	{
		// Change content
		detailPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_STATION,
				station );
	}
	
	/**
	 * Focus on train
	 * 
	 * @param train
	 * 		The train to focus
	 */
	public void focusOnTrain( Train train )
	{
		// Change content
		detailPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_TRAIN,
				train );
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		if( rail != null )
		{
			// Set icon
			trainState.setIcon( rail.isTrainPresent( ) ?
					redLightIcon
					: greenLightIcon );
			
			// Text
			String text = "";
			
			// Create text
			if( rail.isTrainPresent( ) )
				text = "<html><u>"
						+ rail.getTrain( ).toString( )
						+ "</u></html>";
			else
				text = RailDetailPanel.NO_TRAIN_TEXT;
			
			// Set text
			trainState.setText( text );
			trainState.setAlignmentX( Component.CENTER_ALIGNMENT );
			trainState.setHorizontalAlignment( SwingConstants.CENTER );
			
			// Set clickable
			if( rail.isTrainPresent( ) )
				trainState.addMouseListener( new MouseListener( )
				{
					
					@Override
					public void mouseReleased( MouseEvent e )
					{
					}
					
					@Override
					public void mousePressed( MouseEvent e )
					{
					}
					
					@Override
					public void mouseExited( MouseEvent e )
					{
						// Set hand cursor
						trainState.setCursor( new Cursor( Cursor.DEFAULT_CURSOR ) );
					}
					
					@Override
					public void mouseEntered( MouseEvent e )
					{
						// Set hand cursor
						trainState.setCursor( new Cursor( Cursor.HAND_CURSOR ) );
					}
					
					@Override
					public void mouseClicked( MouseEvent e )
					{
						// Get train
						Train train = rail.getTrain( );
						
						// Train present
						if( train != null )
							// Focus
							focusOnTrain( train );
					}
				} );
			else
			{
				// Remove mouse listener
				for( MouseListener ml : trainState.getMouseListeners( ) )
					trainState.removeMouseListener( ml );
				
				// Restore cursor
				trainState.setCursor( new Cursor( Cursor.DEFAULT_CURSOR ) );
			}
			
			// Enable/disable create incident button
			createIncidentButton.setEnabled( !rail.isIncidentOccuring( ) );
		}
		
		// Show/Hide
		title.setVisible( isActivated );
		trainState.setVisible( isActivated );
		incidentDescription.setVisible( isActivated );
		createIncidentButton.setVisible( isActivated );
	}

	/**
	 * Get selected item
	 * 
	 * @return the selected item
	 */
	public Object getSelectedItem( )
	{
		return rail;
	}

	/**
	 * Activate
	 */
	public void activate( )
	{
		isActivated = true;
	}
	
	/**
	 * Deactivate
	 */
	public void deactivate( )
	{
		isActivated = false;
	}
	
	/**
	 * Create incident
	 */
	public void createIncident( )
	{
		// Rail not null
		if( rail != null )
			// No incident occuring
			if( !rail.isIncidentOccuring( ) )
				// Ask for incident details
				detailPanel.getIncidentBuilder( ).createIncident( IncidentType.RAIL_INCIDENT,
						rail );
	}
}
