package railnrail.gui.simulation.footer.component.detail;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import railnrail.gui.simulation.footer.SimulationFooterPanel;
import railnrail.gui.simulation.footer.component.detail.prevision.StationPrevisionPanel;
import railnrail.incident.IncidentType;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.res.img.ImageResource;
import railnrail.train.Train;

/**
 * Station details panel
 * 
 * @author Lucas SOARES
 */
public class StationDetailPanel extends BaseDetailPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 8867981378178720709L;
	
	/**
	 * Panel name
	 */
	public static final String PANEL_NAME = "Station detail";
	
	/**
	 * Create incident text
	 */
	private static final String CREATE_INCIDENT_TEXT = "Create incident";
	
	/**
	 * Title font size
	 */
	private static final float TITLE_FONT_SIZE = 35.0f;
	
	/**
	 * Margin between rail details
	 */
	private static final int MARGIN_BETWEEN_RAIL_DETAIL = 2;
	
	/**
	 * Station margin border
	 */
	private static final int BORDER_MARGIN_DETAILS = 15;
	
	/**
	 * Margin between content and create button
	 */
	private static final int BORDER_BETWEEN_CONTENT_AND_CREATE_BUTTON = 10;
	
	/**
	 * The station being focused
	 */
	private Station station;
	
	/**
	 * Is activated?
	 */
	private boolean isActivated;
	
	/**
	 * Title
	 */
	private JLabel title = new JLabel( );
	
	/**
	 * Rails detail text
	 */
	private JLabel[ ] railDetailText = null;
	
	/**
	 * Create incident button
	 */
	private JButton createIncidentButton = new JButton( StationDetailPanel.CREATE_INCIDENT_TEXT );
	
	/**
	 * Right panel, details about rails in station
	 */
	private JPanel rightPanel;
	
	/**
	 * Detail panel
	 */
	private DetailPanel detailPanel;
	
	/**
	 * Station prevision panel
	 */
	private StationPrevisionPanel stationPrevisionPanel;
	
	/**
	 * Construct station detail panel
	 * 
	 * @param detailPanel
	 * 		The container for every detail panels
	 * 
	 * @throws IOException
	 */
	public StationDetailPanel( DetailPanel detailPanel ) throws IOException
	{
		// Parent constructor
		super( detailPanel );

		// Add title
		add( title,
				BorderLayout.NORTH );
		
		// Set background color
		setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Center components
		title.setHorizontalAlignment( SwingConstants.CENTER );
		createIncidentButton.setAlignmentX( Component.CENTER_ALIGNMENT );
		
		// Set tile font
		title.setFont( title.getFont( ).deriveFont( StationDetailPanel.TITLE_FONT_SIZE ) );
		title.setForeground( Color.BLACK );
		
		// Create center panel
		JPanel leftPanel = new JPanel( );
		rightPanel = new JPanel( );
		
		// Set layout
		leftPanel.setLayout( new BorderLayout( ) );
		rightPanel.setLayout( new BoxLayout( rightPanel,
				BoxLayout.PAGE_AXIS ) );
		
		// Set border
		setBorder( BorderFactory.createCompoundBorder( getBorder( ),
				BorderFactory.createEmptyBorder( 0,
						StationDetailPanel.BORDER_MARGIN_DETAILS,
						0,
						StationDetailPanel.BORDER_MARGIN_DETAILS ) ) );
		rightPanel.setBorder( BorderFactory.createEmptyBorder( 0,
				0,
				StationDetailPanel.BORDER_BETWEEN_CONTENT_AND_CREATE_BUTTON,
				0 ) );
		leftPanel.setBorder( BorderFactory.createEmptyBorder( 0,
				0,
				StationDetailPanel.BORDER_BETWEEN_CONTENT_AND_CREATE_BUTTON,
				20 ) );
		
		// Add button listener
		createIncidentButton.addActionListener( new ActionListener( )
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				createIncident( );
			}
		} );
		
		// Create station prevision panel
		stationPrevisionPanel = new StationPrevisionPanel( detailPanel );
		
		// Add component to left panel
		leftPanel.add( stationPrevisionPanel,
				BorderLayout.CENTER );

		// Add incident create button
		add( createIncidentButton,
				BorderLayout.SOUTH );
		
		// Set panels background color
		leftPanel.setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		rightPanel.setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Add panel
		add( leftPanel,
				BorderLayout.CENTER );
		add( rightPanel,
				BorderLayout.EAST );
		
		// Save
		this.detailPanel = detailPanel;
	}
	
	/**
	 * Focus on new station
	 * 
	 * @param station
	 * 		The station to focus on
	 */
	public void focus( Station station )
	{
		// Save
		this.station = station;
		
		// Remove old rails details
		if( railDetailText != null )
			for( int i = 0; i < railDetailText.length; i++ )
				rightPanel.remove( railDetailText[ i ] );
		
		// Set title
		title.setText( station.getName( ) );

		// Add rail details
			// Allocate
				railDetailText = new JLabel[ station.getRailCount( ) ];
			// Fill
				int i = 0;
				for( RailDirection direction : RailDirection.values( ) )
					for( int j = 0; j < station.getRailCount( direction ); j++ )
					{
						// Create label
						railDetailText[ i ] = new JLabel( );

						// Set font
						railDetailText[ i ].setFont( railDetailText[ i ].getFont( ).deriveFont( Font.BOLD,
								15.0f ) );
						
						// Set border
						railDetailText[ i ].setBorder( BorderFactory.createEmptyBorder( StationDetailPanel.MARGIN_BETWEEN_RAIL_DETAIL,
							0,
							StationDetailPanel.MARGIN_BETWEEN_RAIL_DETAIL,
							0 ) );
						
						// Add mouse listener
						final int tmp = i;
						railDetailText[ i ].addMouseListener( new MouseListener( )
						{
							/**
							 * Rail
							 */
							private Rail rail;
							
							@Override
							public void mouseReleased( MouseEvent e )
							{
							}
							
							@Override
							public void mousePressed( MouseEvent e )
							{
							}
							
							@Override
							public void mouseExited( MouseEvent e )
							{
								// Set normal cursor
								railDetailText[ tmp ].setCursor( new Cursor( Cursor.DEFAULT_CURSOR ) );
							}
							
							@Override
							public void mouseEntered( MouseEvent e )
							{
								// Set hand cursor
								railDetailText[ tmp ].setCursor( new Cursor( Cursor.HAND_CURSOR ) );
							}
							
							@Override
							public void mouseClicked( MouseEvent e )
							{
								// Focus rail
								focusOnRail( rail );
							}
							
							/**
							 * Set rail
							 * 
							 * @param rail
							 * 		The rail
							 * 
							 * @return the instance of Mouse Listener
							 */
							public MouseListener setRail( Rail rail )
							{
								// Save
								this.rail = rail;
								
								// OK
								return this;
							}
						}.setRail( station.getRail( direction,
								j ) ) );
					
						// Add
						rightPanel.add( railDetailText[ i ] );
						
						// Next label
						i++;
					}
				
		// Pass the focus
		stationPrevisionPanel.focusOnStation( station );
	}
	
	/**
	 * Focus on rail
	 * 
	 * @param rail
	 * 		The rail to be focused
	 */
	public void focusOnRail( Rail rail )
	{
		// Focus
		detailPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_RAIL,
				rail );
	}
	
	/**
	 * Update
	 */
	public void update(  )
	{
		// Save station
		Station s = station;
		
		// Station focused?
		if( s != null )
		{
			// Activated?
			if( isActivated )
			{
				int i = 0;
				
				// Iterate rails
				for( RailDirection direction : RailDirection.values( ) )
				{
					for( int j = 0; j < s.getRailCount( direction ); j++ )
					{
						// Check for focus change
						if( i < railDetailText.length )
						{
							// Rail
							Rail rail = s.getRail( direction,
									j );
							
							// Train
							Train train = rail.getTrain( );
							
							// Create label
							railDetailText[ i ].setText( "<html><img src=\""
								+ ImageResource.getImageURL( ImageResource.IMAGE_RAIL_VERTICAL_SMALL )
								+ "\" />&nbsp;<u>Rail "
								+ rail.getID( )
								+ "</u>&nbsp;(<img src=\""
								+ ( direction == RailDirection.RAIL_DIRECTION_DEPARTURE ?
										ImageResource.getImageURL( ImageResource.IMAGE_RIGHT )
										: ImageResource.getImageURL( ImageResource.IMAGE_LEFT ) )
								+ "\" />)&nbsp;:&nbsp;"
								+ ( ( train != null ) ?
										train.toString( )
										: "No train" )
								+ "</html>" );
							
							// Iterate
							i++;
						}
					}
				}
			}
			
			// Update the prevision
			stationPrevisionPanel.update( );
			
			// Enable/disable create incident button
			createIncidentButton.setEnabled( !s.isIncidentOccuring( ) );
		}
		
		// Show/Hide components
		if( railDetailText != null )
			for( int i = 0; i < railDetailText.length; i++ )
				railDetailText[ i ].setVisible( isActivated );
		title.setVisible( isActivated );
		createIncidentButton.setVisible( isActivated );
		rightPanel.setVisible( isActivated );
		stationPrevisionPanel.setVisible( isActivated );
	}
	
	/**
	 * Activate
	 */
	public void activate( )
	{
		// Activate
		isActivated = true;
		stationPrevisionPanel.activate( );
	}
	
	/**
	 * Deactivate
	 */
	public void deactivate( )
	{
		// Deactivate
		isActivated = false;
		stationPrevisionPanel.deactivate( );
	}
	
	/**
	 * Get selected item
	 * 
	 * @return the selected item
	 */
	public Object getSelectedItem( )
	{
		return station;
	}
	
	/**
	 * Create incident
	 */
	public void createIncident( )
	{
		// Station not null
		if( station != null )
			// No incident occuring
			if( !station.isIncidentOccuring( ) )
				// Ask for incident details
				detailPanel.getIncidentBuilder( ).createIncident( IncidentType.STATION_INCIDENT,
						station );
	}
}
