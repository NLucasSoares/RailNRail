package railnrail.gui.simulation.footer.component.detail.prevision;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import railnrail.gui.simulation.footer.component.detail.DetailContent;
import railnrail.gui.simulation.footer.component.detail.DetailPanel;
import railnrail.network.section.Station;
import railnrail.network.section.rail.RailDirection;
import railnrail.res.img.ImageResource;
import railnrail.train.prevision.ArrivalPrevision;

public class StationPrevisionPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -8030432070057563703L;

	/**
	 * Padding of the monitor
	 */
	private static final int PADDING_MONITOR = 0;
	
	/**
	 * Prevision left margin
	 */
	private static final int PREVISION_MARGIN = 20;
	
	/**
	 * Prevision count
	 */
	private static final int PREVISION_COUNT = 5;
	
	/**
	 * Is activated?
	 */
	private boolean isActivated = false;
	
	/**
	 * Title
	 */
	private JLabel title;
	
	/**
	 * Prevision list
	 */
	private JLabel[ ] previsionList = new JLabel[ StationPrevisionPanel.PREVISION_COUNT ];
	
	/**
	 * The station
	 */
	private Station station = null;
	
	/**
	 * Detail panel
	 */
	private DetailPanel detailPanel;
	
	/**
	 * Direction icons
	 */
	private ImageIcon[ ] iconDirection = new ImageIcon[ RailDirection.values( ).length ];
	
	/**
	 * Build
	 * 
	 * @param detailPanel
	 *		The whole detail panel
	 *
	 * @throws IOException
	 */
	public StationPrevisionPanel( DetailPanel detailPanel ) throws IOException
	{
		// Save
		this.detailPanel = detailPanel;
		
		// Load icons
		for( int i = 0; i < RailDirection.values( ).length; i++ )
			iconDirection[ i ] = ImageResource.loadIcon( ImageResource.values( )[ ImageResource.IMAGE_LEFT.ordinal( ) + i ] );
		
		// Set layout
		setLayout( new BorderLayout( ) );
		
		// Create title
		title = new JLabel( "Next train:" );
				
		// Add title
		add( title,
				BorderLayout.NORTH );
		
		// Center panel
		JPanel centerPanel = new JPanel( );
		
		// Set background color
		setBackground( Color.WHITE );
		
		// Set center panel layout
		centerPanel.setLayout( new GridLayout( StationPrevisionPanel.PREVISION_COUNT,
				0 ) );

		// Set border of center panel
		centerPanel.setBorder( BorderFactory.createCompoundBorder( BorderFactory.createLineBorder( Color.BLACK ),
				BorderFactory.createEmptyBorder( StationPrevisionPanel.PADDING_MONITOR,
						StationPrevisionPanel.PADDING_MONITOR,
						StationPrevisionPanel.PADDING_MONITOR,
						StationPrevisionPanel.PADDING_MONITOR ) ) );
		
		// Set background for monitor
		centerPanel.setBackground( new Color( 0x2B4EFF ) );

		// Allocate prevision
		for( int i = 0; i < StationPrevisionPanel.PREVISION_COUNT; i++ )
		{
			// Create
			previsionList[ i ] = new JLabel( );
		
			// Set background
			previsionList[ i ].setBackground( ( i % 2 ) == 0 ?
					new Color( 0x2B4EFF )
					: new Color( 0x0B0038 ) );
			previsionList[ i ].setOpaque( true );
			
			// Set font color
			previsionList[ i ].setForeground( Color.WHITE );
			
			// Set font
			previsionList[ i ].setFont( previsionList[ i ].getFont( ).deriveFont( Font.BOLD,
					16.0f ) );
			
			// Set border
			previsionList[ i ].setBorder( BorderFactory.createEmptyBorder( 0,
				StationPrevisionPanel.PREVISION_MARGIN,
				0,
				StationPrevisionPanel.PREVISION_MARGIN ) );
			
			// Add mouse listener
			final int j = i;
			previsionList[ i ].addMouseListener( new MouseListener( )
			{
				/**
				 * Field index
				 */
				private int fieldIndex = j;
				
				@Override
				public void mouseReleased( MouseEvent e )
				{
				}
				
				@Override
				public void mousePressed( MouseEvent e )
				{
				}
				
				@Override
				public void mouseExited( MouseEvent e )
				{
					// Set normal cursor
					previsionList[ fieldIndex ].setCursor( new Cursor( Cursor.DEFAULT_CURSOR ) );
				}
				
				@Override
				public void mouseEntered( MouseEvent e )
				{
					// Set hand cursor
					if( station.getPrevision( )[ fieldIndex ] != null )
						previsionList[ fieldIndex ].setCursor( new Cursor( Cursor.HAND_CURSOR ) );
				}
				
				@Override
				public void mouseClicked( MouseEvent e )
				{
					// Focus on field
					focusOnField( fieldIndex );
				}
			} );
			
			// Add
			centerPanel.add( previsionList[ i ] );
		}
		
		// Add center panel
		add( centerPanel,
				BorderLayout.CENTER );
	}
	
	/**
	 * Focus on field (train)
	 * 
	 * @param index
	 * 		The field index
	 */
	public void focusOnField( int index )
	{
		// Get field
		ArrivalPrevision arrivalPrevision = station.getPrevision( )[ index ];
		
		// Check
		if( arrivalPrevision != null )
		{
			// Focus
			detailPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_TRAIN,
					arrivalPrevision.getTrain( ) );
		}
	}
	
	/**
	 * Focus on station
	 * 
	 * @param station
	 * 		The station to be focused
	 */
	public void focusOnStation( Station station )
	{
		// Save
		this.station = station;
	}
	
	/**
	 * Activate
	 */
	public void activate( )
	{
		// Activate
		this.isActivated = true;
	}
	
	/**
	 * Deactivate
	 */
	public void deactivate( )
	{
		// Deactivate
		this.isActivated = false;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		if( isActivated )
		{
			// Show
			title.setVisible( true );
			
			// Save station
			Station station = this.station;
			
			// Set text for labels
			for( int i = 0; i < StationPrevisionPanel.PREVISION_COUNT; i++ )
			{
				// Prevision
				ArrivalPrevision prevision = station.getPrevision( )[ i ];
				
				// Check for null
				if( prevision != null )
				{
					// Set label
					previsionList[ i ].setIcon( iconDirection[ prevision.getTrain( ).getDirection( ).ordinal( ) ] );
					
					
					
					// Set text
					previsionList[ i ].setText( prevision.getTrain( ).toString( )
							+ ": "
							+ prevision.convertArrivalTime( )
							+ " (Currently: "
							+ prevision.getTrain( ).getCurrentRail( ).getParentSection( ).getName( )
							+ " - "
							+ prevision.getArrivalDistance( )
							+ "m)" );
				}
				else
				{
					previsionList[ i ].setText( "" );
					previsionList[ i ].setIcon( null );
				}
				
				// Show
				previsionList[ i ].setVisible( true );
			}
		}
		else
		{
			// Hide
			title.setVisible( false );
			for( int i = 0; i < StationPrevisionPanel.PREVISION_COUNT; i++ )
				previsionList[ i ].setVisible( false );
		}
	}
}
