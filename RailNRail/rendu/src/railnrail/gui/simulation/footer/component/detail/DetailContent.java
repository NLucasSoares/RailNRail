package railnrail.gui.simulation.footer.component.detail;

/**
 * Detail content possibilities for the detail panel
 * 
 * @author Lucas SOARES
 */
public enum DetailContent
{
	DETAIL_CONTENT_EMPTY,
	
	DETAIL_CONTENT_RAIL,
	DETAIL_CONTENT_STATION,
	DETAIL_CONTENT_TRAIN,
	DETAIL_CONTENT_INCIDENT;
	
	/**
	 * Panels names
	 */
	public static final String[ ] Name =
	{
		EmptyDetailPanel.PANEL_NAME,
		
		RailDetailPanel.PANEL_NAME,
		StationDetailPanel.PANEL_NAME,
		TrainDetailPanel.PANEL_NAME,
		IncidentDetailPanel.PANEL_NAME
	};
}
