package railnrail.gui.simulation.footer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import railnrail.gui.simulation.footer.component.detail.DetailContent;
import railnrail.gui.simulation.footer.component.detail.DetailPanel;
import railnrail.gui.simulation.footer.component.incident.IncidentListPanel;
import railnrail.gui.simulation.footer.component.train.TrainPanel;
import railnrail.network.Line;
import railnrail.simulation.Simulation;

/**
 * Footer panel
 * 
 * @author Lucas SOARES
 */
public class SimulationFooterPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -1085502238889359445L;
	
	/**
	 * Footer background color
	 */
	public static final Color FOOTER_BACKGROUND_COLOR = Color.WHITE;
	
	/**
	 * Margin
	 */
	private static final int MARGIN = 8;
	
	/**
	 * Footer title
	 */
	private static final String FOOTER_TITLE = "Control panel";
	
	/**
	 * Title font size
	 */
	private static final float TITLE_FONT_SIZE = 25.0f;
	
	/**
	 * Title
	 */
	private JLabel title;
	
	/**
	 * Detail panel
	 */
	private DetailPanel detailPanel;
	
	/**
	 * General informations panel
	 */
	private TrainPanel trainInformationPanel;
	
	/**
	 * Incident gesture panel
	 */
	private IncidentListPanel incidentGesturePanel;
	
	/**
	 * Construct footer panel
	 * 
	 * @param line
	 * 		The line
	 * 
	 * @throws IOException 
	 */
	public SimulationFooterPanel( Line line,
			Simulation simulation,
			JFrame mainFrame ) throws IOException
	{
		// Parent constructor
		super( );
		
		// Set layout
		setLayout( new BorderLayout( ) );
		
		// Set background color
		setBackground( Color.DARK_GRAY );
		
		// Set border
		setBorder( BorderFactory.createEmptyBorder( SimulationFooterPanel.MARGIN,
				SimulationFooterPanel.MARGIN,
				SimulationFooterPanel.MARGIN,
				SimulationFooterPanel.MARGIN ) );
		
		// Create title
		title = new JLabel( SimulationFooterPanel.FOOTER_TITLE );
		title.setHorizontalAlignment( SwingConstants.CENTER );
		title.setForeground( Color.WHITE );
		title.setFont( title.getFont( ).deriveFont( SimulationFooterPanel.TITLE_FONT_SIZE ) );
		title.setBorder( BorderFactory.createCompoundBorder( BorderFactory.createMatteBorder( 0,
				0,
				1,
				0,
				Color.GRAY ),
			BorderFactory.createEmptyBorder( 0,
				0,
				5,
				0 ) ) );
		
		// Add title
		add( title,
				BorderLayout.PAGE_START );
		
		// Center panel
		JPanel centerPanel = new JPanel( );
		
		// Set center panel layout
		centerPanel.setLayout( new BoxLayout( centerPanel,
				BoxLayout.LINE_AXIS ) );
		
		// Create panels
		incidentGesturePanel = new IncidentListPanel( this );
		detailPanel = new DetailPanel( line,
				simulation,
				mainFrame );
		trainInformationPanel = new TrainPanel( simulation,
				this );
		
		// Add train/station details
		centerPanel.add( detailPanel );
		
		// Add general informations
		centerPanel.add( trainInformationPanel );
		
		// Add incident gesture
		centerPanel.add( incidentGesturePanel );
		
		// Add center panel
		add( centerPanel,
				BorderLayout.CENTER );
	}
	
	/**
	 * Change detail content
	 * 
	 * @param type
	 * 		Content type
	 * @param element
	 * 		The new element to consider
	 */
	public void changeDetailContent( DetailContent type,
			Object element )
	{
		// Change content
		detailPanel.changeDetailContent( type,
				element );
	}
	
	/**
	 * Get selected item
	 * 
	 * @return the selected item
	 */
	public Object getSelectedItem( )
	{
		return detailPanel.getSelectedItem( );
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Update detail panel
		detailPanel.update( );
		
		// General informations update
		trainInformationPanel.update( );
	}
	
	/**
	 * Get incident gesture panel
	 * 
	 * @return the incident gesture panel
	 */
	public IncidentListPanel getIncidentListPanel( )
	{
		return incidentGesturePanel;
	}
}
