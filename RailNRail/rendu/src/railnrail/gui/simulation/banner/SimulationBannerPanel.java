package railnrail.gui.simulation.banner;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import railnrail.res.img.ImageResource;
import railnrail.simulation.Simulation;
import railnrail.simulation.SimulationState;

/**
 * The simulation banner, this is a banner shown above the lign L graphic. Contains an incident menu button, speed
 * buttons and a clock.
 * 
 * @author Gilles Santos
 */
public class SimulationBannerPanel extends JPanel
	implements ActionListener
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 6255484508083618750L;
	
	/**
	 * Date label
	 */
	private JLabel dateLabel = new JLabel( "",
			SwingConstants.CENTER );
	
	/**
	 * Play/Pause button
	 */
	private JButton pausePlayButton = new JButton( );
	
	/**
	 * Speed change button
	 */
	private JButton speedButton = new JButton( );
	
	/**
	 * The simulation instance
	 */
	private Simulation simulation;
	
	/**
	 * Icon for simulation state
	 */
	private ImageIcon[ ] simulationStateIcon = new ImageIcon[ SimulationState.values( ).length ];

	/**
	 * Add our components to the simulation banner panel and add action listener to the buttons. Creates a border for
	 * the time label and disable the speed button.
	 *
	 * @param simulation
	 * 		The simulation instance
	 * 
	 * @throws IOException 
	 */
	public SimulationBannerPanel( Simulation simulation ) throws IOException
	{
		// Save
		this.simulation = simulation;
		
		// Load simulation states icons
		for( SimulationState state : SimulationState.values( ) )
			simulationStateIcon[ state.ordinal( ) ] = ImageResource.loadIcon( ImageResource.values( )[ ImageResource.IMAGE_PAUSE.ordinal( ) + state.ordinal( ) ] );

		// Init buttons
		updateSpeedButtonContent( );
		
		// Set layout
		setLayout( new GridLayout( 1,
				1 ) );

		// Add components
		add( pausePlayButton );
		add( dateLabel );
		add( speedButton );

		// Add action listeners
		pausePlayButton.addActionListener( this );
		speedButton.addActionListener( this );

		// Set border around date
		dateLabel.setBorder( BorderFactory.createLineBorder( Color.GRAY,
				1 ) );
		
		// Set pause button icon
		pausePlayButton.setIcon( simulationStateIcon[ SimulationState.SIMULATION_STATE_1.ordinal( ) ] );
		
		// Create formatted time
		simulation.getCurrentTime( ).updateFormattedTime( );
		
		// Update time
		updateTimeLabel( );
	}

	/**
	 * Update speed button content
	 */
	private void updateSpeedButtonContent( )
	{
		speedButton.setIcon( simulationStateIcon[ SimulationState.getValue( simulation.getSpeedMultiplier( ) ).ordinal( ) ] );
		speedButton.setText( "x" + simulation.getSpeedMultiplier( ) );
	}
	
	/**
	 * ActionListener for the SimulationBanner's buttons.
	 * 
	 * @param e
	 * 		The ActionEvent performed.
	 */
	public void actionPerformed( ActionEvent e )
	{

		// Play/Pause button
		if( e.getSource( ).equals( pausePlayButton ) )
		{
			// Change state
			if( simulation.isPaused( ) )
				simulation.resume( );
			else
				simulation.pause( );
			
			// Change icon
			pausePlayButton.setIcon( simulationStateIcon[ !simulation.isPaused( ) ?
					SimulationState.SIMULATION_STATE_PAUSED.ordinal( )
					: SimulationState.SIMULATION_STATE_1.ordinal( ) ] );
		}
		else
			// Speed button
			if( e.getSource( ).equals( speedButton ) )
			{
				if( simulation.getSpeedMultiplier( ) == ( SimulationState.Value[ SimulationState.values( ).length - 1 ] ) )
					simulation.setSpeedMultiplier( SimulationState.Value[ SimulationState.SIMULATION_STATE_1.ordinal( ) ] );
				else
					simulation.setSpeedMultiplier( SimulationState.Value[ SimulationState.values( )[ SimulationState.getValue( simulation.getSpeedMultiplier( ) ).ordinal( ) + 1 ].ordinal( ) ] );
				
				// Update speed button content
				updateSpeedButtonContent( );
			}
	}
	
	/**
	 * Update time
	 */
	private void updateTimeLabel( )
	{
		// Display
		dateLabel.setText( simulation.getFormattedCurrentTime( ) );
		dateLabel.revalidate( );
	}
	
	/**
	 * External update request
	 */
	public void update( )
	{
		// Update time label
		updateTimeLabel( );
	}
}
