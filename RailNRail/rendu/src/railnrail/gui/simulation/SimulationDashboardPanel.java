package railnrail.gui.simulation;

import java.awt.BorderLayout;
import java.awt.ScrollPane;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import railnrail.gui.simulation.banner.SimulationBannerPanel;
import railnrail.gui.simulation.dashboard.line.LineDisplayPanel;
import railnrail.gui.simulation.footer.SimulationFooterPanel;
import railnrail.network.Line;
import railnrail.simulation.Simulation;

/**
 * The simulation dashboard, this is where you'll see the simulation of the L line.
 * 
 * @author Gilles Santos
 */
public class SimulationDashboardPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 5246437481644720096L;

	/**
	 * Simulation banner
	 */
	private SimulationBannerPanel simulationBanner;
	
	/**
	 * Line display panel
	 */
	private LineDisplayPanel lineDisplayPanel;
	
	/**
	 * Scroll panel for line display
	 */
	private ScrollPane lineLScrollPane = new ScrollPane( );
	
	/**
	 * Footer panel
	 */
	private SimulationFooterPanel simulationFooter;
	
	/**
	 * Add label containing the line L image to the scroll pane and set the splitPane parameters.
	 * 
	 * @param line
	 * 		The line
	 * @param simulation
	 * 		The simulation instance
	 * @param mainFrame
	 * 		The main frame
	 * 
	 * @throws IOException 
	 */
	public SimulationDashboardPanel( Line line,
			Simulation simulation,
			JFrame mainFrame ) throws IOException
	{
		// Set layout
		setLayout( new BorderLayout( ) );
		
		// Create banner panel
		simulationBanner = new SimulationBannerPanel( simulation );
		
		// Create line display panel
		lineDisplayPanel = new LineDisplayPanel( line,
				this );
		
		// Create footer
		simulationFooter = new SimulationFooterPanel( line,
				simulation,
				mainFrame );
		
		// Add components
		add( simulationBanner,
				BorderLayout.PAGE_START );
		add( lineLScrollPane,
				BorderLayout.CENTER );
		add( simulationFooter,
				BorderLayout.PAGE_END );
		
		// Set what will be scrolled
		lineLScrollPane.add( lineDisplayPanel );
	}

	/**
	 * @return the lineLScrollPane
	 */
	public ScrollPane getLineLScrollPane( )
	{
		return lineLScrollPane;
	}

	/**
	 * @return the lineDisplayPanel
	 */
	public LineDisplayPanel getLineDisplayPanel( )
	{
		return lineDisplayPanel;
	}

	/**
	 * @return the simulationBanner
	 */
	public SimulationBannerPanel getSimulationBanner( )
	{
		return simulationBanner;
	}

	/**
	 * @return the simulationFooter
	 */
	public SimulationFooterPanel getSimulationFooter( )
	{
		return simulationFooter;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Update banner
		simulationBanner.update( );
		
		// Update footer
		simulationFooter.update( );
	}
}
