package railnrail.gui.simulation.dashboard.line.plan;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.security.InvalidParameterException;

import javax.swing.JPanel;

import railnrail.gui.simulation.dashboard.line.plan.rail.CellDisplay;
import railnrail.gui.simulation.dashboard.line.plan.rail.RailCellDisplay;
import railnrail.gui.simulation.dashboard.line.plan.rail.RailPictureStorage;
import railnrail.gui.simulation.dashboard.line.plan.rail.RedlightState;
import railnrail.gui.simulation.dashboard.line.plan.rail.SectionCellDisplay;
import railnrail.gui.simulation.dashboard.line.plan.rail.StationCellType;
import railnrail.incident.Incident;
import railnrail.incident.station.StationIncident;
import railnrail.network.Line;
import railnrail.network.empty.EmptyPlan;
import railnrail.network.section.Section;
import railnrail.network.section.Station;
import railnrail.network.section.rail.RailDirection;
import railnrail.train.Train;

/**
 * Display plan for the line
 * 
 * @author Lucas SOARES
 */
public class DisplayPlan
{
	/**
	 * Display cell
	 */
	private CellDisplay[ ][ ] cell;
	
	/**
	 * Line
	 */
	private Line line;
	
	/**
	 * Rail picture storage
	 */
	private RailPictureStorage railStorage;
	
	/**
	 * Rail of station background color
	 */
	private static final Color STATION_RAIL_BACKGROUND_COLOR = new Color( 0xFFEA75 );
	
	/**
	 * Padding before redlight
	 */
	private static final int SECTION_REDLIGHT_PADDING_TOP = 0;
	
	/**
	 * Station name correction Y
	 */
	private static final int STATION_NAME_POSITION_CORRECTION_Y = 17;
	
	/**
	 * Alpha direction
	 */
	private float alphaDirection;
	
	/**
	 * Current incident light
	 */
	private int currentIncidentLight = 0;
	
	/**
	 * Selected item
	 */
	private Object selectedItem;
	
	/**
	 * Build the display plan
	 * 
	 * @param line
	 * 		The line to display
	 * @param railStorage
	 * 		The rail storage
	 * @param alphaDirection
	 * 		The alpha direction
	 * @param currentIncidentLight
	 * 		The incident light
	 * @param selectedItem
	 * 		The item being selected
	 */
	public DisplayPlan( Line line,
			RailPictureStorage railStorage,
			float alphaDirection,
			int currentIncidentLight,
			Object selectedItem )
	{
		// Size of the grid
		Point size = line.getMaxPosition( );

		// Save
		this.railStorage = railStorage;
		this.alphaDirection = alphaDirection;
		this.line = line;
		this.selectedItem = selectedItem;
		this.currentIncidentLight = currentIncidentLight;
		
		// Allocate
		cell = new CellDisplay[ size.x ][ ];
		for( int i = 0; i < size.x; i++ )
		{
			cell[ i ] = new CellDisplay[ size.y ];
			for( int j = 0; j < size.y; j++ )
				cell[ i ][ j ] = new CellDisplay( );
		}
		
		// Positionning the rail
		for( int i = 0; i < line.getSectionCount( ); i++ )
		{
			// Get section
			Section section = line.getSection( i );
			
			// Rail index
			int railIndex = 0;
			
			// Iterate direction
			for( RailDirection direction : RailDirection.values( ) )
			{
				// Vertical start point
				int base = 0;
				
				// Choose vertical start point
				switch( direction )
				{
					case RAIL_DIRECTION_DEPARTURE:
						// Select base
						base = line.getMaxRailCount( section.getId( ),
								RailDirection.RAIL_DIRECTION_DEPARTURE );
						
						// Reset rail counter
						railIndex = 0;
						break;
						
					default:
						break;
				}
				
				// Iterate rails
				for( int j = 0; j < section.getRailCount( direction ); j++ )
				{				
					// Create
					cell[ section.getPosition( ).x ][ base + section.getPosition( ).y + railIndex ] = new RailCellDisplay( section.getRail( direction,
							j ),
						railStorage,
						section.getRail( direction,
								j ).getParentSection( ) instanceof Station ?
									DisplayPlan.STATION_RAIL_BACKGROUND_COLOR
									: Color.WHITE );
					
					// Increment rail index
					railIndex++;
				}
			}
					
		}
		
		// Positionning the sections
		for( int i = 0; i < line.getSectionCount( ); i++ )
		{
			// Get section
			Section section = line.getSection( i );
			
			// Check position
			if( section.getPosition( ).x < 0
					|| section.getPosition( ).y - 1 < 0
					|| section.getPosition( ).x >= cell.length
					|| section.getPosition( ).y - 1 >= cell[ 0 ].length )
				continue;
			
			// Create section display
				// Bottom
					cell[ section.getPosition( ).x ][ section.getPosition( ).y - 1 ] = new SectionCellDisplay( section,
							Color.WHITE,
							section instanceof Station ?
								StationCellType.TYPE_BOTTOM
								: StationCellType.TYPE_EMPTY );
				// Top
					if( section.getPosition( ).y - 2 >= 0
						&& section instanceof Station )
						cell[ section.getPosition( ).x ][ section.getPosition( ).y - 2 ] = new SectionCellDisplay( section,
							Color.WHITE,
							StationCellType.TYPE_TOP );
		}
	}
	
	/**
	 * Display the line
	 * 
	 * @param panel
	 * 		The panel
	 * @param g
	 * 		The display screen
	 */
	public void display( JPanel panel,
			Graphics g )
	{
		// Empty plan
		EmptyPlan emptyPlan = line.getEmptyPlan( );
		
		// Clear
		g.setColor( new Color( 0x7F, 0x7F, 0xFF ) );
		g.fillRect( 0, 0, panel.getWidth( ), panel.getHeight( ) );
		
		// Draw line
		for( int i = 0; i < cell.length; i++ )
			for( int j = 0; j < cell[ i ].length; j++ )
			{			    			
				// If the cell isn't empty
				if( cell[ i ][ j ] instanceof RailCellDisplay )
				{
					if( selectedItem == ((RailCellDisplay)cell[ i ][ j ]).getRail( )
							|| ( selectedItem instanceof Incident
									&& ( (Incident)selectedItem ).getObject( ) == ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ) ) )
					{
						// Set alpha for selected item
						AlphaComposite ac = java.awt.AlphaComposite.getInstance( AlphaComposite.SRC_OVER,
								alphaDirection );
					    ( (Graphics2D)g ).setComposite( ac );
					}
					    
					// Draw cell
					g.drawImage( railStorage.getRail( ( (RailCellDisplay)cell[ i ][ j ] ).getRailType( ) ),
							i * railStorage.getSize( ).x,
							j * railStorage.getSize( ).y,
							panel );
					
					// Set alpha for direction arrow
					AlphaComposite ac = java.awt.AlphaComposite.getInstance( AlphaComposite.SRC_OVER,
							alphaDirection );
				    ( (Graphics2D)g ).setComposite( ac );
				    
				    // Draw direction arrow
					g.drawImage( railStorage.getDirection( ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getDirection( ) ),
							i * railStorage.getSize( ).x + ( ( railStorage.getSize( ).x / 2 - railStorage.getDirection( ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getDirection( ) ).getWidth( ) / 2 ) ),
							j * railStorage.getSize( ).y + ( ( railStorage.getSize( ).y / 2 - railStorage.getDirection( ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getDirection( ) ).getHeight( ) / 2 ) ),
							panel );
					
					// Restore alpha
					ac = java.awt.AlphaComposite.getInstance( AlphaComposite.SRC_OVER,
							1.0f );
				    ( (Graphics2D)g ).setComposite( ac );
				}
				else if( cell[ i ][ j ] instanceof SectionCellDisplay )
				{
					// Selected item?
					if( selectedItem == ((SectionCellDisplay)cell[ i ][ j ]).getSection( )
							|| ( selectedItem instanceof StationIncident
									&& ( (Incident)selectedItem ).getObject( ) == ( (SectionCellDisplay)cell[ i ][ j ] ).getSection( ) ) )
					{
						// Set alpha for selected item
						AlphaComposite ac = java.awt.AlphaComposite.getInstance( AlphaComposite.SRC_OVER,
								alphaDirection );
					    ( (Graphics2D)g ).setComposite( ac );
					}
				    
					// Draw
					g.drawImage( railStorage.getStationCell( ((SectionCellDisplay)cell[ i ][ j ]).getType( ) ),
							i * railStorage.getSize( ).x,
							j * railStorage.getSize( ).y,
							panel );
					
					// Restore alpha on selected item
					if( selectedItem == ((SectionCellDisplay)cell[ i ][ j ]).getSection( )
							|| ( selectedItem instanceof Incident
									&& ( (Incident)selectedItem ).getObject( ) == ( (SectionCellDisplay)cell[ i ][ j ] ).getSection( ) ) )
					{
						// Set alpha for selected item
						AlphaComposite ac = java.awt.AlphaComposite.getInstance( AlphaComposite.SRC_OVER,
								1.0f );
					    ( (Graphics2D)g ).setComposite( ac );
					}
				}
				else
					g.drawImage( railStorage.getEmptyCell( emptyPlan.getType( i,
								j ) ),
							i * railStorage.getSize( ).x,
							j * railStorage.getSize( ).y,
							panel );
			}
		
		// Draw train
		for( int i = 0; i < cell.length; i++ )
			for( int j = 0; j < cell[ i ].length; j++ )
				// If the cell isn't empty
				if( cell[ i ][ j ] instanceof RailCellDisplay )
				{
					// If train is present
					if( ( (RailCellDisplay)cell[ i ][ j ] ).isTrain( ) )
					{
						// Is train selected?
						boolean isTrainSelected = selectedItem == ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getTrain( )
								|| ( selectedItem instanceof Incident
										&& ( (Incident)selectedItem ).getObject( ) == ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getTrain( ) );
						
						// Get train icon
						BufferedImage trainIcon = railStorage.getTrainIcon( isTrainSelected );
						
						// Train position
						Point position = new Point( i * railStorage.getSize( ).x - trainIcon.getWidth( ) / 2 + ( (RailCellDisplay)cell[ i ][ j ] ).getTrainPosition( ),
								j * railStorage.getSize( ).y + ( railStorage.getSize( ).y / 2 - trainIcon.getHeight( ) / 2 ) );
						
						// Alpha for selected train
						if( isTrainSelected )
						{
							// Set alpha for selected item
							AlphaComposite ac = java.awt.AlphaComposite.getInstance( AlphaComposite.SRC_OVER,
									alphaDirection );
						    ( (Graphics2D)g ).setComposite( ac );
						}
						
						// Draw
						g.drawImage( trainIcon,
								position.x,
								position.y,
								panel );
						
						// Alpha for selected train
						if( isTrainSelected )
						{
							// Set alpha for selected item
							AlphaComposite ac = java.awt.AlphaComposite.getInstance( AlphaComposite.SRC_OVER,
									1.0f );
						    ( (Graphics2D)g ).setComposite( ac );
						}
						
						// Get train
						Train train = ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getTrain( );
						
						// Show information + travel code
						if( train != null )
						{
							// Text
							String text = train.toString( );
							
							// Draw
							g.setFont( g.getFont( ).deriveFont( Font.BOLD,
									16.0f ) );
							g.setColor( train.isIncidentOccuring( ) ?
									Color.RED
									: Color.WHITE );
							g.drawString( text,
									position.x + ( trainIcon.getWidth( ) / 2 - g.getFontMetrics( ).stringWidth( text ) / 2 ),
									position.y + trainIcon.getHeight( ) + g.getFontMetrics( ).getHeight( ) );
							g.setFont( g.getFont( ).deriveFont( 0 ) );
						}
					}
				}
		
		// Draw redlights and station name
		for( int i = 0; i < cell.length; i++ )
			for( int j = 0; j < cell[ i ].length; j++ )
				// If the cell isn't empty
				if( cell[ i ][ j ] instanceof RailCellDisplay )
				{
					// Redlight
					BufferedImage redLight = null;
					
					// Get redlight picture
					if( ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).isIncidentOccuring( )
							|| ( ( ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getParentSection( ) instanceof Station ) ?
									( ( (Station)( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getParentSection( ) ).isIncidentOccuring( ) )
									: false ) )
						redLight = railStorage.getRedlight( RedlightState.values( )[ RedlightState.STATE_INCIDENT_1.ordinal( ) + currentIncidentLight ] );
					else
						redLight = railStorage.getRedlight( ( (RailCellDisplay)cell[ i ][ j ] ).isTrain( ) ?
							RedlightState.STATE_OFF
							: RedlightState.STATE_ON );
					
					// Draw redlight
					g.drawImage( redLight,
						( ( (RailCellDisplay)cell[ i ][ j ] ).getRail( ).getDirection( ) == RailDirection.RAIL_DIRECTION_DEPARTURE ?
								i
								: i + 1 ) * railStorage.getSize( ).x,
						( j + 1 ) * railStorage.getSize( ).y - redLight.getHeight( ) + SECTION_REDLIGHT_PADDING_TOP,
						panel );
				}
				else if( cell[ i ][ j ] instanceof SectionCellDisplay )
				{
					// Is it a station?
					if( ( (SectionCellDisplay)cell[ i ][ j ] ).getSection( ) instanceof Station
							&& ( (SectionCellDisplay)cell[ i ][ j ] ).getType( ) == StationCellType.TYPE_TOP )
					{
						// Display name
						g.setFont( g.getFont( ).deriveFont( Font.BOLD,
								11.0f ) );
						g.setColor( Color.BLACK );
						g.drawString( ( (SectionCellDisplay)cell[ i ][ j ] ).getSection( ).getNickname( ),
								i * railStorage.getSize( ).x + ( railStorage.getSize( ).x / 2 - g.getFontMetrics( ).stringWidth( ( (SectionCellDisplay)cell[ i ][ j ] ).getSection( ).getNickname( ) ) / 2 ),
								j * railStorage.getSize( ).y + DisplayPlan.STATION_NAME_POSITION_CORRECTION_Y );
					}
				}
		
		// Draw line name
		g.setFont( g.getFont( ).deriveFont( Font.BOLD,
				18.0f ) );
		g.setColor( Color.WHITE );
		g.drawString( line.getName( ),
				10,
				g.getFontMetrics( ).getHeight( ) + 10 );
	}
	
	/**
	 * Get display size
	 * 
	 * @return display size
	 */
	public Dimension getDisplaySize( )
	{
		return new Dimension( railStorage.getSize( ).x * cell.length,
				railStorage.getSize( ).y * cell[ 0 ].length );
	}
	
	/**
	 * Get a cell
	 * 
	 * @param x
	 * 		The x coordinate
	 * @param y
	 * 		The y coordinate
	 * 
	 * @throws InvalidParameterException
	 */
	public CellDisplay getCell( int x,
			int y ) throws InvalidParameterException
	{
		// Check parameter
		if( x < 0
				|| y < 0
				|| x >= cell.length
				|| y >= cell[ 0 ].length )
			throw new InvalidParameterException( );
		
		// OK
		return cell[ x ][ y ];
	}
}
