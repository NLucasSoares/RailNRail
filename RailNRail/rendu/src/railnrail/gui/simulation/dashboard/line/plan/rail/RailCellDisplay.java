package railnrail.gui.simulation.dashboard.line.plan.rail;

import java.awt.Color;

import railnrail.network.section.rail.Rail;
import railnrail.network.type.RailType;
import railnrail.train.Train;

/**
 * A cell for rail display
 * 
 * @author Lucas SOARES
 */
public class RailCellDisplay
	extends CellDisplay
{
	/**
	 * Parent rail
	 */
	private Rail rail;
	
	/**
	 * Train position
	 */
	private int trainPosition;
	
	/**
	 * Is train present?
	 */
	private boolean isTrainPresent = false;
	
	/**
	 * Build rail cell display
	 * 
	 * @param rail
	 * 		The rail
	 * @param railStorage
	 * 		The rail storage
	 * @param backgroundColor
	 * 		The background color
	 */
	public RailCellDisplay( Rail rail,
			RailPictureStorage railStorage,
			Color backgroundColor )
	{		
		// Parent
		super( backgroundColor );
		
		// Save
		this.rail = rail;
		
		// Train
		Train train;
		
		// Calculate train position
		if( ( train = rail.getTrain( ) ) != null )
		{
			// Position
			double trainPosition = train.getPosition( );

			// Calculate position
			switch( train.getDirection( ) )
			{
				default:
				case RAIL_DIRECTION_DEPARTURE:
					this.trainPosition = ( trainPosition > 0.0d ) ?
							(int)( ( trainPosition / (double)rail.getParentSection( ).getSize( ) ) * (double)railStorage.getSize( ).x )
							: 0;
					break;
					
				case RAIL_DIRECTION_ARRIVAL:
					this.trainPosition = ( trainPosition > 0.0d ) ?
							railStorage.getSize( ).x - ( (int)( ( trainPosition / (double)rail.getParentSection( ).getSize( ) ) * (double)railStorage.getSize( ).x ) )
							: railStorage.getSize( ).x;
					break;
			}
					
			// There is a train here
			this.isTrainPresent = true;
		}
	}
	
	/**
	 * Get rail type
	 * 
	 * @return the rail type
	 */
	public RailType getRailType( )
	{
		return rail.getParentSection( ).getRailType( );
	}
	
	/**
	 * Is train?
	 * 
	 * @return if train is present
	 */
	public boolean isTrain( )
	{
		return isTrainPresent;
	}
	
	/**
	 * Get train position
	 * 
	 * @return train position
	 */
	public int getTrainPosition( )
	{
		return trainPosition;
	}
	
	/**
	 * Get rail
	 * 
	 * @return the rail
	 */
	public Rail getRail( )
	{
		return rail;
	}
}
