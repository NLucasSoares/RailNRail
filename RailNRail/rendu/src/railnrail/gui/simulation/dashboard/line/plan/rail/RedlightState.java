package railnrail.gui.simulation.dashboard.line.plan.rail;

/**
 * Redlight state
 * 
 * @author Lucas SOARES
 */
public enum RedlightState
{
	STATE_ON,
	STATE_OFF,
	STATE_INCIDENT_1,
	STATE_INCIDENT_2;
}
