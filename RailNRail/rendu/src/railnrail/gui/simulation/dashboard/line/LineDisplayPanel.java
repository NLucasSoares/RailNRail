package railnrail.gui.simulation.dashboard.line;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.security.InvalidParameterException;

import javax.swing.JPanel;

import railnrail.gui.simulation.SimulationDashboardPanel;
import railnrail.gui.simulation.dashboard.line.plan.DisplayPlan;
import railnrail.gui.simulation.dashboard.line.plan.rail.CellDisplay;
import railnrail.gui.simulation.dashboard.line.plan.rail.RailCellDisplay;
import railnrail.gui.simulation.dashboard.line.plan.rail.RailPictureStorage;
import railnrail.gui.simulation.dashboard.line.plan.rail.SectionCellDisplay;
import railnrail.gui.simulation.footer.component.detail.DetailContent;
import railnrail.network.Line;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;

/**
 * Line display
 * 
 * @author Lucas SOARES
 */
public class LineDisplayPanel
	extends JPanel
{
	/**
	 * Serial id
	 */
	private static final long serialVersionUID = -1086108942023921416L;

	/**
	 * Rail picture storage
	 */
	private RailPictureStorage railStorage;
	
	/**
	 * Plan for display
	 */
	private DisplayPlan displayPlan;
	
	/**
	 * Direction alpha
	 */
	private float directionAlpha;
	
	/**
	 * Incident redlight
	 */
	private int incidentRedlight;
	
	/**
	 * Line
	 */
	private Line line;
	
	/**
	 * Dashboard panel
	 */
	private SimulationDashboardPanel dashboardPanel;
	
	/**
	 * Construct the line display
	 * 
	 * @param line
	 * 		The line
	 * 
	 * @throws IOException 
	 */
	public LineDisplayPanel( Line line,
			SimulationDashboardPanel dashboardPanel ) throws IOException
	{
		// Parent constructor
		super( );
		
		// Load rails
		railStorage = new RailPictureStorage( );
		
		// Save
		this.line = line;
		this.dashboardPanel = dashboardPanel;
		
		// Create line display plan
		displayPlan = new DisplayPlan( line,
				railStorage,
				directionAlpha,
				0,
				null );
		
		// Add mouse listener
		addMouseListener( new MouseListener( )
		{
			
			@Override
			public void mouseReleased( MouseEvent e )
			{
			}
			
			@Override
			public void mousePressed( MouseEvent e )
			{
			}
			
			@Override
			public void mouseExited( MouseEvent e )
			{
			}
			
			@Override
			public void mouseEntered( MouseEvent e )
			{
			}
			
			@Override
			public void mouseClicked( MouseEvent e )
			{
				clickListener( e );
			}
		} );
		
		// Set the panel size
		setPreferredSize( new Dimension( displayPlan.getDisplaySize( ) ) );
	}
	
	public void paint( Graphics g )
	{
		// Update panels
		dashboardPanel.update( );
				
		// Clear
		g.clearRect( 0,
				0,
				getWidth( ),
				getHeight( ) );
		
		// Create line display plan
		displayPlan = new DisplayPlan( line,
				railStorage,
				directionAlpha,
				incidentRedlight,
				dashboardPanel.getSimulationFooter( ).getSelectedItem( ) );
		
		// Print
		displayPlan.display( this,
				g );
	}
	
	/**
	 * Set direction alpha
	 * 
	 * @param directionAlpha
	 * 		The current direction alpha
	 */
	public void setDirectionAlpha( float directionAlpha )
	{
		this.directionAlpha = directionAlpha;
	}
	
	/**
	 * Set current incident redlight
	 * 
	 * @param incidentRedlight
	 * 		The current incident redlight state
	 */
	public void setCurrentIncidentRedlight( int incidentRedlight )
	{
		this.incidentRedlight = incidentRedlight;
	}
	
	
	/**
	 * Click listener
	 * 
	 * @param mouseEvent
	 * 		The mouse event details
	 */
	public void clickListener( MouseEvent mouseEvent )
	{
		// The cell
		CellDisplay cell;
		
		// Get cell
		try
		{
			cell = displayPlan.getCell( mouseEvent.getX( ) / railStorage.getSize( ).x,
				mouseEvent.getY( ) / railStorage.getSize( ).y );
		}
		catch( InvalidParameterException e )
		{
			// Quit
			return;
		}
		
		if( cell instanceof RailCellDisplay )
			clickOnRail( ((RailCellDisplay)cell).getRail( ) );
		else
			if( cell instanceof SectionCellDisplay )
			{
				if( ( (SectionCellDisplay)cell ).getSection( ) instanceof Station )
					clickOnStation( (Station)( (SectionCellDisplay)cell ).getSection( ) );
				else
					clickOnEmpty( );
			}
			else
				clickOnEmpty( );
	}
	
	/**
	 * Click on rail
	 * 
	 * @param rail
	 * 		The rail being clicked
	 */
	public void clickOnRail( Rail rail )
	{
		// Change detail panel content
		dashboardPanel.getSimulationFooter( ).changeDetailContent( DetailContent.DETAIL_CONTENT_RAIL,
				rail );
	}
	
	/**
	 * Click on station
	 * 
	 * @param station
	 * 		The station
	 */
	public void clickOnStation( Station station )
	{
		// Change detail panel content
		dashboardPanel.getSimulationFooter( ).changeDetailContent( DetailContent.DETAIL_CONTENT_STATION,
				station );
	}
	
	/**
	 * Click on empty cell
	 */
	public void clickOnEmpty( )
	{
		// Empty panel
		dashboardPanel.getSimulationFooter( ).changeDetailContent( DetailContent.DETAIL_CONTENT_EMPTY,
				null );
	}
}
