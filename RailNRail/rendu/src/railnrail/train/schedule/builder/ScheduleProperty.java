package railnrail.train.schedule.builder;

/**
 * Schedule properties
 * 
 * @author Lucas SOARES
 */
public enum ScheduleProperty
{
	PROPERTY_DEPARTURE_DAY,
	PROPERTY_DEPARTURE_TIME,
	PROPERTY_DEPARTURE_STATION,
	PROPERTY_DESTINATION_STATION_NAME,
	PROPERTY_TRAVEL_CODE;
}
