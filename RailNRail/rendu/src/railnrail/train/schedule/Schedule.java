package railnrail.train.schedule;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import railnrail.file.Helper;
import railnrail.simulation.time.CurrentTime;
import railnrail.simulation.time.Day;
import railnrail.train.schedule.builder.ScheduleProperty;

/**
 * Train schedule
 * 
 * @author Lucas SOARES
 */
public class Schedule
{
	/**
	 * Trains plannings
	 */
	private HashMap<Day, ArrayList<TrainPlanning>> trainPlanning = new HashMap<Day, ArrayList<TrainPlanning>>( );
	
	/**
	 * Construct schedule
	 * 
	 * @param resFolder
	 * 		The resources folder
	 * @param scheduleFile
	 * 		The schedule file
	 * @param initialTimestamp
	 * 		The initial timestamp
	 * 
	 * @throws IOException 
	 */
	public Schedule( String resFolder,
			String scheduleFile,
			long initialTimestamp ) throws IOException
	{
		// Open file
		BufferedReader fr = new BufferedReader( new InputStreamReader( new FileInputStream( new File( resFolder
				+ "/"
				+ scheduleFile ) ) ) );

		// Line
		String line;
		

		// Create hashmap days
		for( Day day : Day.values( ) )
			trainPlanning.put( day,
				new ArrayList<TrainPlanning>( ) );
		
		// Read
		while( ( line = Helper.getNextUncommentedLine( fr ) ) != null )
		{
			// Get properties
			String[ ] properties = line.split( ";" );
			
			// Check
			if( properties.length != ScheduleProperty.values( ).length )
				throw new IOException( "Can't load file \""
						+ resFolder
						+ "/"
						+ scheduleFile
						+ "\"" );

			// Days
			String[ ] days = properties[ ScheduleProperty.PROPERTY_DEPARTURE_DAY.ordinal( ) ].split( "," );
			
			// Get time informations
			String[ ] timeInfo = properties[ ScheduleProperty.PROPERTY_DEPARTURE_TIME.ordinal( ) ].split( "h" );
			
			// Check informations
			if( days == null
					|| timeInfo == null
					|| days.length == 0
					|| timeInfo.length != 2 )
				throw new IOException( "Can't load file \""
						+ resFolder
						+ "/"
						+ scheduleFile
						+ "\""
						+ " line \""
						+ line
						+ "\"" );
			
			
			// Add to schedule
			for( String day : days )
			{
				// Get day enum value
				Day dayEnum = Day.getDay( day );
				
				// Check
				if( dayEnum == null )
					throw new IOException( "Can't load file \""
							+ resFolder
							+ "/"
							+ scheduleFile
							+ "\""
							+ " line \""
							+ line
							+ "\"" );
				
				// Get day
				trainPlanning.get( dayEnum ).add( new TrainPlanning( Integer.parseInt( timeInfo[ 0 ] ),
								Integer.parseInt( timeInfo[ 1 ] ),
								properties[ ScheduleProperty.PROPERTY_DEPARTURE_STATION.ordinal( ) ],
								properties[ ScheduleProperty.PROPERTY_DESTINATION_STATION_NAME.ordinal( ) ],
								properties[ ScheduleProperty.PROPERTY_TRAVEL_CODE.ordinal( ) ],
								dayEnum,
								initialTimestamp ) );
			}
		}
		
		// Close file
		fr.close( );
	}
	
	/**
	 * Get trains which have to go
	 * 
	 * @param currentTime
	 * 		The current time
	 * 
	 * @return trains which have to go
	 */
	public ArrayList<TrainPlanning> getTrainScheduled( CurrentTime currentTime )
	{
		// Output
		ArrayList<TrainPlanning> output = new ArrayList<TrainPlanning>( );

		// Iterate
		for( TrainPlanning planning : trainPlanning.get( currentTime.getDay( ) ) )
		{
			// Train already gone?
			if( planning.isGone( ) )
				continue;

			// Check if it's time to go
			if( currentTime.getDeltaTime( ) >= planning.getDepartureTimestamp( ) )
				// Add train departure request
				output.add( planning );
		}
		
		// OK
		return output;
	}
	
	/**
	 * Reset the schedule
	 */
	public void reset( )
	{
		// Iterate
		for( Day day : Day.values( ) )
			for( TrainPlanning planning : trainPlanning.get( day ) )
				planning.reset( );
	}
	
	/**
	 * Cancel previous train
	 * 
	 * @param timestamp
	 * 		The timestamp
	 */
	public void cancelPreviousTrain( long timestamp )
	{
		// Iterate
		for( Day day : Day.values( ) )
			for( TrainPlanning planning : trainPlanning.get( day ) )
				// Departure timestamp before timestamp
				if( planning.getDepartureTimestamp( ) < timestamp * 1000 )
					// Cancel train
					planning.leave( );
	}
}
