package railnrail.train.prevision;

import java.util.ArrayList;

import railnrail.math.speed.Helper;
import railnrail.network.section.Section;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;
import railnrail.simulation.Simulation;
import railnrail.train.Train;

/**
 * The list of all stations arrival prevision for one train
 * 
 * @author Lucas SOARES
 */
public class TrainPrevisionList
{
	/**
	 * Previsions list
	 */
	private ArrayList<ArrivalPrevision> arrivalPrevision = new ArrayList<ArrivalPrevision>( );
	
	/**
	 * Current timestamp
	 */
	private long currentTimestamp;
	
	/**
	 * Construct the previsions
	 * 
	 * @param train
	 * 		The train to consider
	 * @param simulation
	 * 		The simulation
	 */
	public TrainPrevisionList( Train train,
			Simulation simulation )
	{
		// Current rail
		Rail currentRail = train.getCurrentRail( );
		
		// Total distance for now
		int totalDistance = 0;
		
		// Total time
		double totalTime = 0.0d;
		
		// Iteration ID
		int iterationID = 0;
		
		// Calculate until terminus
		while( currentRail != null )
		{
			// Get section
			Section section = currentRail.getParentSection( );
			
			// Distance
			double distance = (double)currentRail.getParentSection( ).getSize( ) - ( currentRail == train.getCurrentRail( ) ?
					train.getPosition( )
					: 0.0d );
			
			// Speed by distance (seconds^-1)
			double speedByDistance = distance == 0.0d ?
					1.0d
					: ( Helper.km_hToM_s( currentRail.getParentSection( ).getMaximumSpeed( ) ) / distance );
			
			// Time to travel this section
			double timeToTravel = 0;
			
			// Calculate time to travel (seconds
			if( speedByDistance > 0.0d )
				timeToTravel = 1.0d / ( speedByDistance );

			// If station
			if( section instanceof Station )
				// Add prevision
				if( totalTime != 0.0d )
				{
					// Create prevision
					ArrivalPrevision prevision = new ArrivalPrevision( (Station)section,
							train,
							(long)totalTime,
							simulation.getCurrentTime( ).getCurrentTime( ),
							(int)totalDistance );
					
					// Add prevision
					arrivalPrevision.add( prevision );
					
					// Notify station
					( (Station)section ).addArrivalPrevision( train,
							prevision );
				}

			
			// Add distance
			totalDistance += section.getSize( ) - ( currentRail == train.getCurrentRail( ) ?
					train.getPosition( )
					: 0.0d );
			
			// Add to total time
			totalTime += timeToTravel;
			
			// Quit if terminus
			if( ( currentRail.getParentSection( ).isTerminus( )
					&& iterationID != 0 )
					|| train.isArrived( )
					|| ( train.getCurrentRail( ).getParentSection( ).isTerminus( )
							&& train.getCurrentRail( ).getParentSection( ).getTravelCode( ).equals(  train.getTravelCode( ) ) ) )
				break;
						
			// Get next rail
			currentRail = currentRail.getRailConnectedToDestination( train.getTravelCode( ),
					train.getDirection( ),
					false );
			
			// Increase
			iterationID++;
		}
		
		// Save
		currentTimestamp = simulation.getCurrentTime( ).getCurrentTime( );
	}
	
	/**
	 * Get the arrival previsions
	 * 
	 * @return the arrival previsions
	 */
	public ArrayList<ArrivalPrevision> getArrivalPrevision( )
	{
		return arrivalPrevision;
	}
	
	/**
	 * Get arrival prevision for one specific section
	 * 
	 * @param section
	 * 		The section to consider
	 * 
	 * @return the arrival prevision
	 */
	public ArrivalPrevision getArrivalPrevision( Section section )
	{
		// Look for section
		for( ArrivalPrevision prevision : arrivalPrevision )
			// Same section?
			if( prevision.getSection( ) == section )
				// OK
				return prevision;
		
		// Can't find section
		return null;
	}
	
	/**
	 * Get timestamp when prevision was made
	 * 
	 * @return the timestamp
	 */
	public long getTimestamp( )
	{
		return currentTimestamp;
	}
}
