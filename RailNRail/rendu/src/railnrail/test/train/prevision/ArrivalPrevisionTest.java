/**
 * 
 */
package railnrail.test.train.prevision;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import railnrail.simulation.time.CurrentTime;
import railnrail.train.prevision.ArrivalPrevision;

/**
 * @author marilou
 *
 */
public class ArrivalPrevisionTest {

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#ArrivalPrevision(railnrail.network.section.Section, railnrail.train.Train, long, long, int)}.
	 */
	@Test
	public void testArrivalPrevision() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#getSection()}.
	 */
	@Test
	public void testGetSection() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#getTrain()}.
	 */
	@Test
	public void testGetTrain() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#getArrivalDelay()}.
	 */
	@Test
	public void testGetArrivalDelay() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#getCurrentTimestamp()}.
	 */
	@Test
	public void testGetCurrentTimestamp() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#getArrivalDistance()}.
	 */
	@Test
	public void testGetArrivalDistance() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#convertArrivalTime()}.
	 */
	@Test
	public void testConvertArrivalTime() {
		//She returns the result of method of the class Helper of the package Time
		//Method indirectly tested  by this class
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#getArrivalTime()}.
	 */
	@Test
	public void testGetArrivalTime() {
		long a = 0;
		long b = 0;
		int c = 0;
		Random r = new Random();
		ArrivalPrevision ap = new ArrivalPrevision(null, null, a, b, c);
		
		long result = CurrentTime.INITIAL_TIMESTAMP;
		assertEquals(1483315200 , result);
		
		for(int i = 0; i <= 100; i++)
		{
			
			assertEquals(1483315200 + a + b, result);
			a = r.nextInt();
			b = r.nextInt();
			ap = new ArrivalPrevision(null, null, a, b, c);
			
			result = CurrentTime.INITIAL_TIMESTAMP + ap.getArrivalDelay() + ap.getCurrentTimestamp();
			
		}
		
	}

	/**
	 * Test method for {@link railnrail.train.prevision.ArrivalPrevision#getArrivalDate()}.
	 */
	@Test
	public void testGetArrivalDate() {
		//Class Date, I supposed it's correct.
	}

}
