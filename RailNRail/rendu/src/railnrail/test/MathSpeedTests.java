package railnrail.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.math.speed.HelperTest;

@RunWith(Suite.class)
@SuiteClasses({ HelperTest.class })
/**
 * TestSuite for the package math.speed
 * @author marilou
 *
 */
public class MathSpeedTests {

}
