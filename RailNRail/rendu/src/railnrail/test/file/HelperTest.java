package railnrail.test.file;

import static org.junit.Assert.*;


import org.junit.Test;
/**
 * Test for the class Helper of the package file
 * @author marilou
 *
 */
public class HelperTest {

	@Test
	public void testGetNextUncommentedLine(){
		//return null if line null before find the correct line
		//else return line
		
		char firstCharacter;
		
		// Line
		String line = null;
		
		// Get line
		
		
		
		do
		{
			// Index
			int index = 0;
			
			// Initialize first character
			firstCharacter = '#';
			
			// Read line
			
			if( line == null )
				break;
				
			// Empty line?
			if( line.isEmpty( ) )
				continue;
			
			// Look for the first character
			do
			{
				// Get next character
				firstCharacter = line.charAt( index++ );
			} while( index < line.length( )
					&& Character.isWhitespace( firstCharacter ) );
		} while( firstCharacter == '#' );
		
		assertNull(line);
		// OK
		
		line = "test";
		do
		{
			// Index
			int index = 0;
			
			// Initialize first character
			firstCharacter = '#';
			
			// Read line
			if( line == null )
				break;
			
			// Empty line?
			if( line.isEmpty( ) )
				continue;
			
			// Look for the first character
			do
			{
				// Get next character
				firstCharacter = line.charAt( index++ );
			} while( index < line.length( )
					&& Character.isWhitespace( firstCharacter ) );
		} while( firstCharacter == '#' );

		assertNotNull(line);
		
		line = "#bonjour";
		int i = 0;
		do
		{
			// Index
			int index = 0;
			
			// Initialize first character
			firstCharacter = '#';
			
			// Read line
			if( line == null )
				break;
			
			// Empty line?
			if( line.isEmpty( ) )
				continue;
			
			// Look for the first character
			do
			{
				// Get next character
				firstCharacter = line.charAt( index++ );
			} while( index < line.length( )
					&& Character.isWhitespace( firstCharacter ) );
			
			line = "#bonjour à nouveau";
			i++;
			if( i >= 50)
			{
				line = "c'est fini";
			}
			else
			{
				assertSame('#',firstCharacter);
			}	
		} while( firstCharacter == '#' );
		
		assertNotSame('#', firstCharacter);
		assertNotSame('#', line.charAt(0));
		
	}

}
