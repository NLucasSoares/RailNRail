package railnrail.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FileTests.class, MathSpeedTests.class, NetworkTests.class, TrainTests.class, TrainIntegrationTest.class })
/**
 * TestSuite of all TestCases and TestSuites
 * @author marilou
 *
 */
public class UnitTests {

}
