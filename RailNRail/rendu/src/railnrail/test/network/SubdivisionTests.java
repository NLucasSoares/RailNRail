package railnrail.test.network;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.network.subdivision.SubdivisionLimitTest;
import railnrail.test.network.subdivision.SubdivisionTest;

@RunWith(Suite.class)
@SuiteClasses({ SubdivisionLimitTest.class, SubdivisionTest.class })
/**
 * TestSuite of the package network.subdivision
 * @author marilou
 *
 */
public class SubdivisionTests {

}
