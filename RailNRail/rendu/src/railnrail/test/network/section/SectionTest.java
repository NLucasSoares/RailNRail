package railnrail.test.network.section;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.Random;

import org.junit.Test;

import railnrail.network.Line;
import railnrail.network.section.Canton;
import railnrail.network.section.Section;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.network.type.RailType;
/**
 * 
 * @author marilou
 *TestCase for class Section of package network
 *
 */
public class SectionTest {

	@Test
	/**
	 * Test for constructor Section()
	 */
	public void testSection() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed, nickname);
		Section s = new Station(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed, nickname);
		assertNotNull(c);
		assertNotNull(s);
	}

	@Test
	/**
	 * Test for the method addRail()
	 */
	public void testAddRail() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		Random r = new Random();
		int a = r.nextInt(10)+1;
		int b = r.nextInt(10+1);
		for(int i = 0; i < a; i++)
		{
			c.addRail(i, RailDirection.RAIL_DIRECTION_DEPARTURE);
			/*
			 * 	ArrayList<Rail>[ ] rail = (ArrayList<Rail>[ ])new ArrayList[ RailDirection.values( ).length ];
			 *	for( int i = 0; i < RailDirection.values( ).length; i++ )
			 *	{
			 *	rail[ i ] = new ArrayList<Rail>( );
			 *	}
			 *Adapter pour virer le getRail()
			 */
			assertNotNull(c.getRail(RailDirection.RAIL_DIRECTION_DEPARTURE, i));
		}
		for(int j = 0; j < b; j++)
		{
			c.addRail(j, RailDirection.RAIL_DIRECTION_ARRIVAL);
			assertNotNull(c.getRail(RailDirection.RAIL_DIRECTION_ARRIVAL, j));
		}
		
		//Ajout de la possibilité d'erreur dans l'id (id unique pour chaque rail => doublon == erreur)
		
	}

	@Test
	/**
	 * test for the method getRail() with parameters RailDirection direction and int index
	 */
	public void testGetRailRailDirectionInt() {
		//testAddRail vérifie déjà si Get ne renvoit pas quelque chose de null
	}

	@Test
	/**
	 * test for the method getRail() with parameters RailDirection direction
	 */
	public void testGetRailRailDirection() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		
		assertNotNull(c.getRail(RailDirection.RAIL_DIRECTION_DEPARTURE));
		assertNotNull(c.getRail(RailDirection.RAIL_DIRECTION_ARRIVAL));
	}

	@Test
	/**
	 * test for the method getRailCount() with parameters RailDirection direction
	 */
	public void testGetRailCountRailDirection() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		
		Random r = new Random();
		int a = r.nextInt(10)+1;
		for(int b = 0; b < a; b++)
		{
			c.addRail(b, RailDirection.RAIL_DIRECTION_DEPARTURE);
			assertNotNull(c.getRail(RailDirection.RAIL_DIRECTION_DEPARTURE, b));
		}
		assertEquals(a,c.getRailCount(RailDirection.RAIL_DIRECTION_DEPARTURE));
	}

	@Test
	/**
	 * test for the method getRailCount()
	 */
	public void testGetRailCount() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		
		Random r = new Random();
		int a = r.nextInt(10)+1;
		int b = r.nextInt(10+1);
		for(int i = 0; i < a; i++)
		{
			c.addRail(i, RailDirection.RAIL_DIRECTION_DEPARTURE);
			assertNotNull(c.getRail(RailDirection.RAIL_DIRECTION_DEPARTURE, i));
		}
		for(int j = 0; j < b; j++)
		{
			c.addRail(j, RailDirection.RAIL_DIRECTION_ARRIVAL);
			assertNotNull(c.getRail(RailDirection.RAIL_DIRECTION_ARRIVAL, j));
		}
		a += b;
		b = c.getRailCount(RailDirection.RAIL_DIRECTION_DEPARTURE) + c.getRailCount(RailDirection.RAIL_DIRECTION_ARRIVAL);
		assertEquals(a,b);
		assertEquals(b,c.getRailCount());
	}

	@Test
	/**
	 * test for the method getNighSectionCount()
	 */
	public void testGetNighSectionCount() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		
		Random r = new Random();
		int a = r.nextInt(10)+1;
		for(int b = 0; b < a; b++)
		{
			c.addConnectionToSection(RailDirection.RAIL_DIRECTION_DEPARTURE,c.getId());
			assertNotNull(c.getNighSection(RailDirection.RAIL_DIRECTION_DEPARTURE, b));
		}
		assertEquals(a,c.getNighSectionCount(RailDirection.RAIL_DIRECTION_DEPARTURE));
	}

	@Test
	/**
	 * test for the method addConnectionToSection()
	 */
	public void testAddConnectionToSection() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		Random r = new Random();
		int a = r.nextInt(10)+1;
		int b = r.nextInt(10)+1;
		for(int i = 0; i < a; i++)
		{
			c.addConnectionToSection(RailDirection.RAIL_DIRECTION_DEPARTURE, c.getId());
			
			assertNotNull(c.getNighSection(RailDirection.RAIL_DIRECTION_DEPARTURE, i));
		}
		for(int j = 0; j < b; j++)
		{
			c.addConnectionToSection(RailDirection.RAIL_DIRECTION_ARRIVAL, c.getId());
			assertNotNull(c.getNighSection(RailDirection.RAIL_DIRECTION_ARRIVAL, j));
		}
	}

	@Test
	/**
	 * test for the method getNighSection()
	 */
	public void testGetNighSection() {
		//Test effectué avec AddConnectionToSection
	}

	@Test
	public void testGetId() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		Random r = new Random();
		id = r.nextInt();
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		assertEquals(id,c.getId());
		
	}

	@Test
	/**
	 * test for the method getName()
	 */
	public void testGetName() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railType = null;
		Point position = null;
		int maximumSpeed = 0;
		Random r = new Random();
		int a = r.nextInt();
		name = Integer.toString(a);
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railType,position,maximumSpeed,nickname);
		assertNotNull(c.getName());
		assertTrue(a == (Integer.parseInt(c.getName())));
	}

	@Test
	/**
	 * test for getRailType()
	 */
	public void testGetRailType() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		Point position = null;
		int maximumSpeed = 0;
		RailType rt1 = RailType.RAIL_BOTTOM_TO_RIGHT;
		RailType rt2 = RailType.RAIL_END_LEFT;
		RailType rt3 = RailType.RAIL_END_RIGHT;
		RailType rt4 = RailType.RAIL_HORIZONTAL;
		RailType rt5 = RailType.RAIL_HORIZONTAL_AND_LEFT_TO_BOTTOM;
		RailType rt6 = RailType.RAIL_LEFT_TO_BOTTOM;
		RailType rt7 = RailType.RAIL_LEFT_TO_TOP;
		RailType rt8 = RailType.RAIL_TOP_TO_RIGHT;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,rt1,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_BOTTOM_TO_RIGHT,c.getRailType());
		c = new Canton(l,name,size,travelCode,id,isTerminus,rt2,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_END_LEFT,c.getRailType());
		c = new Canton(l,name,size,travelCode,id,isTerminus,rt3,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_END_RIGHT,c.getRailType());
		c = new Canton(l,name,size,travelCode,id,isTerminus,rt4,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_HORIZONTAL,c.getRailType());
		c = new Canton(l,name,size,travelCode,id,isTerminus,rt5,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_HORIZONTAL_AND_LEFT_TO_BOTTOM,c.getRailType());
		c = new Canton(l,name,size,travelCode,id,isTerminus,rt6,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_LEFT_TO_BOTTOM,c.getRailType());
		c = new Canton(l,name,size,travelCode,id,isTerminus,rt7,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_LEFT_TO_TOP,c.getRailType());
		c = new Canton(l,name,size,travelCode,id,isTerminus,rt8,position,maximumSpeed,nickname);
		assertEquals(RailType.RAIL_TOP_TO_RIGHT,c.getRailType());
}

	@Test
	/**
	 * test for the method getPosition()
	 */
	public void testGetPosition() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railtype = null;
		Point position = null;
		int maximumSpeed = 0;
		Random r = new Random();
		int a = r.nextInt();
		int b = r.nextInt();
		
		Point p = new Point(a,b);
		position = p;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railtype,position,maximumSpeed,nickname);
		assertEquals(p,c.getPosition());
	}

	@Test
	/**
	 * test for the method getSize()
	 */
	public void testGetSize() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railtype = null;
		Point position = null;
		int maximumSpeed = 0;
		Random r = new Random();
		int a = r.nextInt();
		size = a;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railtype,position,maximumSpeed,nickname);
		assertEquals(a,c.getSize());
	}

	@Test
	/**
	 * test for the method isTerminus()
	 */
	public void testIsTerminus() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railtype = null;
		Point position = null;
		int maximumSpeed = 0;
		Random r = new Random();
		Boolean a = r.nextBoolean();
		isTerminus = a;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railtype,position,maximumSpeed,nickname);
		assertTrue(a == c.isTerminus());
	}

	@Test
	/**
	 * test for the method getTravelCode()
	 */
	public void testGetTravelCode() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railtype = null;
		Point position = null;
		int maximumSpeed = 0;
		Random r = new Random();
		int a = r.nextInt();
		travelCode = Integer.toString(a);
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railtype,position,maximumSpeed,nickname);
		assertNotNull(c.getTravelCode());
		assertEquals(a,Integer.parseInt(c.getTravelCode()));
	}

	@Test
	/**
	 * test for the method get ParentLine
	 */
	public void testGetParentLine() {
		
	}

	@Test
	/**
	 * test for the getMaximumSpeed()
	 */
	public void testGetMaximumSpeed() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railtype = null;
		Point position = null;
		int maximumSpeed = 0;
		Random r = new Random();
		int a = r.nextInt();
		maximumSpeed = a;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railtype,position,maximumSpeed,nickname);
		assertEquals(a,c.getMaximumSpeed());
	}

	@Test
	/**
	 * test for the getFreeRail()
	 */
	public void testGetFreeRail() {
		Line l = null;
		String name = null;
		int size = 0;
		String travelCode = null;
		int id = 0;
		boolean isTerminus = false;
		RailType railtype = null;
		Point position = null;
		int maximumSpeed = 0;
		String nickname = null;
		Section c = new Canton(l,name,size,travelCode,id,isTerminus,railtype,position,maximumSpeed,nickname);
		c.addRail(0,RailDirection.RAIL_DIRECTION_ARRIVAL);
		Rail r = c.getRail(RailDirection.RAIL_DIRECTION_ARRIVAL,0);
		Boolean b = r.isTrainPresent();
		if (b)
			assertNull(c.getFreeRail(RailDirection.RAIL_DIRECTION_ARRIVAL));
		else
			assertEquals(r, c.getFreeRail(RailDirection.RAIL_DIRECTION_ARRIVAL));
	}
	//Trouver le moyen d'initialiser comme on veut le rail (rajouter une méthode AddRail qui permet d'envoyer le rail que l'on souhaite)
}
