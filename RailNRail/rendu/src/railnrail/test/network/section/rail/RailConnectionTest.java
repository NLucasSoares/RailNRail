package railnrail.test.network.section.rail;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailConnection;
import railnrail.network.section.rail.RailDirection;
/**
 * 
 * @author marilou
 *
 * TestCase for the class RailConnection of the package network.section.rail
 */
public class RailConnectionTest {

	@Test
	/**
	 * test for the method railConnection()
	 */
	public void testRailConnection()
	{

	}

	@Test
	/**
	 * test for the method addConnection()
	 */
	public void testAddConnection() {
		RailDirection direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
		Random rand = new Random();
		Rail r = new Rail(rand.nextInt(),null , direction);
		
		RailConnection railconnection = new RailConnection(r);
		railconnection.addConnection(direction, r);
		assertNotNull(railconnection.getRail(direction, 0));
		assertEquals(r,railconnection.getRail(direction, 0));
		assertEquals(r.getID(),railconnection.getRail(direction, 0).getID());
	}

	@Test
	/**
	 * test for the method getConnectionCount()
	 */
	public void testGetConnectionCount() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getRail
	 */
	public void testGetRail() {

		RailConnection railConnection = new RailConnection(null);
		Rail rail;
		RailDirection direction;
		
		for(int j = 0; j < 50; j++)
		{
			direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
			rail = new Rail(j, null, direction);
			railConnection.addConnection(direction, rail);
		}
		
		for(int i = 0; i<100;i++)
		{
			direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
			if (i<50)
				assertNotNull(railConnection.getRail(direction, i));
			else
				assertNull(railConnection.getRail(direction, i));
			
			direction = RailDirection.RAIL_DIRECTION_DEPARTURE;
				assertNull(railConnection.getRail(direction, i));
		}
		
			for(int j = 0; j < 50; j++)
			{
				direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
				rail = new Rail(j, null, direction);
				railConnection.addConnection(direction, rail);
				direction = RailDirection.RAIL_DIRECTION_DEPARTURE;
				rail = new Rail(j, null, direction);
				railConnection.addConnection(direction, rail);
			}
			
			for(int i = 0; i<100;i++)
			{
				direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
				if (i<railConnection.getConnectionCount(RailDirection.RAIL_DIRECTION_ARRIVAL))
					assertNotNull(railConnection.getRail(direction, i));
				else
					assertNull("i est egal à "+i,railConnection.getRail(direction, i));
				
				direction = RailDirection.RAIL_DIRECTION_DEPARTURE;
				if (i<railConnection.getConnectionCount(RailDirection.RAIL_DIRECTION_DEPARTURE))
					assertNotNull(railConnection.getRail(direction, i));
				else
					assertNull(railConnection.getRail(direction, i));
			}
	}

	@Test
	/**
	 * test for the method getRailConnectedToDestination
	 */
	public void testGetRailConnectedToDestination() {
		
	}

}
