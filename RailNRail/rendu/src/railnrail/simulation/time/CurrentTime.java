package railnrail.simulation.time;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Time gesture
 * 
 * @author Lucas SOARES
 */
public class CurrentTime
{
	/**
	 * Initial timestamp
	 */
	public static final long INITIAL_TIMESTAMP = 1483315200;
	
	/**
	 * Seconds in day
	 */
	public static final long SECONDS_IN_DAY = 86400;
	
	/**
	 * Seconds in hour
	 */
	public static final long SECONDS_IN_HOUR = 3600;
	
	/**
	 * Seconds in minute
	 */
	public static final long SECONDS_IN_MINUTE = 60;
	
	/**
	 * Seconds in week
	 */
	public static final long SECONDS_IN_WEEK = 604800;

	/**
	 * Time in seconds (timestamp)
	 */
	private double time;
	
	/**
	 * Delta time (time modulated one week)
	 */
	private long deltaTime = 0;
	
	/**
	 * Last delta time
	 */
	private long lastDeltaTime = 0;
	
	/**
	 * Is new week?
	 */
	private boolean isNewWeek = false;
	
	/**
	 * Current day
	 */
	private Day day;

	/**
	 * Last update time
	 */
	private long lastUpdateTime;
	
	/**
	 * Is paused?
	 */
	private boolean isPaused;
	
	/**
	 * Day formated string
	 */
	private String dateFormatted;
	
	/**
	 * Construct time
	 */
	public CurrentTime( )
	{
		// Starting being paused
		isPaused = true;
		
		// Zero
		time = 0.0d;
	}
	
	/**
	 * Update
	 * 
	 * @param updateSpeed
	 * 		The update speed
	 */
	public void update( int updateSpeed )
	{
		// Is paused?
		if( isPaused )
			return;
		
		// Time elapsed since last update
		long timeElapsed = System.currentTimeMillis( ) - lastUpdateTime;
				
		// Calculate time increase
		time += ( ( timeElapsed * 0.001d ) * updateSpeed );
		
		// Save the update time
		lastUpdateTime = System.currentTimeMillis( );
		
		// Create time
		updateFormattedTime( );
		
		// Save last delta time
		lastDeltaTime = deltaTime;
		
		// Calculate delta time
		deltaTime = ( CurrentTime.INITIAL_TIMESTAMP + ( (long)time % CurrentTime.SECONDS_IN_WEEK ) ) * 1000;
	
		// Is this a new week?
		isNewWeek = ( deltaTime < lastDeltaTime );
	}
	
	/**
	 * Pause
	 */
	public void pause( )
	{
		// Pause
		isPaused = true;
	}
	
	/**
	 * Resume
	 */
	public void resume( )
	{
		// Save
		lastUpdateTime = System.currentTimeMillis( );
		isPaused = false;
	}
	
	/**
	 * Get current time
	 * 
	 * @return the current time
	 */
	public long getCurrentTime( )
	{
		return (long)time;
	}
	
	/**
	 * Get day
	 * 
	 * @return the day
	 */
	public Day getDay( )
	{
		return day;
	}
	
	/**
	 * Get formatted current time
	 * 
	 * @return the formatted current time
	 */
	public String getFormattedCurrentTime( )
	{
		return dateFormatted;
	}
	
	/**
	 * Is new week?
	 * 
	 * @return if this is a new week
	 */
	public boolean isNewWeek( )
	{
		return isNewWeek;
	}
	
	/**
	 * Reset new week indicator
	 */
	public void resetIsNewWeek( )
	{
		isNewWeek = false;
	}
	
	/**
	 * Get the delta time (time moduled on one week)
	 * 
	 * @return the delta time
	 */
	public long getDeltaTime( )
	{
		return deltaTime;
	}
	
	/**
	 * Update formatted time
	 */
	public void updateFormattedTime( )
	{
		// Convert timestamp in date
		Date clock = new Date( ( CurrentTime.INITIAL_TIMESTAMP + (long)time ) * 1000 );
		
		// Get current day
		Format dayFormat = new SimpleDateFormat( "EEEE",
				Locale.ENGLISH );
		day = Day.getDay( dayFormat.format( clock ) );
				
		// Format
		Format dateFormat = new SimpleDateFormat( "dd/MM/y HH:mm:ss" );
		dateFormatted = Day.Name[ day.ordinal( ) ] + " " +  dateFormat.format( clock );
	}
}
