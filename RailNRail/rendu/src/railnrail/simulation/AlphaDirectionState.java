package railnrail.simulation;

import railnrail.gui.simulation.SimulationDashboardPanel;

/**
 * Alpha gesture for direction arrow on rails
 * 
 * @author Lucas SOARES
 */
public class AlphaDirectionState
{
	/**
	 * Alpha changing rate
	 */
	private static final float DIRECTION_ALPHA_CHANGING_RATE = 0.002f;
	
	/**
	 * Minimum alpha threshold
	 */
	private static final float DIRECTION_ALPHA_MINIMUM_THRESHOLD = 0.4f;
	
	/**
	 * Maximum alpha threshold
	 */
	private static final float DIRECTION_ALPHA_MAXIMUM_THRESHOLD = 0.8f;
	
	/**
	 * Direction alpha changing direction
	 */
	private boolean directionAlphaDirection = false;
	
	/**
	 * Direction alpha
	 */
	private float directionAlpha = DIRECTION_ALPHA_MINIMUM_THRESHOLD;
	
	/**
	 * Update
	 * 
	 * @param dashboard
	 * 		The dashboard
	 */
	public void update( SimulationDashboardPanel dashboard )
	{
		// Alpha increment
		if( directionAlphaDirection )
		{
			if( directionAlpha >= DIRECTION_ALPHA_MAXIMUM_THRESHOLD )
				directionAlphaDirection = !directionAlphaDirection;
			else
				directionAlpha += DIRECTION_ALPHA_CHANGING_RATE;
		}
		// Alpha decrement
		else
		{
			if( directionAlpha <= DIRECTION_ALPHA_MINIMUM_THRESHOLD )
				directionAlphaDirection = !directionAlphaDirection;
			else
				directionAlpha -= DIRECTION_ALPHA_CHANGING_RATE;
		}
		
		// Set alpha
		dashboard.getLineDisplayPanel( ).setDirectionAlpha( directionAlpha );
	}
}
