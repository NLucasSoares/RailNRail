package railnrail.network.builder;

/**
 * The differents types of the section
 * 
 * @author Lucas SOARES
 */
public enum SectionType
{
	SECTION_TYPE_STATION,
	SECTION_TYPE_CANTON;
	
	/**
	 * Name of the type
	 */
	public final static String[ ] Name =
	{
		"Gare",
		"Canton"
	};
}
