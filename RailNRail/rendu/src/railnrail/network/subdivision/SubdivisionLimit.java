package railnrail.network.subdivision;

/**
 * Subdivision limits
 * 
 * @author Lucas SOARES
 */
public class SubdivisionLimit
{
	/**
	 * Begin id
	 */
	private int beginID;
	
	/**
	 * End id
	 */
	private int endID;
	
	/**
	 * Construct subdivision limit
	 * 
	 * @param beginID
	 * 		Begin section id
	 * @param endID
	 * 		End section id
	 */
	public SubdivisionLimit( int beginID,
			int endID )
	{
		// Save
		this.beginID = beginID;
		this.endID = endID;
	}
	
	/**
	 * Get begin ID
	 * 
	 * @return the begin ID
	 */
	public int getBeginID( )
	{
		return beginID;
	}
	
	/**
	 * Get end ID
	 * 
	 * @return the end ID
	 */
	public int getEndID( )
	{
		return endID;
	}
}
