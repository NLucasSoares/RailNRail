package railnrail.network.subdivision;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Subdivision of line
 * 
 * @author Lucas SOARES
 */
public class Subdivision
{
	/**
	 * Subdivision limits
	 */
	private ArrayList<SubdivisionLimit> subdivision = new ArrayList<SubdivisionLimit>( );
	
	/**
	 * Construct subdivision
	 * 
	 * @param configLine
	 * 		The line coming from line definition
	 * 
	 * @throws IOException
	 */
	public Subdivision( String configLine ) throws IOException
	{
		// Divide line
		String[ ] properties = configLine.split( "," );
		
		// Iterate properties
		for( String division : properties )
		{
			// Limit
			String[ ] limit = division.split( "-" );
			
			// Add subdivision limit
			subdivision.add( new SubdivisionLimit( Integer.parseInt( limit[ 0 ] ),
					Integer.parseInt( limit[ 1 ] ) ) );
		}
	}
	
	/**
	 * Get subdivision limit count
	 * 
	 * @return the subdivision limit count
	 */
	public int getSubdivisionLimitCount( )
	{
		return subdivision.size( );
	}
	
	/**
	 * Get subdivision limit
	 * 
	 * @param index
	 * 		The subdivision index
	 * 		
	 * @return the subdivision limit
	 */
	public SubdivisionLimit getSubdivisionLimit( int index )
	{
		return subdivision.get( index );
	}
}
