package railnrail.network.section;

import java.awt.Point;

import railnrail.incident.Incident;
import railnrail.incident.IncidentHolder;
import railnrail.incident.station.StationIncident;
import railnrail.network.Line;
import railnrail.network.section.prevision.PrevisionList;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.network.type.RailType;
import railnrail.train.Train;
import railnrail.train.prevision.ArrivalPrevision;

/**
 * A station
 * 
 * @author Lucas SOARES
 */
public class Station extends Section
	implements IncidentHolder
{
	/**
	 * Arrival prevision
	 */
	private PrevisionList previsionList;
	
	/**
	 * Incident
	 */
	private StationIncident incident;
	
	/**
	 * Construct a station
	 * 
	 * @param parentLine
	 * 		The parent line
	 * @param name
	 * 		The name of the station
	 * @param size
	 * 		The size of the station
	 * @param travelCode
	 * 		The travel code
	 * @param id
	 * 		The section id
	 * @param isTerminus
	 * 		Is this section the last one
	 * @param railType
	 * 		The type of rail
	 * @param position
	 * 		The position
	 * @param maximumSpeed
	 * 		The maximum speed
	 * @param nickname
	 * 		The nickname
	 */
	public Station( Line parentLine,
			String name,
			int size,
			String travelCode,
			int id,
			boolean isTerminus,
			RailType railType,
			Point position,
			int maximumSpeed,
			String nickname )
	{
		// Parent constructor
		super( parentLine,
				name,
				size,
				travelCode,
				id,
				isTerminus,
				railType,
				position,
				maximumSpeed,
				nickname );
		
		// Build prevision list
		previsionList = new PrevisionList( );
	}

	/**
	 * Add prevision
	 * 
	 * @param train
	 * 		The train concerned
	 * @param prevision
	 * 		The prevision
	 */
	public void addArrivalPrevision( Train train,
			ArrivalPrevision prevision )
	{
		// Add prevision
		previsionList.addPrevision( train,
				prevision );
	}
	
	/**
	 * Get previsions
	 * 
	 * @return the previsions
	 */
	public ArrivalPrevision[ ] getPrevision( )
	{
		return previsionList.getPrevision( );
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Parent update
		super.update( );
		
		// Update prevision
		previsionList.update( );
		
		// Incident occuring?
		if( incident != null )
		{
			// Check for incident end
			if( incident.isPassed( ) )
			{
				// No more incident
				incident = null;
				
				// Unblock rails
				setBlockRail( false );
			}
			// Incident is happening
			else
				// Block all the rails
				setBlockRail( true );
		}
		else
			setBlockRail( false );
	}

	/**
	 * Set block on rail
	 * 
	 * @param isBlocked
	 * 		Is rail blocked?
	 */
	public void setBlockRail( boolean isBlocked )
	{
		// Iterate direction
		for( RailDirection direction : RailDirection.values( ) )
			// Iterate rail
			for( Rail rail : getRail( direction ) )
				// Set block
				rail.setBlocked( isBlocked );
	}
	
	/**
	 * Start an incident
	 * 
	 * @param incident
	 *		The incident
	 */
	@Override
	public void startIncident( Incident incident )
	{
		// Save
		this.incident = (StationIncident)incident;
	}
	
	/**
	 * Is an incident occuring?
	 * 
	 * @return if incident is occuring
	 */
	public boolean isIncidentOccuring( )
	{
		return incident != null;
	}
	
	/**
	 * Get incident
	 * 
	 * @return the incident occuring
	 */
	public StationIncident getIncident( )
	{
		return incident;
	}
}
