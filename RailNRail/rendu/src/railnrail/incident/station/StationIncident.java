package railnrail.incident.station;

import railnrail.incident.Incident;

/**
 * A section incident
 * 
 * @author Lucas SOARES
 */
public class StationIncident extends Incident
{
	/**
	 * Build section incident
	 * 
	 * @param startTime
	 * 		The start time
	 * @param type
	 * 		The incident type
	 * @param object
	 * 		The object to consider
	 */
	public StationIncident( long startTime,
			Object type,
			Object object )
	{
		// Parent constructor
		super( startTime,
				type,
				object );
		
		// Determine length
		length = StationIncidentList.MinimalLength[ ( (StationIncidentList)type ).ordinal( ) ] + (long)( Math.random( ) * (double)( StationIncidentList.MaximalLength[ ( (StationIncidentList)type ).ordinal( ) ] - StationIncidentList.MinimalLength[ ( (StationIncidentList)type ).ordinal( ) ] ) );
	}

	/**
	 * Get type
	 * 
	 * @return the type
	 */
	public StationIncidentList getType( )
	{
		return (StationIncidentList)type;
	}
}
