package railnrail.incident.station;

/**
 * Section incident enum
 * 
 * @author Lucas SOARES
 */
public enum StationIncidentList
{
	INCIDENT_TRAVELER,
	ELECTRICITY_PROBLEM,
	TERRORISM,
	METEO_ISSUE;
	
	/**
	 * Get value from text
	 * 
	 * @param text
	 * 		The text
	 * 
	 * @return the value
	 */
	public static StationIncidentList getValue( String text )
	{
		// Look for
		for( StationIncidentList out : StationIncidentList.values( ) )
			if( StationIncidentList.Name[ out.ordinal( ) ].equals( text ) )
				return out;
		
		// Can't find
		return null;
	}
	
	/**
	 * Name of each incident type
	 */
	public static final String[ ] Name =
	{
		"Traveler",
		"Electricity",
		"Terrorism",
		"Meteo issue",
	};
	
	/**
	 * Minimal length (seconds) of each incident type
	 */
	public static final long[ ] MinimalLength =
	{
		1800,
		1900,
		4000,
		3600
	};
	
	/**
	 * Maximal length
	 */
	public static final long[ ] MaximalLength =
	{
		2300,
		2000,
		4500,
		4000
	};
}
