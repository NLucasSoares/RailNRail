package railnrail.incident.train;

import railnrail.incident.Incident;

/**
 * A train incident
 * 
 * @author Lucas SOARES
 */
public class TrainIncident extends Incident
{
	/**
	 * Build train incident
	 * 
	 * @param startTime
	 * 		The start time
	 * @param type
	 * 		The incident type
	 * @param object
	 * 		The object to consied
	 */
	public TrainIncident( long startTime,
			Object type,
			Object object )
	{
		// Parent constructor
		super( startTime,
				type,
				object );
		
		// Determine length
		length = TrainIncidentList.MinimalLength[ ( (TrainIncidentList)type ).ordinal( ) ] + (long)( Math.random( ) * (double)( TrainIncidentList.MaximalLength[ ( (TrainIncidentList)type ).ordinal( ) ] - TrainIncidentList.MinimalLength[ ( (TrainIncidentList)type ).ordinal( ) ] ) );
	}
	
	/**
	 * Get type
	 * 
	 * @return the type
	 */
	public TrainIncidentList getType( )
	{
		return (TrainIncidentList)type;
	}

}
