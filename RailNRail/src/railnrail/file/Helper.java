package railnrail.file;

import java.io.BufferedReader;
import java.io.IOException;

/**
 *	File helper
 *
 *	@author SOARES Lucas
 */
public class Helper
{
	/**
	 * Get next line usable
	 * 
	 * @param f
	 * 		The stream to be read from
	 * 
	 * @return the line
	 */
	public static String getNextUncommentedLine( BufferedReader f ) throws IOException
	{
		// First character found
		char firstCharacter;
		
		// Line
		String line;
		
		// Get line
		do
		{
			// Index
			int index = 0;
			
			// Initialize first character
			firstCharacter = '#';
			
			// Read line
			if( ( line = f.readLine( ) ) == null )
				return null;
			
			// Empty line?
			if( line.isEmpty( ) )
				continue;
			
			// Look for the first character
			do
			{
				// Get next character
				firstCharacter = line.charAt( index++ );
			} while( index < line.length( )
					&& Character.isWhitespace( firstCharacter ) );
		} while( firstCharacter == '#' );
		
		// OK
		return line;
	}
}

