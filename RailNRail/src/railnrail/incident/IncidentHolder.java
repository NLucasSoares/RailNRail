package railnrail.incident;

/**
 * Implemented by classes which can declare incidents
 * 
 * @author Lucas SOARES
 */
public interface IncidentHolder
{
	/**
	 * Start an incident
	 * 
	 * @param incident
	 * 		The incident to start
	 */
	public abstract void startIncident( Incident incident );
}
