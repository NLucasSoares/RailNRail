package railnrail.incident.rail;

import railnrail.incident.Incident;

/**
 * A rail incident
 * 
 * @author Lucas SOARES
 */
public class RailIncident extends Incident
{
	/**
	 * Build a rail incident
	 * 
	 * @param startTime
	 * 		The start time
	 * @param type
	 * 		The type of incident
	 * @param object
	 * 		The object to consider
	 */
	public RailIncident( long startTime,
			Object type,
			Object object )
	{
		// Parent constructor
		super( startTime,
				type,
				object );
		
		// Determine length
		length = RailIncidentList.MinimalLength[ ( (RailIncidentList)type ).ordinal( ) ] + (long)( Math.random( ) * (double)( RailIncidentList.MaximalLength[ ( (RailIncidentList)type ).ordinal( ) ] - RailIncidentList.MinimalLength[ ( (RailIncidentList)type ).ordinal( ) ] ) );
	}

	/**
	 * Get type
	 * 
	 * @return the type
	 */
	public RailIncidentList getType( )
	{
		return (RailIncidentList)type;
	}
}
