package railnrail.incident.rail;

/**
 * Rail incident enum
 * 
 * @author Lucas SOARES
 */
public enum RailIncidentList
{
	RAILROAD_CROSSING,
	ELECTRICITY_PROBLEM,
	DAMAGED_RAIL;
	
	/**
	 * Get value from text
	 * 
	 * @param text
	 * 		The text
	 * 
	 * @return the value
	 */
	public static RailIncidentList getValue( String text )
	{
		// Look for
		for( RailIncidentList out : RailIncidentList.values( ) )
			if( RailIncidentList.Name[ out.ordinal( ) ].equals( text ) )
				return out;
		
		// Can't find
		return null;
	}
	
	/**
	 * Name of each incident type
	 */
	public static final String[ ] Name =
	{
		"Electricity",
		"Railroad crossing",
		"Damaged rail"
	};
	
	/**
	 * Minimal length (seconds) of each incident type
	 */
	public static final long[ ] MinimalLength =
	{
		550,
		1500,
		1000
	};
	
	/**
	 * Maximal length
	 */
	public static final long[ ] MaximalLength =
	{
		650,
		2000,
		2000
	};
}
