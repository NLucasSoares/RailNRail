package railnrail.incident.train;

/**
 * Train incident enum
 * 
 * @author Lucas SOARES
 */
public enum TrainIncidentList
{
	LATE_DRIVER,
	CANCELED_TRAIN,
	ENGINE_FAILURE,
	PASSENGER_DISCOMFORT;
	
	/**
	 * Get value from text
	 * 
	 * @param text
	 * 		The text
	 * 
	 * @return the value
	 */
	public static TrainIncidentList getValue( String text )
	{
		// Look for
		for( TrainIncidentList out : TrainIncidentList.values( ) )
			if( TrainIncidentList.Name[ out.ordinal( ) ].equals( text ) )
				return out;
		
		// Can't find
		return null;
	}
	
	/**
	 * Name of each incident type
	 */
	public static final String[ ] Name =
	{
		"Late driver",
		"Canceled train",
		"Engine failure",
		"Passenger discomfort"
	};
	
	/**
	 * Minimal length (seconds) of each incident type
	 */
	public static final long[ ] MinimalLength =
	{
		300,
		0,
		500,
		600
	};
	
	/**
	 * Maximal length
	 */
	public static final long[ ] MaximalLength =
	{
		500,
		0,
		600,
		700
	};
}
