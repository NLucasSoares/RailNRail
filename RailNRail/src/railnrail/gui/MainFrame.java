package railnrail.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import railnrail.gui.simulation.SimulationDashboardPanel;
import railnrail.network.Line;
import railnrail.simulation.Simulation;

/**
 * Frame we're going to use to display all the panels.
 * 
 * @author Gilles Santos
 */
public class MainFrame extends JFrame
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -6713622358840102637L;

	/**
	 * Simulation title
	 */
	private static final String SIMULATION_TITLE = "RailNRail";
	
	/**
	 * Dashboard
	 */
	private SimulationDashboardPanel dashboard;
	
	/**
	 * Call methods to add our components to their respective panel. Initialize our main panel (mainMenuPanel) and add all
	 * our panels to this main panel. Then show the GUI.
	 * 
	 * @param line
	 * 		The line
	 * @param simulation
	 * 		The simulation gesture instance
	 */
	public MainFrame( Line line,
			Simulation simulation )
	{
		// Parent constructor
		super( MainFrame.SIMULATION_TITLE );
		
		// Add components and actionlistener to the simulationDashboard panel.
		try
		{
			// Create dashboard
			dashboard = new SimulationDashboardPanel( line,
					simulation,
					this );
		}
		catch( IOException e )
		{

		}
		
		// Create main panel
		JPanel mainFramePanel = new JPanel( );

		// Set size
		mainFramePanel.setPreferredSize( new Dimension( 1024,
				600 ) );
		
		// Set layout
		mainFramePanel.setLayout( new BorderLayout( ) );
		
		// Add split panel
		mainFramePanel.add( dashboard );

		// Add contPanel to our frame and set the GUI visible at the center of the screen.
		add( mainFramePanel );
		setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		pack( );
		setVisible( true );
		setLocationRelativeTo( null );
		
		// Window close event
		addWindowListener( new WindowListener( )
		{
			@Override
			public void windowActivated( WindowEvent arg0 )
			{
			}

			@Override
			public void windowClosed( WindowEvent arg0 )
			{
				simulation.stop( );
			}

			@Override
			public void windowClosing( WindowEvent arg0 )
			{
			}

			@Override
			public void windowDeactivated( WindowEvent arg0 )
			{
			}

			@Override
			public void windowDeiconified( WindowEvent arg0 )
			{
			}

			@Override
			public void windowIconified( WindowEvent arg0 )
			{
			}

			@Override
			public void windowOpened( WindowEvent arg0 )
			{
			}
	
		} );
	}
	
	/**
	 * Get simulation dashboard
	 * 
	 * @return simulation dashboard
	 */
	public SimulationDashboardPanel getSimulationDashboard( )
	{
		return dashboard;
	}
}
