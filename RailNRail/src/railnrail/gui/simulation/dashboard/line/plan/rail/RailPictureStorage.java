package railnrail.gui.simulation.dashboard.line.plan.rail;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;

import railnrail.network.section.rail.RailDirection;
import railnrail.network.type.EmptyCellType;
import railnrail.network.type.RailType;
import railnrail.res.img.ImageResource;

/**
 * Rail pictures storage
 * 
 * @author Lucas SOARES
 */
public class RailPictureStorage
{
	/**
	 * Rails
	 */
	private BufferedImage[ ] rail = new BufferedImage[ RailType.values( ).length ];
	
	/**
	 * Empty cell
	 */
	private BufferedImage[ ] emptyCell = new BufferedImage[ EmptyCellType.values( ).length ];
	
	/**
	 * Station icon
	 */
	private BufferedImage stationIcon;
	
	/**
	 * Train icons
	 */
	private BufferedImage[ ] trainIcon = new BufferedImage[ 2 ];
	
	/**
	 * Station cells
	 */
	private BufferedImage[ ] stationCell = new BufferedImage[ StationCellType.values( ).length ];
	
	/**
	 * Size of one rail
	 */
	private Point size = new Point( );
	
	/**
	 * Redlight
	 */
	private BufferedImage[ ] redlight = new BufferedImage[ RedlightState.values( ).length ];
	
	/**
	 * Directions
	 */
	private BufferedImage[ ] direction = new BufferedImage[ RailDirection.values( ).length ];
	
	/**
	 * Construct rail storage
	 */
	public RailPictureStorage( ) throws IOException
	{
		// Load images
			// Rails
				for( RailType type : RailType.values( ) )
				{
					// Read image
					rail[ type.ordinal( ) ] = ImageResource.loadImage( ImageResource.values( )[ ImageResource.IMAGE_RAIL_END_LEFT.ordinal( ) + type.ordinal( ) ] );
					
					// If first save size
					if( type.ordinal( ) == 0 )
						size.setLocation( rail[ type.ordinal( ) ].getWidth( ),
								rail[ type.ordinal( ) ].getHeight( ) );
					// Check the size
					else
						if( rail[ type.ordinal( ) ].getWidth( ) != size.x
							|| rail[ type.ordinal( ) ].getHeight( ) != size.y )
							throw new IOException( "Rail " + type + " doesn't have the same size" );
				}
			// Empty cell
				for( EmptyCellType t : EmptyCellType.values( ) )
					emptyCell[ t.ordinal( ) ] = ImageResource.loadImage( ImageResource.values( )[ ImageResource.IMAGE_EMPTY_CELL.ordinal( ) + t.ordinal( ) ] );
			// Station cells
				for( StationCellType t : StationCellType.values( ) )
					stationCell[ t.ordinal( ) ] = ImageResource.loadImage( ImageResource.values( )[ ImageResource.IMAGE_STATION_CELL_EMPTY.ordinal( ) + t.ordinal( ) ] );
			// Station
				stationIcon = ImageResource.loadImage( ImageResource.IMAGE_STATION );
			// Train
				for( int i = 0; i < 2; i++ )
					trainIcon[ i ] = ImageResource.loadImage( ImageResource.values( )[ ImageResource.IMAGE_TRAIN.ordinal( ) + i ] );
			// Redlight
				for( RedlightState state : RedlightState.values( ) )
					redlight[ state.ordinal( ) ] = ImageResource.loadImage( ImageResource.values( )[ ImageResource.IMAGE_GREEN_LIGHT.ordinal( ) + state.ordinal( ) ] );
			// Directions
				for( RailDirection d : RailDirection.values( ) )
					direction[ d.ordinal( ) ] = ImageResource.loadImage( ImageResource.values( )[ ImageResource.IMAGE_LEFT.ordinal( ) + d.ordinal( ) ] );
	}
	
	/**
	 * Get picture of the rail
	 * 
	 * @param type
	 * 		The type of rail
	 * 
	 * @return the picture asked for
	 */
	public BufferedImage getRail( RailType type )
	{
		return rail[ type.ordinal( ) ];
	}
	
	/**
	 * Get empty cell
	 * 
	 * @param type
	 * 		The empty cell type
	 * 
	 * @return the empty cell tile
	 */
	public BufferedImage getEmptyCell( EmptyCellType type )
	{
		return emptyCell[ type.ordinal( ) ];
	}
	
	/**
	 * Get station icon
	 * 
	 * @return station icon
	 */
	public BufferedImage getStationIcon( )
	{
		return stationIcon;
	}
	
	/**
	 * Get train icon
	 * 
	 * @param isSelected
	 * 		Is for selected train?
	 * 
	 * @return train icon
	 */
	public BufferedImage getTrainIcon( boolean isSelected )
	{
		return trainIcon[ isSelected ?
				1
				: 0 ];
	}
	
	/**
	 * Get redlight
	 * 
	 * @param state
	 * 		The redlight state to get
	 * 
	 * @return the redlight
	 */
	public BufferedImage getRedlight( RedlightState state )
	{
		return redlight[ state.ordinal( ) ];
	}
	
	/**
	 * Get station cell
	 * 
	 * @param type
	 * 		The station type
	 * 
	 * @return the cell
	 */
	public BufferedImage getStationCell( StationCellType type )
	{
		return stationCell[ type.ordinal( ) ];
	}
	
	/**
	 * Get size of rail
	 * 
	 * @return the size of rail
	 */
	public Point getSize( )
	{
		return size;
	}
	
	/**
	 * Get direction picture
	 * 
	 * @param direction
	 * 		The direction
	 * 
	 * @return the direction picture
	 */
	public BufferedImage getDirection( RailDirection direction )
	{
		return this.direction[ direction.ordinal( ) ];
	}
}
