package railnrail.gui.simulation.dashboard.line.plan.rail;

/**
 * Station cell type
 * 
 * @author Lucas SOARES
 */
public enum StationCellType
{
	TYPE_EMPTY,
	TYPE_BOTTOM,
	TYPE_TOP;
}
