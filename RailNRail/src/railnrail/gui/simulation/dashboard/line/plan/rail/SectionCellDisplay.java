package railnrail.gui.simulation.dashboard.line.plan.rail;

import java.awt.Color;

import railnrail.network.section.Section;

/**
 * A cell containing a section definition
 * 
 * @author Lucas SOARES
 */
public class SectionCellDisplay
	extends CellDisplay
{
	/**
	 * The section displayed
	 */
	private Section section;
	
	/**
	 * The type
	 */
	private StationCellType type;
	
	/**
	 * Build a section cell
	 * 
	 * @param section
	 * 		The section
	 * @param backgroundColor
	 * 		The background color
	 * @param type
	 * 		The station cell type
	 */
	public SectionCellDisplay( Section section,
			Color backgroundColor,
			StationCellType type )
	{
		// Parent constructor
		super( backgroundColor );
		
		// Save
		this.section = section;
		this.type = type;
	}
	
	/**
	 * Get section
	 * 
	 * @return the section
	 */
	public Section getSection( )
	{
		return section;
	}
	
	/**
	 * Get type
	 * 
	 * @return the type
	 */
	public StationCellType getType( )
	{
		return type;
	}
}
