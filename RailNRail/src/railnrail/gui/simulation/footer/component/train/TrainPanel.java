package railnrail.gui.simulation.footer.component.train;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.List;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import railnrail.gui.simulation.footer.SimulationFooterPanel;
import railnrail.gui.simulation.footer.component.detail.DetailContent;
import railnrail.simulation.Simulation;
import railnrail.train.Train;

/**
 * General informations on the line panel
 * 
 * @author Lucas SOARES
 */
public class TrainPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 8976378362056056746L;
	
	/**
	 * Train list title
	 */
	private static final String TRAIN_LIST_TITLE = "Train list";
	
	/**
	 * Train list font size
	 */
	private static final float TRAIN_LIST_TITLE_FONT_SIZE = 24.0f;
	
	/**
	 * Simulation
	 */
	private Simulation simulation;
	
	/**
	 * Footer panel
	 */
	private SimulationFooterPanel footerPanel;
	
	/**
	 * Train list title
	 */
	private JLabel trainListTitle;
	
	/**
	 * Train list
	 */
	private List trainList;
	
	/**
	 * Construct general informations panel
	 */
	public TrainPanel( Simulation simulation,
			SimulationFooterPanel footerPanel )
	{
		// Set background color
		setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Set margin
		setBorder( BorderFactory.createEmptyBorder( 0,
				10,
				10,
				10 ) );
		
		// Set layout
		setLayout( new BorderLayout( ) );

		// Create train list
		trainList = new List( 5,
				false );
		
		// Add item selection listener
		trainList.addItemListener( new ItemListener( )
		{
			@Override
			public void itemStateChanged( ItemEvent ie )
			{
				// Check if selected
				if( ie.getItemSelectable( ).getSelectedObjects( ).length <= 0 )
					return;
				
				// Focus on train
				focusOnTrain( (String)ie.getItemSelectable( ).getSelectedObjects( )[ 0 ].toString( ) );
			}
		} );
		
		// Create train list title
		trainListTitle = new JLabel( TrainPanel.TRAIN_LIST_TITLE );
		trainListTitle.setForeground( Color.BLACK );
		trainListTitle.setFont( trainListTitle.getFont( ).deriveFont( TrainPanel.TRAIN_LIST_TITLE_FONT_SIZE ) );
		// Parameter train list title
		trainListTitle.setHorizontalAlignment( SwingConstants.CENTER );
		
		// Add list to panel
		add( trainListTitle,
				BorderLayout.NORTH );
		add( trainList,
				BorderLayout.CENTER );
		
		// Save
		this.simulation = simulation;
		this.footerPanel = footerPanel;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Get train list
		@SuppressWarnings( "unchecked" )
		ArrayList<Train> list = (ArrayList<Train>)simulation.getTrainList( ).clone( );
		
		// Compare existing trains
		for( int i = 0; i < list.size( ); i++ )
		{
			// Train
			Train t;
			
			// Get train
			if( i < list.size( ) )
				t = list.get( i );
			else
				break;
			
			// Check
			if( t != null )
			{
				// Create name
				String name = t.toString( );
				
				// Check
				boolean isFound = false;
				for( String trainName : trainList.getItems( ) )
					if( name.equals( trainName ) )
						// We've found the train
						isFound = true;
				
				// Add train
				if( !isFound )
					trainList.add( name );
			}
		}
		
		// Trains to be removed
		boolean[ ] isTrainMustBeRemoved = new boolean[ trainList.getItemCount( ) ];
		
		// Set train removal to all
		for( int i = 0; i < isTrainMustBeRemoved.length; i++ )
			isTrainMustBeRemoved[ i ] = true;
		
		// Compare existing trains
		for( int i = 0; i < trainList.getItemCount( ); i++ )
			// Check
			for( Train t : list )
			{
				// Get train
				Train train = t;
				
				// Train still exist?
				if( trainList.getItem( i ) != null
						&& train != null
						&& trainList.getItem( i ).equals( train.toString( ) ) )
					// We don't want to remove
					isTrainMustBeRemoved[ i ] = false;
			}
		
		// Remove absent trains
		for( int i = 0; i < isTrainMustBeRemoved.length; i++ )
			if( isTrainMustBeRemoved[ i ] )
				if( i < trainList.getItemCount( ) )
					trainList.remove( i );
	}
	
	/**
	 * Focus on train
	 * 
	 * @param train
	 * 		The train to focus on
	 */
	public void focusOnTrain( String train )
	{
		footerPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_TRAIN,
				train );
	}
}
