package railnrail.gui.simulation.footer.component.detail;

/**
 * Empty panel
 * 
 * @author Lucas SOARES
 */
public class EmptyDetailPanel extends BaseDetailPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 7564418128256769061L;

	/**
	 * Panel name
	 */
	public static final String PANEL_NAME = "EmptyPanel";
	
	/**
	 * Build
	 * 
	 * @param detailPanel
	 * 		The parent panel
	 */
	public EmptyDetailPanel( DetailPanel detailPanel )
	{
		super( detailPanel );
	}

	/**
	 * Update
	 */
	@Override
	public void update( )
	{
		
	}

	/**
	 * Activate
	 */
	@Override
	public void activate( )
	{
		
	}

	/**
	 * Deactivate
	 */
	@Override
	public void deactivate( )
	{

	}

}
