package railnrail.gui.simulation.footer.component.detail;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import railnrail.gui.simulation.footer.SimulationFooterPanel;
import railnrail.gui.simulation.incident.IncidentBuilder;
import railnrail.incident.Incident;
import railnrail.network.Line;
import railnrail.network.section.Section;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;
import railnrail.simulation.Simulation;
import railnrail.train.Train;

/**
 * Details for either train or station
 * 
 * @author Lucas SOARES
 */
public class DetailPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -2575315429627809587L;
	
	/**
	 * Panel title font size
	 */
	private static final float PANEL_TITLE_FONT_SIZE = 24.0f;
	
	/**
	 * The line
	 */
	private Line line;
	
	/**
	 * Simulation
	 */
	private Simulation simulation;
	
	/**
	 * Panel title
	 */
	private JLabel panelTitle;
	
	/**
	 * Station detail panel
	 */
	private StationDetailPanel stationDetail;
	
	/**
	 * Train detail panel
	 */
	private TrainDetailPanel trainDetail;
	
	/**
	 * Rail detail panel
	 */
	private RailDetailPanel railDetail;
	
	/**
	 * Incident detail panel
	 */
	private IncidentDetailPanel incidentDetail;
	
	/**
	 * Center panel
	 */
	private JPanel centerPanel;
	
	/**
	 * Focused panel type
	 */
	private DetailContent focusedPanelType = DetailContent.DETAIL_CONTENT_EMPTY;
	
	/**
	 * Incident creation
	 */
	private IncidentBuilder incidentCreation;
	
	/**
	 * Construct detail panel
	 * 
	 * @param line
	 * 		The line
	 * @param simulation
	 * 		The simulation
	 * @param mainFrame
	 * 		The main frame
	 * 
	 * @throws IOException 
	 */
	public DetailPanel( Line line,
			Simulation simulation,
			JFrame mainFrame ) throws IOException
	{
		// Save
		this.line = line;
		this.simulation = simulation;
		
		// Create incident creator
		incidentCreation = new IncidentBuilder( mainFrame,
				simulation );
		
		// Build panels
		stationDetail = new StationDetailPanel( this );
		trainDetail = new TrainDetailPanel( this );
		railDetail = new RailDetailPanel( this );
		incidentDetail = new IncidentDetailPanel( this );
		
		// Set layout
		setLayout( new BorderLayout( ) );
		
		// Set background color
		setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Create title
		panelTitle = new JLabel( "Selected item detail" );
		panelTitle.setForeground( Color.BLACK );
		panelTitle.setFont( panelTitle.getFont( ).deriveFont( DetailPanel.PANEL_TITLE_FONT_SIZE ) );
		panelTitle.setHorizontalAlignment( SwingConstants.CENTER );
		
		// Center panel
		centerPanel = new JPanel( );
		
		// Set layout
		centerPanel.setLayout( new CardLayout( ) );
		
		// Create empty panel
		JPanel emptyPanel = new EmptyDetailPanel( this );
		
		// Set empty panel background color
		emptyPanel.setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Add empty selection message
		JLabel emptyMessage = new JLabel( "NO SELECTION" );
		emptyMessage.setFont( emptyMessage.getFont( ).deriveFont( 20.0f ) );
		emptyMessage.setHorizontalAlignment( SwingConstants.CENTER );
		emptyPanel.add( emptyMessage,
				BorderLayout.NORTH );
		
		// Add panels
		centerPanel.add( emptyPanel,
				EmptyDetailPanel.PANEL_NAME );
		centerPanel.add( stationDetail,
				StationDetailPanel.PANEL_NAME );
		centerPanel.add( trainDetail,
				TrainDetailPanel.PANEL_NAME );
		centerPanel.add( railDetail,
				RailDetailPanel.PANEL_NAME );
		centerPanel.add( incidentDetail,
				IncidentDetailPanel.PANEL_NAME );
		
		// Add title
		add( panelTitle,
				BorderLayout.NORTH );
		
		// Add center panel
		add( centerPanel,
				BorderLayout.CENTER );
	}
	
	/**
	 * Change content
	 * 
	 * @param type
	 * 		Content type
	 * @param element
	 * 		The new element to consider
	 */
	public void changeDetailContent( DetailContent type,
			Object element )
	{
		// Save
		focusedPanelType = type;
		
		// Pass the information to the correct panel
		switch( type )
		{
			default:
			case DETAIL_CONTENT_EMPTY:
				// Activate/Deactivate
				stationDetail.deactivate( );
				trainDetail.deactivate( );
				railDetail.deactivate( );
				incidentDetail.deactivate( );
				break;
				
			case DETAIL_CONTENT_RAIL:
				// Focus on rail
				railDetail.focus( (Rail)element );
				
				// Activate/Deactivate
				railDetail.activate( );
				stationDetail.deactivate( );
				trainDetail.deactivate( );
				incidentDetail.deactivate( );
				break;
				
			case DETAIL_CONTENT_STATION:
				// Working directly with station?
				if( element instanceof Station )
					stationDetail.focus( (Station)element );
				// Working with String
				else if( element instanceof String )
				{
					// Get station
					Section station = line.getSection( (String )element );
					
					// Check for type
					if( !( station instanceof Station ) )
						return;
					
					// Focus on station
					stationDetail.focus( (Station)station );
				}
				
				// Activate/Deactivate
				stationDetail.activate( );
				railDetail.deactivate( );
				trainDetail.deactivate( );
				incidentDetail.deactivate( );
				break;
				
			case DETAIL_CONTENT_TRAIN:
				// Working directly with train?
				if( element instanceof Train )
					trainDetail.focus( (Train)element );
				// Working with String
				else if( element instanceof String )
				{
					// Train list
					ArrayList<Train> list = simulation.getTrainList( );
					
					// Look for train
					for( Train t : list )
						// Same train as the one clicked?
						if( t.toString( ).equals( (String)element ) )
						{
							// Focus on train
							trainDetail.focus( t );
							
							// Quit
							break;
						}
				}
				
				// Activate/Deactivate
				stationDetail.deactivate( );
				railDetail.deactivate( );
				trainDetail.activate( );
				break;
				
			case DETAIL_CONTENT_INCIDENT:
				if( element instanceof Incident )
					incidentDetail.focus( (Incident)element );
				else if( element instanceof String )
				{
					// Get incident list
					ArrayList<Incident> list = simulation.getIncidentList( );
					
					// Look for incident
					for( Incident i : list )
						// Same train as the one clicked?
						if( i.toString( ).equals( (String)element ) )
						{
							// Focus on train
							incidentDetail.focus( i );
							
							// Quit
							break;
						}
				}
				
				// Activate/Deactivate panels
				stationDetail.deactivate( );
				railDetail.deactivate( );
				trainDetail.deactivate( );
				incidentDetail.activate( );
				break;
		}
		
		// Show the correct panel
		( (CardLayout)centerPanel.getLayout( ) ).show( centerPanel,
				DetailContent.Name[ type.ordinal( ) ] );
		
		// Revalidate panel
		revalidate( );
		centerPanel.revalidate( );
	}
	
	/**
	 * Get selected item
	 * 
	 * @return the selected item
	 */
	public Object getSelectedItem( )
	{
		switch( focusedPanelType )
		{
			default:
			case DETAIL_CONTENT_EMPTY:
				return null;
				
			case DETAIL_CONTENT_RAIL:
				return railDetail.getSelectedItem( );
				
			case DETAIL_CONTENT_STATION:
				return stationDetail.getSelectedItem( );
				
			case DETAIL_CONTENT_TRAIN:
				return trainDetail.getSelectedItem( );
				
			case DETAIL_CONTENT_INCIDENT:
				return incidentDetail.getSelectedItem( );
		}
	}
	
	/**
	 * Get the incident creator
	 * 
	 * @return the incident creator
	 */
	public IncidentBuilder getIncidentBuilder( )
	{
		return incidentCreation;
	}

	/**
	 * Get the simulation
	 * 
	 * @return the simulation
	 */
	public Simulation getSimulation( )
	{
		return simulation;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Update cards
		railDetail.update( );
		stationDetail.update( );
		trainDetail.update( );
		incidentDetail.update( );
	}
}
