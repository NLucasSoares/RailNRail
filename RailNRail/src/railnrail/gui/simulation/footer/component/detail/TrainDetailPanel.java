package railnrail.gui.simulation.footer.component.detail;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import railnrail.gui.simulation.footer.SimulationFooterPanel;
import railnrail.gui.simulation.footer.component.detail.prevision.TrainPrevisionPanel;
import railnrail.incident.IncidentType;
import railnrail.network.section.Station;
import railnrail.res.img.ImageResource;
import railnrail.simulation.time.Helper;
import railnrail.train.Train;

/**
 * Train details panel
 *
 * @author Lucas SOARES
 */
public class TrainDetailPanel extends BaseDetailPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -1834237291720325187L;

	/**
	 * Panel name
	 */
	public static final String PANEL_NAME = "Train detail";
	
	/**
	 * Late time text
	 */
	private static final String LATE_TEXT = "Late time: ";
	
	/**
	 * Title font size
	 */
	private static final float TITLE_FONT_SIZE = 23.0f;
	
	/**
	 * Center panel margin
	 */
	private static final int CENTER_PANEL_MARGIN_VERTICAL = 10;
	private static final int CENTER_PANEL_MARGIN_HORIZONTAL = 0;
	
	/**
	 * Train being focused
	 */
	private Train trainFocused;
	
	/**
	 * Details panel
	 */
	private DetailPanel detailPanel;
	
	/**
	 * Current rail
	 */
	private JLabel currentRail = new JLabel( );
	
	/**
	 * Current late
	 */
	private JLabel currentLate = new JLabel( );
	
	/**
	 * Title
	 */
	private JLabel title = new JLabel( );
	
	/**
	 * Icons
	 */
	private ImageIcon railIcon,
		stationIcon;
	
	/**
	 * Incident button
	 */
	JButton createIncidentButton = new JButton( "Create incident" );
			
	/**
	 * Train prevision panel
	 */
	private TrainPrevisionPanel trainPrevisionPanel;
	
	/**
	 * Is activated?
	 */
	private boolean isActivated = false;
	
	/**
	 * Center panel
	 */
	private JPanel centerPanel;
	
	/**
	 * Construct panel
	 * 
	 * @param detailPanel
	 * 		The container for every detail panels
	 * 
	 * @throws IOException 
	 */
	public TrainDetailPanel( DetailPanel detailPanel ) throws IOException
	{
		// Parent constructor
		super( detailPanel );
		
		// Set layout
		setLayout( new BorderLayout( ) );

		// Load icon
		railIcon = ImageResource.loadIcon( ImageResource.IMAGE_RAIL_VERTICAL_SMALL );
		stationIcon = ImageResource.loadIcon( ImageResource.IMAGE_STATION );
		
		// Set background color
		setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Set border
		setBorder( BorderFactory.createCompoundBorder( getBorder( ),
				BorderFactory.createEmptyBorder( 0, 10, 0, 10 ) ) );
		
		// Center title
		title.setHorizontalAlignment( SwingConstants.CENTER );
		
		// Set title icon
		title.setIcon( ImageResource.loadIcon( ImageResource.IMAGE_TRAIN_ICON ) );
		
		// Center center panel components
		currentRail.setHorizontalAlignment( SwingConstants.CENTER );
		currentRail.setAlignmentX( Component.CENTER_ALIGNMENT );
		currentLate.setHorizontalAlignment( SwingConstants.CENTER );
		currentLate.setAlignmentX( Component.CENTER_ALIGNMENT );
		
		// Set title color
		title.setForeground( Color.BLACK );
		
		// Set font
		title.setFont( title.getFont( ).deriveFont( TrainDetailPanel.TITLE_FONT_SIZE ) );
		
		// Add title
		add( title,
				BorderLayout.NORTH );
		
		// Add button listener
		createIncidentButton.addActionListener( new ActionListener( )
		{
			
			@Override
			public void actionPerformed( ActionEvent evt )
			{
				createIncident( );
			}
		} );
		// Add incident button
		add( createIncidentButton,
				BorderLayout.SOUTH );
		
		// Create center panel
		centerPanel = new JPanel( );
		
		// Set layout
		centerPanel.setLayout( new BoxLayout( centerPanel,
				BoxLayout.PAGE_AXIS ) );
		
		// Set background color
		centerPanel.setBackground( SimulationFooterPanel.FOOTER_BACKGROUND_COLOR );
		
		// Set center panel border
		centerPanel.setBorder( BorderFactory.createCompoundBorder( BorderFactory.createCompoundBorder( BorderFactory.createEmptyBorder( TrainDetailPanel.CENTER_PANEL_MARGIN_VERTICAL,
							TrainDetailPanel.CENTER_PANEL_MARGIN_HORIZONTAL,
							TrainDetailPanel.CENTER_PANEL_MARGIN_VERTICAL,
							TrainDetailPanel.CENTER_PANEL_MARGIN_HORIZONTAL ),
						BorderFactory.createLineBorder( Color.BLACK ) ),
				BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) ) );

		// Build prevision list
		trainPrevisionPanel = new TrainPrevisionPanel( );
		
		// Add train prevision panel
		centerPanel.add(  trainPrevisionPanel );
				
		// Add current rail detail
		centerPanel.add( currentRail );
		
		// Set current rail icon
		currentRail.setIcon( railIcon );
		
		// Set border
		currentRail.setBorder( BorderFactory.createEmptyBorder( 5,
				0,
				0,
				0 ) );
		
		// Set font size
		currentRail.setFont( currentRail.getFont( ).deriveFont( 20.0f ) );

		// Set click callback
		currentRail.addMouseListener( new MouseListener( )
		{
			
			@Override
			public void mouseReleased( MouseEvent arg0 )
			{
			}
			
			@Override
			public void mousePressed( MouseEvent arg0 )
			{
			}
			
			@Override
			public void mouseExited( MouseEvent e )
			{
				// Set hand cursor
				currentRail.setCursor( new Cursor( Cursor.DEFAULT_CURSOR ) );
			}
			
			@Override
			public void mouseEntered( MouseEvent e )
			{
				// Set hand cursor
				currentRail.setCursor( new Cursor( Cursor.HAND_CURSOR ) );
			}
			
			@Override
			public void mouseClicked( MouseEvent me )
			{
				focusOn( );
			}
		} );
		
		// Set late font
		currentLate.setFont( currentRail.getFont( ) );
		
		// Add late text to center panel
		centerPanel.add( currentLate );
		
		// Add center panel
		add( centerPanel,
				BorderLayout.CENTER );
		
		// Save
		this.detailPanel = detailPanel;
	}
	
	/**
	 * Focus
	 * 
	 * @param train
	 * 		Train to focus on
	 */
	public void focus( Train train )
	{
		// Save
		this.trainFocused = train;
		
		// Set title
		title.setText( "Train "
				+ train.toString( ) );
	}
	
	/**
	 * Focus on
	 */
	public void focusOn( )
	{
		if( trainFocused != null )
		{
			// Station focus
			if( trainFocused.getCurrentRail( ).getParentSection( ) instanceof Station )
				detailPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_STATION,
						trainFocused.getCurrentRail( ).getParentSection( ) );
			else
				// Rail focus
				detailPanel.changeDetailContent( DetailContent.DETAIL_CONTENT_RAIL,
						this.trainFocused.getCurrentRail( ) );
		}
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Train focused?
		if( trainFocused != null )
		{
			// String buffer
			StringBuffer sb = new StringBuffer( );
			
			// Update rail
			sb.append( "<html>" );
			sb.append( trainFocused.getCurrentRail( ).getParentSection( ) instanceof Station ?
					"<u>"
					: "" );
			sb.append( trainFocused.getCurrentRail( ).getParentSection( ).getName( ) );
			sb.append( trainFocused.getCurrentRail( ).getParentSection( ) instanceof Station ?
					"</u>"
					: "" );
			sb.append( trainFocused.getCurrentRail( ).getParentSection( ) instanceof Station ?
					" - Rail "
					: " - <u>Rail " );
			sb.append( trainFocused.getCurrentRail( ).getID( ) );
			sb.append( trainFocused.getCurrentRail( ).getParentSection( ) instanceof Station ?
					""
					: "</u>" );
			if( trainFocused.isArrived( ) )
				sb.append( " (Terminus)" );
			else
			{
				sb.append( " - Current speed: " );
				sb.append( (int)trainFocused.getCurrentSpeed( ) );
				sb.append( "km/h" );
			}
			sb.append( "</html>" );
			
			// Set text
			currentRail.setText( sb.toString( ) );
			
			// Set icon
			currentRail.setIcon( trainFocused.getCurrentRail( ).getParentSection( ) instanceof Station
					? stationIcon
					: railIcon );
			
			// Set text
			currentLate.setText( TrainDetailPanel.LATE_TEXT
					+ Helper.convertSecondToText( trainFocused.getLateTime( ) ) );
			
			// Update prevision list
			trainPrevisionPanel.update( trainFocused );
			
			// Enable/Disable add button
			createIncidentButton.setEnabled( !trainFocused.isIncidentOccuring( ) );
		}
		
		// Show only if activated
		centerPanel.setVisible( isActivated );
		currentRail.setVisible( isActivated );
		title.setVisible( isActivated );
		createIncidentButton.setVisible( isActivated );
	}
	
	/**
	 * Activate
	 */
	public void activate( )
	{
		// Activate
		this.isActivated = true;
		trainPrevisionPanel.activate( );
	}
	
	/**
	 * Deactivate
	 */
	public void deactivate( )
	{
		// Deactivate
		this.isActivated = false;
		trainPrevisionPanel.deactivate( );
	}
	
	/**
	 * Get selected item
	 * 
	 * @return the selected item
	 */
	public Object getSelectedItem( )
	{
		return trainFocused;
	}
	
	/**
	 * Create incident
	 */
	public void createIncident( )
	{
		// Train focused not null
		if( trainFocused != null )
			// No incident occuring
			if( !trainFocused.isIncidentOccuring( ) )
				// Ask for incident details
				detailPanel.getIncidentBuilder( ).createIncident( IncidentType.TRAIN_INCIDENT,
						trainFocused );
	}
}
