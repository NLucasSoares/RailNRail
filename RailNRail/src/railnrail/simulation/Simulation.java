package railnrail.simulation;

import java.util.ArrayList;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import railnrail.gui.simulation.SimulationDashboardPanel;
import railnrail.incident.Incident;
import railnrail.network.Line;
import railnrail.network.section.Section;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.simulation.incident.IncidentGenerator;
import railnrail.simulation.time.CurrentTime;
import railnrail.train.Train;
import railnrail.train.schedule.TrainPlanning;

/**
 * Simulation, main loop goes in simulate( )
 * 
 * @author Lucas SOARES
 */
public class Simulation
{
	/**
	 * Is occuring?
	 */
	private boolean isOccuring = true;
	
	/**
	 * Time between change for incident light
	 */
	private static final long TIME_BETWEEN_INCIDENT_LIGHT_CHANGE = 500;
	
	/**
	 * Trains
	 */
	private ArrayList<Train> train = new ArrayList<Train>( );
	
	/**
	 * Incident
	 */
	private ArrayList<Incident> incident = new ArrayList<Incident>( );

	/**
	 * Simulation state
	 */
	private SimulationState state = SimulationState.SIMULATION_STATE_PAUSED;
	
	/**
	 * Speed multiplier
	 */
	private int speedMultiplier = 1;
	
	/**
	 * Last train id
	 */
	private int lastTrainId = 0;

	/**
	 * Current time
	 */
	private CurrentTime currentTime = new CurrentTime( );
	
	/**
	 * Alpha direction state
	 */
	private AlphaDirectionState alphaDirectionState = new AlphaDirectionState( );
	
	/**
	 * Current incident light
	 */
	private int currentIncidentLight = 0;
	
	/**
	 * Last incident light change
	 */
	private long lastIncidentLightChange = 0;
	
	/**
	 * Incident generator
	 */
	private IncidentGenerator incidentGenerator;
	
	/**
	 * Construct simulation
	 */
	public Simulation( )
	{
		// GMT +0 set
		TimeZone.setDefault( new SimpleTimeZone( 0,
				"Out Timezone" ) );
	}
	
	/**
	 * Launch the simulation on the loaded line
	 * (Console mode)
	 * 
	 * @param line
	 * 		The line to simulate on
	 */
	public void simulate( Line line )
	{
		// Console mode
		simulate( line,
				null );
	}
	
	/**
	 * Launch the simulation on the loaded line
	 * (GUI mode)
	 * 
	 * @param line
	 * 		The line to simulate on
	 * @param dashboard
	 * 		The GUI dashboard
	 */
	public void simulate( Line line,
			SimulationDashboardPanel dashboard )
	{
		// Cancel train starting before simulation initial timestamp
		cancelPreviousTrain( line,
				CurrentTime.INITIAL_TIMESTAMP );
		
		// Create incident generator
		incidentGenerator = new IncidentGenerator( this,
				line );
		
		// Main thread
		do
		{
			// GUI gesture (only if not null [equals not console mode])
			if( dashboard != null )
			{
				// Invalidate dashboard
				dashboard.getLineDisplayPanel( ).updateUI( );
				
				// Update incident list
				dashboard.getSimulationFooter( ).getIncidentListPanel( ).update( incident );

				// Update direction alpha
				alphaDirectionState.update( dashboard );
				
				// Update incident redlight
				dashboard.getLineDisplayPanel( ).setCurrentIncidentRedlight( currentIncidentLight );
			}
			
			// Train departure gesture
			updateTrainDeparture( line );
			
			// Train arrival gesture
			updateTrainArrival( );
			
			// Train arrival time calculation
			updateTrainArrivalTime( );
			
			// Update incident
			updateIncident( );
			
			// Update time
			currentTime.update( speedMultiplier );

			// Check if new week
			if( currentTime.isNewWeek( ) )
			{
				// Reset train schedules
				resetTrainSchedule( line );

				// Reset new week indicator
				currentTime.resetIsNewWeek( );
			}
			
			// Update incident light
			if( System.currentTimeMillis( ) - lastIncidentLightChange >= Simulation.TIME_BETWEEN_INCIDENT_LIGHT_CHANGE )
			{
				// Change light
				currentIncidentLight = currentIncidentLight == 0 ?
						1
						: 0;
				
				// Save time
				lastIncidentLightChange = System.currentTimeMillis( );
			}
				
			// Update line
			line.update( );
			
			// Update incident generation
			incidentGenerator.update( currentTime );
			
			// Sleep
			try
			{
				Thread.sleep( 16 );
			}
			catch( InterruptedException e )
			{
			}
		} while( isOccuring );
		
		// Stop all the train
		for( Train t : train )
			t.quit( );
	}

	/**
	 * Stop the simulation
	 */
	public void stop( )
	{
		isOccuring = false;
	}
	
	/**
	 * Update incident
	 */
	public void updateIncident( )
	{
		// Update incidents
		for( int i = 0; i < incident.size( ); i++ )
		{
			// Update
			incident.get( i ).update( CurrentTime.INITIAL_TIMESTAMP + currentTime.getCurrentTime( ) );
			
			// Check if finished
			if( incident.get( i ).isPassed( ) )
				// Remove incident
				incident.remove( i );
		}
	}
	
	/**
	 * Update train arrival time
	 */
	public void updateTrainArrivalTime( )
	{
		// Train
		for( Train t : train )
			t.updateArrivalTime( );
	}
	
	/**
	 * Cancel train before timestamp
	 * 
	 * @param line
	 * 		The line
	 * @param timestamp
	 * 		The timestamp
	 */
	private void cancelPreviousTrain( Line line,
			long timestamp )
	{
		// Cancel train
		line.getSchedule( ).cancelPreviousTrain( timestamp );
	}
	
	/**
	 * Reset train schedule
	 * 
	 * @param line
	 * 		The line being simulate
	 */
	public void resetTrainSchedule( Line line )
	{
		// Reset schedule
		line.getSchedule( ).reset( );
	}
	
	/**
	 * Train departure gesture
	 * 
	 * @param line
	 * 		The line being simulate
	 */
	private void updateTrainDeparture( Line line )
	{
		// Schedule train at this time
		ArrayList<TrainPlanning> schedule = line.getSchedule( ).getTrainScheduled( currentTime );
		
		// Iterate result
		for( TrainPlanning planning : schedule )
		{
			// Get section
			Section section = line.getSection( planning.getDepartureStation( ) );
			
			// Check
			if( section == null )
			{
				// Notify
				System.err.println( "Station "
						+ planning.getDepartureStation( )
						+ " is missing." );
				
				// Continue
				continue;
			}
			
			// Direction to go
			RailDirection direction;
			
			// Must go right?
			if( section.getRail( RailDirection.RAIL_DIRECTION_DEPARTURE,
					0 ).getRailConnectedToDestination( planning.getTravelCode( ),
							RailDirection.RAIL_DIRECTION_DEPARTURE,
							false ) != null )
				direction = RailDirection.RAIL_DIRECTION_DEPARTURE;
			else if( section.getRail( RailDirection.RAIL_DIRECTION_ARRIVAL,
					0 ).getRailConnectedToDestination( planning.getTravelCode( ),
							RailDirection.RAIL_DIRECTION_ARRIVAL,
							false ) != null )
				direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
			else
			{
				// Notify
				System.err.println( "Destination code "
						+ planning.getTravelCode( )
						+ " can't be found." );
				
				// Continue
				continue;
			}

			// TODO LOG
			// System.out.println( "Le train prevu a " + planning.getHourToLeave( ) + "h" + planning.getMinuteToLeave( ) + " depuis " + planning.getDepartureStation( ) +  " part." );
			
			// Add train
			addTrain( section,
					direction,
					planning );
		}
	}
	
	/**
	 * Update train arrival gesture
	 */
	private void updateTrainArrival( )
	{
		// Remove train which are arrived
		for( int i = 0; i < train.size( ); i++ )
		{
			// Get train
			Train t = train.get( i );
			
			// Remove
			if( t.isArrived( ) )
				train.remove( i );
		}
	}
	
	/**
	 * Add train
	 *
	 * @param section
	 * 		The section where to add train
	 * @param direction
	 * 		The direction
	 * @param planning
	 * 		The planning entry
	 */
	public void addTrain( Section section,
			RailDirection direction,
			TrainPlanning planning )
	{
		// Get free rail
		Rail freeRail = section.getFreeRail( direction );
		
		// If no free rail, we ignore the add, which report the departure
		if( freeRail == null )
			return;
		
		// Schedule planning is done
		planning.leave( );

		// Create train
		Train train = new Train( this,
			planning.getTravelCode( ),
			freeRail,
			direction,
			speedMultiplier,
			lastTrainId++ );
		
		// Add train
		this.train.add( train );
		
		// Is simulation running
		if( state != SimulationState.SIMULATION_STATE_PAUSED )
			train.resumeSimulation( );
		
		// Run train
		train.start( );
	}
	
	/**
	 * Pause simulation
	 */
	public void pause( )
	{
		// Pause
		state = SimulationState.SIMULATION_STATE_PAUSED;
		
		// Notify time
		currentTime.pause( );
		
		// Notify train
		for( Train t : train )
			t.pauseSimulation( );
	}
	
	/**
	 * Resume simulation
	 */
	public void resume( )
	{
		// Resume
		state = SimulationState.getValue( speedMultiplier );
		
		// Notify time
		currentTime.resume( );
		
		// Notify train
		for( Train t : train )
			t.resumeSimulation( );
	}
	
	/**
	 * Change speed multiplier
	 * 
	 * @param speedMultiplier
	 * 		Speed multiplier
	 */
	public void setSpeedMultiplier( int speedMultiplier )
	{
		// Save
		this.speedMultiplier = speedMultiplier;
		
		// Change state?
		if( state != SimulationState.SIMULATION_STATE_PAUSED )
			state = SimulationState.getValue( speedMultiplier );
		
		// Notify train
		for( Train t : train )
			t.setSpeedMultiplier( speedMultiplier );
	}

	/**
	 * Is paused?
	 * 
	 * @return if paused
	 */
	public boolean isPaused( )
	{
		return state == SimulationState.SIMULATION_STATE_PAUSED;
	}
	
	/**
	 * Get state
	 * 
	 * @return current state
	 */
	public SimulationState getState( )
	{
		return state;
	}
	
	/**
	 * Get simulation speed multiplier
	 * 
	 * @return simulation speed multiplier
	 */
	public int getSpeedMultiplier( )
	{
		return speedMultiplier;
	}
	
	/**
	 * Get current time
	 * 
	 * @return the current time
	 */
	public CurrentTime getCurrentTime( )
	{
		return currentTime;
	}
	
	/**
	 * Get formatted current time
	 * 
	 * @return the formatted current time
	 */
	public String getFormattedCurrentTime( )
	{
		return currentTime.getFormattedCurrentTime( );
	}
	
	/**
	 * Get trains
	 * 
	 * @return the trains
	 */
	public ArrayList<Train> getTrainList( )
	{
		return train;
	}
	
	/**
	 * Get incident list
	 * 
	 * @return the incident list
	 */
	public ArrayList<Incident> getIncidentList( )
	{
		return incident;
	}
	
	/**
	 * Add an incident
	 * 
	 * @param incident
	 * 		Incident to add
	 */
	public void addIncident( Incident incident )
	{
		// Add incident
		this.incident.add( incident );
	}
}

