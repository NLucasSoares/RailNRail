package railnrail.simulation.time;

public class Helper
{
	/**
	 * Convert seconds to text
	 * 
	 * @param time
	 * 		The time to convert
	 * 
	 * @return the converted time
	 */
	public static String convertSecondToText( long time )
	{
		// Output
		StringBuffer sb = new StringBuffer( );
		
		// Days
		if( time / 86400 > 0 )
		{
			// Add
			sb.append( time / 86400 );
			sb.append( "D" );
		}
		
		// Reduce
		time %= 86400;
		
		// Hours
		if( time / 3600 > 0 )
		{
			// Add
			sb.append( time / 3600 );
			sb.append( "h" );
		}
		
		// Reduce
		time %= 3600;
			
		// Minutes
		sb.append( time / 60 );
		sb.append( "m" );
		
		// Reduce
		time %= 60;
		
		// Seconds
		sb.append( time );
		sb.append( "s" );
		
		// OK
		return sb.toString( );
	}
}
