package railnrail.simulation.time;

/**
 * Day enum
 * 
 * @author Lucas SOARES
 */
public enum Day
{
	/**
	 * Days
	 */
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY;
	
	/**
	 * String constants
	 */
	public static final String[ ] Name =
	{
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
		"Sunday"
	};
	
	/**
	 * Get day
	 * 
	 * @param day
	 * 		The day
	 * 
	 * @return the day constant
	 */
	public static Day getDay( String day )
	{
		// Look for
		for( Day d : Day.values( ) )
			// Compare
			if( day.equalsIgnoreCase( Day.Name[ d.ordinal( ) ] ) )
				// OK
				return d;
		
		// Can't find
		return null;
	}			
}
