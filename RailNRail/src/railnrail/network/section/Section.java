package railnrail.network.section;

import java.awt.Point;
import java.util.ArrayList;

import railnrail.network.Line;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.network.type.RailType;

/**
 * An abstract definition of one section of the rail network
 * which are either station or canton
 * 
 * @author Lucas SOARES
 */
public abstract class Section
{
	/**
	 * Name of the section
	 */
	private String name;
	
	/**
	 * Nickname
	 */
	private String nickname;
	
	/**
	 * ID of the section
	 */
	private int id;
	
	/**
	 * The parent line
	 */
	private Line parentLine;
	
	/**
	 * Maximum speed on this section
	 */
	private int maximumSpeed;
	
	/**
	 * Section before and after
	 */
	@SuppressWarnings( "unchecked" )
	private ArrayList<Integer>[ ] nighSection = (ArrayList<Integer>[ ])new ArrayList[ RailDirection.values( ).length ];
	
	/**
	 * Departure/arrival rail
	 */
	@SuppressWarnings( "unchecked" )
	private ArrayList<Rail>[ ] rail = (ArrayList<Rail>[ ])new ArrayList[ RailDirection.values( ).length ];
	
	/**
	 * Size of the section
	 */
	private int size;
	
	/**
	 * Travel code
	 */
	private String travelCode;
	
	/**
	 * Is terminus?
	 */
	private boolean isTerminus;
	
	/**
	 * Rail type
	 */
	private RailType railType;
	
	/**
	 * Position on the grid
	 */
	private Point position;
	
	/**
	 * Build a section
	 * 
	 * @param parentLine
	 * 		The parent line
	 * @param name
	 * 		The name of the section
	 * @param size
	 * 		Size of the section
	 * @param travelCode
	 * 		The travel code
	 * @param isTerminus
	 * 		Is this section the last one?
	 * @param maximumSpeed
	 * 		The maximum speed
	 * @param nickname
	 * 		The nickname
	 */
	public Section( Line parentLine,
			String name,
			int size,
			String travelCode,
			int id,
			boolean isTerminus,
			RailType railType,
			Point position,
			int maximumSpeed,
			String nickname )
	{
		// Save
		this.parentLine = parentLine;
		this.name = name;
		this.size = size;
		this.travelCode = travelCode;
		this.id = id;
		this.isTerminus = isTerminus;
		this.railType = railType;
		this.position = position;
		this.maximumSpeed = maximumSpeed;
		this.nickname = nickname;
		
		// Create rail container
		for( int i = 0; i < RailDirection.values( ).length; i++ )
		{
			this.nighSection[ i ] = new ArrayList<Integer>( );
			this.rail[ i ] = new ArrayList<Rail>( );
		}
	}
	
	/**
	 * Add a rail in the section
	 * 
	 * @param id
	 * 		The unique id to be associated with this rail
	 * @param railDirection
	 * 		The direction of the rail (departure or arrival)
	 */
	public void addRail( int id,
			RailDirection direction )
	{
		// Add the rail
		rail[ direction.ordinal( ) ].add( new Rail( id,
				this,
				direction ) );
	}
	
	/**
	 * Get rail
	 * 
	 * @param direction
	 * 		The rail direction to get
	 * @param index
	 * 		The rail index
	 * 
	 * @return the requested rail
	 */
	public Rail getRail( RailDirection direction,
			int index )
	{
		return rail[ direction.ordinal( ) ].get( index );
	}
	
	/**
	 * Get rail array
	 * 
	 * @param direction
	 * 		The rail direction to get
	 * 
	 * @return the rail array
	 */
	public ArrayList<Rail> getRail( RailDirection direction )
	{
		return rail[ direction.ordinal( ) ];
	}
	
	/**
	 * Get rail count
	 * 
	 * @param direction
	 * 		The rail direction to get count of
	 * 
	 * @return the requested direction rail count
	 */
	public int getRailCount( RailDirection direction )
	{
		return rail[ direction.ordinal( ) ].size( );
	}
	
	/**
	 * Get total rail count
	 * 
	 * @return the total rail count
	 */
	public int getRailCount( )
	{
		return getRailCount( RailDirection.RAIL_DIRECTION_ARRIVAL )
				+ getRailCount( RailDirection.RAIL_DIRECTION_DEPARTURE );
	}
	
	/**
	 * Get night section count
	 * 
	 * @param direction
	 * 		The nigh section direction to get count of
	 * 
	 * @return the night section count
	 */
	public int getNighSectionCount( RailDirection direction )
	{
		return nighSection[ direction.ordinal( ) ].size( );
	}
	
	/**
	 * Add connection to another section
	 * 
	 * @param direction
	 * 		The direction of the connection (departure or arrival)
	 * @param sectionID
	 * 		The id of the section to connect
	 */
	public void addConnectionToSection( RailDirection direction,
			int sectionID )
	{
		// Add
		nighSection[ direction.ordinal( ) ].add( sectionID );
	}
	
	/**
	 * Get nigh section
	 * 
	 * @param direction
	 * 		The direction of the section
	 * @param index
	 * 		The nigh section index
	 * 
	 * @return the section id
	 */
	public int getNighSection( RailDirection direction,
			int index )
	{
		return nighSection[ direction.ordinal( ) ].get( index );
	}
	
	/**
	 * Get id
	 * 
	 * @return the id
	 */
	public int getId( )
	{
		return id;
	}
	
	/**
	 * Get name
	 * 
	 * @return the name of the section
	 */
	public String getName( )
	{
		return name;
	}
	
	/**
	 * Get rail type
	 * 
	 * @return the rail type
	 */
	public RailType getRailType( )
	{
		return railType;
	}
	
	/**
	 * Get position
	 * 
	 * @return the position
	 */
	public Point getPosition( )
	{
		return position;
	}
	
	/**
	 * Get size
	 *
	 * @return the size
	 */
	public int getSize( )
	{
		return size;
	}
	
	/**
	 * Is terminus?
	 * 
	 * @return is terminus?
	 */
	public boolean isTerminus( )
	{
		return isTerminus;
	}
	
	/**
	 * Get travel code
	 * 
	 * @return travel code
	 */
	public String getTravelCode( )
	{
		return travelCode;
	}
	
	/**
	 * Get parent line
	 * 
	 * @return the parent line
	 */
	public Line getParentLine( )
	{
		return parentLine;
	}
	
	/**
	 * Get maximum speed
	 * 
	 * @return the maximum speed
	 */
	public int getMaximumSpeed( )
	{
		return maximumSpeed;
	}
	
	/**
	 * Get free rail
	 * 
	 * @param direction
	 * 		The direction
	 * 
	 * @return free rail (no train on)
	 */
	public Rail getFreeRail( RailDirection direction )
	{
		// Look for
		for( Rail r : rail[ direction.ordinal( ) ] )
			if( !r.isTrainPresent( ) )
				return r;
		
		// Can't find
		return null;
	}
	
	/**
	 * Get nickname
	 * 
	 * @return the nickname
	 */
	public String getNickname( )
	{
		return nickname;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Update rails
		for( RailDirection direction : RailDirection.values( ) )
			for( Rail r : rail[ direction.ordinal( ) ] )
				r.update( );
	}
}
