package railnrail.network.section.rail;

/**
 * Available directions for rails
 * 
 * @author Lucas SOARES
 */
public enum RailDirection
{
	RAIL_DIRECTION_ARRIVAL,
	RAIL_DIRECTION_DEPARTURE;
	
	/**
	 * Name of each side
	 */
	public static final String[ ] Name =
	{
		"ARRIVAL",
		"DEPARTURE"
	};
}
