package railnrail.network.section.rail;

import railnrail.incident.Incident;
import railnrail.incident.IncidentHolder;
import railnrail.incident.rail.RailIncident;
import railnrail.network.section.Section;
import railnrail.train.Train;

/**
 * Rail in section, can be for arrival or departure
 * 
 * @author Lucas SOARES
 */
public class Rail implements IncidentHolder
{
	/**
	 * Rail direction (departure or arrival)
	 */
	private RailDirection direction;
	
	/**
	 * Parent section
	 */
	private Section parentSection;
	
	/**
	 * Unique id
	 */
	private int id;
	
	/**	 	
	 * Connected rails
	 */
	private RailConnection connection;
	
	/**
	 * Train on this rail
	 */
	private Train presentTrain = null;
	
	/**
	 * Current incident
	 */
	private RailIncident incident;
	
	/**
	 * Is blocked?
	 */
	private boolean isBlocked = false;
	
	/**
	 * Construct a rail
	 * 
	 * @param id
	 * 		A unique id for this rail
	 * @param parentSection
	 * 		The parent section of this rail
	 * @param railDirection
	 * 		The direction of the way (departure or arrival)
	 */
	public Rail( int id,
			Section parentSection,
			RailDirection direction )
	{
		// Save the informations
		this.id = id;
		this.direction = direction;
		this.parentSection = parentSection;
		
		// Construct
		connection = new RailConnection( this );
	}
	
	/**
	 * Get the connection informations
	 * 
	 * @return the connection informations
	 */
	public RailConnection getConnection( )
	{
		return connection;
	}
	
	/**
	 * Get direction
	 * 
	 * @return the direction
	 */
	public RailDirection getDirection( )
	{
		return direction;
	}
	
	/**
	 * Add connection
	 * 
	 * @param direction
	 * 		The direction to connect
	 * @param rail
	 * 		The rail to connect
	 */
	public void addConnection( RailDirection direction,
			Rail rail )
	{
		// Add connection
		connection.addConnection( direction,
				rail );
	}
	
	/**
	 * Get parent section
	 * 
	 * @return the parent section
	 */
	public Section getParentSection( )
	{
		return parentSection;
	}
	
	/**
	 * Is train present on this rail?
	 * 
	 * @return if train is present
	 */
	public boolean isTrainPresent( )
	{
		return presentTrain != null;
	}
	
	/**
	 * Get train
	 * 
	 * @return present train
	 */
	public Train getTrain( )
	{
		return presentTrain;
	}
	
	/**
	 * Get rail connected to destination
	 * 
	 * @param destinationCode
	 * 		The destination code
	 * @param direction
	 * 		The direction to look at
	 * @param isLookingForFree
	 * 		Is train looking for free rail?
	 * 
	 * @return the rail which lead to this destination
	 */
	public Rail getRailConnectedToDestination( String destinationCode,
			RailDirection direction,
			boolean isLookingForFree )
	{
		return connection.getRailConnectedToDestination( destinationCode,
				direction,
				isLookingForFree );
	}
	
	/**
	 * Train enter
	 * 
	 * @return is entering successful?
	 */
	public synchronized boolean enterTrain( Train train )
	{
		// Currently a train on this rail?
		if( presentTrain != null )
		{
			// Wait for the current train to leave
			try
			{
				wait( );
			}
			catch( InterruptedException e )
			{
				return false;
			}
		}
		
		// Reset position
		train.setAtRailStart( );

		// Save the train
		presentTrain = train;
		
		// OK
		return true;
	}
	
	/**
	 * Train leave
	 */
	public synchronized void leaveTrain( )
	{
		// Present train leave
		presentTrain = null;

		// Notify the threads to stop waiting
		notify( );
	}
	
	/**
	 * Get id
	 * 
	 * @return the id
	 */
	public int getID( )
	{
		return id;
	}

	/**
	 * Start an incident
	 * 
	 * @param incident
	 *		The incident
	 */
	@Override
	public void startIncident( Incident incident )
	{
		// Save
		this.incident = (RailIncident)incident;
	}
	
	/**
	 * Is an incident occuring?
	 * 
	 * @return if incident is occuring
	 */
	public boolean isIncidentOccuring( )
	{
		return incident != null;
	}
	
	/**
	 * Get incident
	 * 
	 * @return the incident occuring
	 */
	public RailIncident getIncident( )
	{
		return incident;
	}
	
	/**
	 * Set blocked
	 * 
	 * @param isBlocked
	 * 		Is blocked?
	 */
	public void setBlocked( boolean isBlocked )
	{
		this.isBlocked = isBlocked;
	}
	
	/**
	 * Is blocked?
	 * 
	 * @return if blocked
	 */
	public boolean isBlocked( )
	{
		return isBlocked;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Is blocked?
		if( isBlocked )
		{
			// Train present?
			if( presentTrain != null )
				// Block train
				presentTrain.setBlocked( true );
		}
		else
		{
			// Incident occuring?
			if( incident != null )
			{
				// Check for incident end
				if( incident.isPassed( ) )
					// No more incident
					incident = null;
				// Train on the rail?
				else if( presentTrain != null )
				{
					// Stop the train
					presentTrain.setBlocked( true );
				}
			}
			// No incident
			else if( presentTrain != null )
				// Unblock train
				presentTrain.setBlocked( false );
		}
	}
}
