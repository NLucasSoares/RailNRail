package railnrail.network.builder;

/**
 * Subdivision properties
 * 
 * @author Lucas SOARES
 */
public enum SubdivisionProperty
{
	SUBDIVISION_COUNT,
	SUBDIVISION_LIST;
}

