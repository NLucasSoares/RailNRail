package railnrail.network.builder;

/**
 * Properties inside the line definition
 * 
 * @author Lucas SOARES
 */
public enum LineFileProperty
{
	PROPERTY_ID,
	PROPERTY_NAME,
	PROPERTY_TYPE,
	PROPERTY_SIZE,
	PROPERTY_RAIL_COUNT,
	PROPERTY_CONNECTION_DESCRIPTION,
	PROPERTY_ALLOWED_TRAVEL_ID,
	PROPERTY_POSITION_X,
	PROPERTY_POSITION_Y,
	PROPERTY_RAIL_TYPE,
	PROPERTY_MAXIMUM_SPEED,
	PROPERTY_NICKNAME;
	
	/**
	 * Terminus value
	 */
	public static String TERMINUS_VALUE = "TERMINUS";
}
