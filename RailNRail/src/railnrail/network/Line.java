package railnrail.network;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import railnrail.file.Helper;
import railnrail.network.builder.LineFileProperty;
import railnrail.network.builder.SectionType;
import railnrail.network.builder.SubdivisionProperty;
import railnrail.network.empty.EmptyPlan;
import railnrail.network.section.Canton;
import railnrail.network.section.Section;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.network.subdivision.Subdivision;
import railnrail.network.type.RailType;
import railnrail.train.schedule.Schedule;

/**
 * One train line, containing Section
 * 
 * @author Lucas SOARES
 */
public class Line
{
	/**
	 * Sections of the line
	 */
	private ArrayList<Section> section = new ArrayList<Section>( );
	
	/**
	 * Train planning
	 */
	private Schedule schedule;
	
	/**
	 * Name of the line
	 */
	private String name;
	
	/**
	 * Border cell margin
	 */
	private static final int LINE_BORDER_CELL_COUNT_MARGIN_HORIZONTAL = 1;
	private static final int LINE_BORDER_CELL_COUNT_MARGIN_VERTICAL = 0;
	
	/**
	 * Max rail count
	 */
	private int[ ] maxRailCount = new int[ RailDirection.values( ).length ];
	
	/**
	 * Subdivision gesture
	 */
	private Subdivision[ ] subdivision;
	
	/**
	 * Empty plan
	 */
	private EmptyPlan emptyPlan;
	
	/**
	 * Construct the line
	 * 
	 * @param resFolder
	 * 		The resources folder
	 * @param filename
	 * 		The file to be loaded
	 * @param initialTimestamp
	 * 		The initial timestamp
	 * 
	 * @throws IOException 
	 */
	public Line( String resFolder,
			String filename,
			long initialTimestamp ) throws IOException
	{
		// Input stream
		BufferedReader fr = new BufferedReader( new InputStreamReader( new FileInputStream( new File( resFolder
				+ "/"
				+ filename ) ) ) );

		// Line
		String line;
		
		// Properties
		String[ ] properties;
		
		// Read name
		line = Helper.getNextUncommentedLine( fr );
		name = line.substring(  1, line.length( ) - 1 );
		
		// Load train schedule
		schedule = new Schedule( resFolder,
				Helper.getNextUncommentedLine( fr ),
				initialTimestamp );
		
		// Read subdivision
		line = Helper.getNextUncommentedLine( fr );
		properties = line.split( ";" );
		
		// Allocate subdivision
		subdivision = new Subdivision[ Integer.parseInt( properties[ SubdivisionProperty.SUBDIVISION_COUNT.ordinal( ) ] ) ];
		
		// Load subdivisions
		for( int i = 0; i < subdivision.length; i++ )
			subdivision[ i ] = new Subdivision( properties[ SubdivisionProperty.SUBDIVISION_LIST.ordinal( ) + i ] );

		// Rail id
		int railUUID = 0;
					
		// Read line details
		while( ( line = Helper.getNextUncommentedLine( fr ) ) != null )
		{
			// Properties for section
			properties = line.split( ";" );
			
			// Check file well-formed
			if( properties.length != LineFileProperty.values( ).length )
				throw new IOException( "Malformed line ["
						+ filename
						+ "] \""
						+ line
						+ "\"." );
			
			// Check size
			if( Integer.parseInt( properties[ LineFileProperty.PROPERTY_SIZE.ordinal( ) ] ) <= 0 )
				throw new IOException( "Malformed line ["
						+ filename
						+ "] \""
						+ line
						+ "\"." );
			
			// Section container
			Section section;
			
			// Travel id array
			String[ ] travelIDArray = properties[ LineFileProperty.PROPERTY_ALLOWED_TRAVEL_ID.ordinal( ) ].split( "," );
			
			// Travel id
			String travelID = travelIDArray[ 0 ];
			
			// Is terminus?
			boolean isTerminus = travelIDArray.length > 1 ?
					travelIDArray[ 1 ].equals( LineFileProperty.TERMINUS_VALUE )
					: false;
			
			// Create section
			if( properties[ LineFileProperty.PROPERTY_TYPE.ordinal( ) ].equals( SectionType.Name[ SectionType.SECTION_TYPE_STATION.ordinal( ) ] ) )
			{
				// Create station
				section = new Station( this,
						properties[ LineFileProperty.PROPERTY_NAME.ordinal( ) ],
						Integer.parseInt( properties[ LineFileProperty.PROPERTY_SIZE.ordinal( ) ] ),
						travelID,
						Integer.parseInt( properties[ LineFileProperty.PROPERTY_ID.ordinal( ) ] ),
						isTerminus,
						RailType.FindRailType( properties[ LineFileProperty.PROPERTY_RAIL_TYPE.ordinal( ) ] ),
						new Point( Integer.parseInt( properties[ LineFileProperty.PROPERTY_POSITION_X.ordinal( ) ] ),
							Integer.parseInt( properties[ LineFileProperty.PROPERTY_POSITION_Y.ordinal( ) ] ) ),
						Integer.parseInt( properties[ LineFileProperty.PROPERTY_MAXIMUM_SPEED.ordinal( ) ] ),
						properties[ LineFileProperty.PROPERTY_NICKNAME.ordinal( ) ] );
			}
			else if( properties[ LineFileProperty.PROPERTY_TYPE.ordinal( ) ].equals( SectionType.Name[ SectionType.SECTION_TYPE_CANTON.ordinal( ) ] ) )
			{
				// Create canton
				section = new Canton( this,
						properties[ LineFileProperty.PROPERTY_NAME.ordinal( ) ],
						Integer.parseInt( properties[ LineFileProperty.PROPERTY_SIZE.ordinal( ) ] ),
						travelID,
						Integer.parseInt( properties[ LineFileProperty.PROPERTY_ID.ordinal( ) ] ),
						isTerminus,
						RailType.FindRailType( properties[ LineFileProperty.PROPERTY_RAIL_TYPE.ordinal( ) ] ),
						new Point( Integer.parseInt( properties[ LineFileProperty.PROPERTY_POSITION_X.ordinal( ) ] ),
							Integer.parseInt( properties[ LineFileProperty.PROPERTY_POSITION_Y.ordinal( ) ] ) ),
						Integer.parseInt( properties[ LineFileProperty.PROPERTY_MAXIMUM_SPEED.ordinal( ) ] ),
						properties[ LineFileProperty.PROPERTY_NICKNAME.ordinal( ) ] );
			}
			else
				throw new IOException( "Malformed line ["
						+ filename
						+ "] \""
						+ line
						+ "\"." );
			
			// Get rail count
			int railCount = Integer.parseInt( properties[ LineFileProperty.PROPERTY_RAIL_COUNT.ordinal( ) ] );
			
			// Add rail
			for( int i = 0; i < railCount; i++ )
				section.addRail( railUUID++,
						( i < railCount / 2 ) ?
							RailDirection.RAIL_DIRECTION_ARRIVAL
							: RailDirection.RAIL_DIRECTION_DEPARTURE );
			
			// Read connections
			String[ ] connectionArray = properties[ LineFileProperty.PROPERTY_CONNECTION_DESCRIPTION.ordinal( ) ].split( "," );
			
			// Add connections
			for( String connection : connectionArray )
			{
				// Direction
				String directionString = connection.substring( 0,
						connection.indexOf( '(' ) );
				RailDirection direction;
				
				// Analyse direction
				if( directionString.equals( RailDirection.Name[ RailDirection.RAIL_DIRECTION_ARRIVAL.ordinal( ) ] ) )
					direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
				else if( directionString.equals( RailDirection.Name[ RailDirection.RAIL_DIRECTION_DEPARTURE.ordinal( ) ] ) )
					direction = RailDirection.RAIL_DIRECTION_DEPARTURE;
				else
					throw new IOException( "Malformed line ["
							+ filename
							+ "] \""
							+ line
							+ "\"." );

				// Add connection
				section.addConnectionToSection( direction,
						Integer.parseInt( connection.substring( connection.indexOf( '(' ) + 1,
								connection.length( ) - 1 ) ) );
			}
			
			// Add the section to all of the sections
			this.section.add( section );
		}
		
		// Create physical connections
		createPhysicalConnection( );
		
		// Close stream
		fr.close( );
		
		// Calculate maximum rail count
		for( RailDirection direction : RailDirection.values( ) )
			maxRailCount[ direction.ordinal( ) ] = getMaxRailCountPrivate( direction );
		
		// Create empty plan
		emptyPlan = new EmptyPlan( getMaxPosition( ) );
	}
	
	/**
	 * Create the physical connection between rails
	 */
	private void createPhysicalConnection( )
	{
		// Touching sections
		@SuppressWarnings( "unchecked" )
		ArrayList<Section>[ ] touchingSection = (ArrayList<Section>[ ])new ArrayList[ RailDirection.values( ).length ];
		
		// Allocate array list
		for( RailDirection direction : RailDirection.values( ) )
			touchingSection[ direction.ordinal( ) ] = new ArrayList<Section>( );
		
		// Iterate sections
		for( Section s : section )
		{
			// Empty touching arrays
			for( int i = 0; i < touchingSection.length; i++ )
				touchingSection[ i ].clear( );
			
			// Get touching sections
			for( RailDirection direction : RailDirection.values( ) )
				// Fill
				for( int i = 0; i < s.getNighSectionCount( direction ); i++ )
					touchingSection[ direction.ordinal( ) ].add( getSection( s.getNighSection( direction,
							i ) ) );
					
			// Iterate rails
			for( RailDirection direction : RailDirection.values( ) )
				for( int i = 0; i < s.getRailCount( direction ); i++ )
				{
					// Rail
					Rail rail = s.getRail( direction,
						i );

					// Connect
					switch( direction )
					{
						case RAIL_DIRECTION_DEPARTURE:
							for( Section ts : touchingSection[ RailDirection.RAIL_DIRECTION_ARRIVAL.ordinal( ) ] )
								for( Rail r : ts.getRail( RailDirection.RAIL_DIRECTION_DEPARTURE ) )
									r.addConnection( RailDirection.RAIL_DIRECTION_DEPARTURE,
											rail );
							break;
							
						case RAIL_DIRECTION_ARRIVAL:
							for( Section ts : touchingSection[ RailDirection.RAIL_DIRECTION_DEPARTURE.ordinal( ) ] )
								for( Rail r : ts.getRail( RailDirection.RAIL_DIRECTION_ARRIVAL ) )
									r.addConnection( RailDirection.RAIL_DIRECTION_ARRIVAL,
											rail );
							break;
							
						default:
							break;
					}
				}
		}
	}
	
	/**
	 * Get line name
	 * 
	 * @return the name
	 */
	public String getName( )
	{
		return name;
	}
	
	/**
	 * Get section from id
	 * 
	 * @param id
	 * 		The section id
	 * 
	 * @return the section
	 */
	public Section getSection( int id )
	{
		// Look for section
		for( Section section : this.section )
			if( section.getId( ) == id )
				return section;
		
		// Can't find
		return null;
	}
	
	/**
	 * Get section from name
	 * 
	 * @param name
	 * 		The section name
	 * 
	 * @return the section
	 */
	public Section getSection( String name )
	{
		// Look for
		for( Section s : section )
			// Names are equals?
			if( s.getName( ).equalsIgnoreCase( name ) )
				return s;
		
		// Can't find
		return null;
	}
	
	/**
	 * Get section count
	 * 
	 * @return the section count
	 */
	public int getSectionCount( )
	{
		return section.size( );
	}
	
	/**
	 * Get max position
	 * 
	 * @return max position
	 */
	public Point getMaxPosition( )
	{
		// Max position
		Point maxPosition = new Point( 0,
				0 );
		
		for( Section s : section )
		{
			// x position
			if( s.getPosition( ).x  + 1 + Line.LINE_BORDER_CELL_COUNT_MARGIN_HORIZONTAL > maxPosition.x )
				maxPosition.x = s.getPosition( ).x + 1 + Line.LINE_BORDER_CELL_COUNT_MARGIN_HORIZONTAL;
			
			// y position
			if( s.getPosition( ).y + s.getRailCount( )  + 1 + Line.LINE_BORDER_CELL_COUNT_MARGIN_VERTICAL > maxPosition.y )
				maxPosition.y = s.getPosition( ).y + s.getRailCount( ) + 1 + Line.LINE_BORDER_CELL_COUNT_MARGIN_VERTICAL;
		}
		
		// OK
		return maxPosition;
	}
	
	/**
	 * Get max rail on line for one direction
	 * 
	 * @param direction
	 * 		The direction to consider
	 * 
	 * @return the rail count
	 */
	private int getMaxRailCountPrivate( RailDirection direction )
	{
		// Current max
		int currentMax = 0;
		
		// Iterate
		for( Section s : section )
			// More rails on this section?
			if( s.getRailCount( direction ) > currentMax )
				currentMax = s.getRailCount( direction );
		
		// OK
		return currentMax;
	}

	/**
	 * Get max rail on line for one direction
	 * 
	 * @param direction
	 * 		The direction to consider
	 * 
	 * @return the rail count
	 */
	public int getMaxRailCount( RailDirection direction )
	{
		return maxRailCount[ direction.ordinal( ) ];
	}
	
	/**
	 * Get the max rail count for a line
	 * 
	 * @return the max rail count
	 */
	public int getMaxRailCount( )
	{
		return getMaxRailCount( RailDirection.RAIL_DIRECTION_DEPARTURE )
				+ getMaxRailCount( RailDirection.RAIL_DIRECTION_ARRIVAL );
	}
	
	/**
	 * Get the max rail count based on line subdivision
	 * 
	 * @param sectionID
	 * 		The section ID
	 * @param direction
	 * 		The direction to consider
	 */
	public int getMaxRailCount( int sectionID,
			RailDirection direction )
	{
		// Get subdivision
		Subdivision subdivision = getSubdivisionFromSectionID( sectionID );
		
		// Check result
		if( subdivision == null )
			return 0;
		
		// Max rail count
		int result = 0;
			
		// Iterate subdivisions
		for( int i = 0; i < subdivision.getSubdivisionLimitCount( ); i++ )
			for( int j = subdivision.getSubdivisionLimit( i ).getBeginID( ); j <= subdivision.getSubdivisionLimit( i ).getEndID( ); j++ )
				// Check for maximum
				if( section.get( j ).getRailCount( direction ) > result )
					result = section.get( j ).getRailCount( direction );
		
		// OK
		return result;
	}
	
	/**
	 * Get subdivision count
	 * 
	 * @return the subdivision count
	 */
	public int getSubdivisionCount( )
	{
		return subdivision.length;
	}
	
	/**
	 * Get subdivision
	 * 
	 * @param index
	 * 		Subdivision index
	 * 
	 * @return the subdivision
	 */
	public Subdivision getSubdivisionFromIndex( int index )
	{
		return subdivision[ index ];
	}
	
	/**
	 * Get subdivision associated to one section id
	 * 
	 * @param sectionID
	 * 		The section ID
	 * 
	 * @return the subdivision
	 */
	public Subdivision getSubdivisionFromSectionID( int sectionID )
	{
		// Iterate subdivision
		for( Subdivision s : subdivision )
		{
			for( int i = 0; i < s.getSubdivisionLimitCount( ); i++ )
				if( sectionID >= s.getSubdivisionLimit( i ).getBeginID( )
					&& sectionID <= s.getSubdivisionLimit( i ).getEndID( ) )
					return s;
		}
		
		// Can't find
		return null;
	}
	
	/**
	 * Get train schedule
	 * 
	 * @return the train schedule
	 */
	public Schedule getSchedule( )
	{
		return schedule;
	}
	
	/**
	 * Get empty plan
	 * 
	 * @return the empty plan
	 */
	public EmptyPlan getEmptyPlan( )
	{
		return emptyPlan;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Update
		for( Section s : section )
			s.update( );
	}
}

