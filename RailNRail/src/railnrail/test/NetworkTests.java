package railnrail.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.network.SectionTests;
import railnrail.test.network.SubdivisionTests;

@RunWith(Suite.class)
@SuiteClasses({ SectionTests.class, SubdivisionTests.class })
/**
 * TestSuite for the package network
 * @author marilou
 *
 */
public class NetworkTests {

}
