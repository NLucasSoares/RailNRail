package railnrail.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.train.PrevisionTests;
import railnrail.test.train.ScheduleTests;
import railnrail.test.train.TrainTest;

@RunWith(Suite.class)
@SuiteClasses({ PrevisionTests.class, ScheduleTests.class, TrainTest.class })
/**
 * 
 *  TestSuite for the package train
 * @author marilou
 *
 */
public class TrainTests {

}
