package railnrail.test.train;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.train.prevision.ArrivalPrevisionTest;
import railnrail.test.train.prevision.TrainPrevisionListTest;

@RunWith(Suite.class)
@SuiteClasses({ ArrivalPrevisionTest.class, TrainPrevisionListTest.class })
/**
 * TestSuite for the package train.prevision
 * @author marilou
 *
 */
public class PrevisionTests {

}
