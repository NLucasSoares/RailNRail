/**
 * 
 */
package railnrail.test.train;

import static org.junit.Assert.*;

import java.util.Random;

import railnrail.train.Train;
import org.junit.Test;

/**
 * @author marilou
 *
 */
public class TrainTest {

	/**
	 * Test method for {@link railnrail.train.Train#run()}.
	 */
	@Test
	public void testRun() {
	}

	/**
	 * Test method for {@link railnrail.train.Train#Train(railnrail.simulation.Simulation, java.lang.String, railnrail.network.section.rail.Rail, railnrail.network.section.rail.RailDirection, int, int)}.
	 */
	@Test
	public void testTrain() {
	}

	/**
	 * Test method for {@link railnrail.train.Train#getPosition()}.
	 */
	@Test
	public void testGetPosition() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#quit()}.
	 */
	@Test
	public void testQuit() {
	}

	/**
	 * Test method for {@link railnrail.train.Train#resumeSimulation()}.
	 */
	@Test
	public void testResumeSimulation() {
		Train train = new Train(null, null, null, null, 0, 0);
		
		long lastupdate = System.currentTimeMillis();
		train.resumeSimulation();
		assertTrue(train.getLastUpdate() >= lastupdate);
		assertTrue(train.isRunning());
	}

	/**
	 * Test method for {@link railnrail.train.Train#setSpeedMultiplier(int)}.
	 */
	@Test
	public void testSetSpeedMultiplier() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#pauseSimulation()}.
	 */
	@Test
	public void testPauseSimulation() {

		Train train = new Train(null, null, null, null, 0, 0);
		train.pauseSimulation();
		assertFalse(train.isRunning());
	}

	/**
	 * Test method for {@link railnrail.train.Train#getCurrentRail()}.
	 */
	@Test
	public void testGetCurrentRail() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#getTrainId()}.
	 */
	@Test
	public void testGetTrainId() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#getDirection()}.
	 */
	@Test
	public void testGetDirection() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#getTravelCode()}.
	 */
	@Test
	public void testGetTravelCode() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#getPrevisionList()}.
	 */
	@Test
	public void testGetPrevisionList() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#setAtRailStart()}.
	 */
	@Test
	public void testSetAtRailStart() {
		
		Train train = new Train(null, null, null, null, 0, 0);
		train.setAtRailStart();
		assertEquals(0,train.getPosition(),0);
	}

	/**
	 * Test method for {@link railnrail.train.Train#isArrived()}.
	 */
	@Test
	public void testIsArrived() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.train.Train#updateArrivalTime()}.
	 */
	@Test
	public void testUpdateArrivalTime() {
	}

	/**
	 * Test method for {@link railnrail.train.Train#isAfter(railnrail.network.section.Section)}.
	 */
	@Test
	public void testIsAfter() {
	}

	/**
	 * Test method for {@link railnrail.train.Train#toString()}.
	 */
	@Test
	public void testToString() {
		String travelCode = "TravelCode";
		Random r = new Random();
		int id = r.nextInt();
		Train train = new Train(null, travelCode, null, null, r.nextInt(), id);
		
		assertEquals(travelCode +" ("+id+")",train.toString());
	}

}
