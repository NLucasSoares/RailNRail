package railnrail.test.train.schedule;

import static org.junit.Assert.*;

import org.junit.Test;

import railnrail.simulation.time.Day;
import railnrail.train.schedule.TrainPlanning;
/**
 * TestCase for the class TrainPlanning of the package train.schedule
 * @author marilou
 *
 */
public class TrainPlanningTest {

	@Test
	/**
	 * test for the constructor TrainPlanning()
	 */
	public void testTrainPlanning() {
	}

	@Test
	/**
	 * test for the method getHourToLeave()
	 */
	public void testGetHourToLeave() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getMinuteToLeave()
	 */
	public void testGetMinuteToLeave() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getDepartureStation()
	 */
	public void testGetDepartureStation() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getTravelCode()
	 */
	public void testGetTravelCode() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}
	/**
	 * test for the method getDestination()
	 */
	@Test
	public void testGetDestination() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getDayToLeave()
	 */
	public void testGetDayToLeave() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getDepartureTimestamp()
	 */
	public void testGetDepartureTimestamp() {
		
	}

	@Test
	/**
	 * test for the method isGone()
	 */
	public void testIsGone() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method leave()
	 */
	public void testLeave() {
		String station = "";
		String dest = "";
		String travelCode = "";
		Day day = Day.MONDAY;
		TrainPlanning train = new TrainPlanning(0,0,station,dest,travelCode,day,0);
		
		assertNotNull(train.isGone());
		train.leave();
		assertNotNull(train.isGone());
		assertTrue(train.isGone());
	}

	@Test
	/**
	 * test for the method reset()
	 */
	public void testReset() {
		String station = "";
		String dest = "";
		String travelCode = "";
		Day day = Day.MONDAY;
		TrainPlanning train = new TrainPlanning(0,0,station,dest,travelCode,day,0);
		
		assertNotNull(train.isGone());
		train.reset();
		assertNotNull(train.isGone());
		assertFalse(train.isGone());
	}

}
