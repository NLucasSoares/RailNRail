package railnrail.test.train.schedule;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.junit.Test;

import railnrail.simulation.time.Day;
import railnrail.train.schedule.TrainPlanning;
/**
 * TestCase for the class Schedule of the package train.schedule
 * @author marilou
 *
 */
public class ScheduleTest {

	@Test
	/**
	 * Test for the constructor Schedule()
	 */
	public void testSchedule() {
		//fail("Not yet implemented");
	}

	@Test
	/**
	 * Test for the method getTrainScheduled()
	 */
	public void testGetTrainScheduled() {
		//fail("Not yet implemented");
	}

	@Test
	/**
	 * Test for the method reset()
	 */
	public void testReset() {
		HashMap<Day, ArrayList<TrainPlanning>> trainPlanning = new HashMap<Day, ArrayList<TrainPlanning>>( );
		long timestamp;
		Random r = new Random();
		timestamp = r.nextLong();
		if (timestamp < 0)
			timestamp *=(-1);
		ArrayList<TrainPlanning> trainMonday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainTuesday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainWednesday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainThursday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainFriday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainSaturday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainSunday = new ArrayList<TrainPlanning>();
		
		TrainPlanning train;
			for(int i = 0; i <=50; i++)
			{
				train = new TrainPlanning(0, 0, null, null, null, Day.MONDAY, timestamp);
				trainMonday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.TUESDAY, timestamp);
				trainTuesday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.WEDNESDAY, timestamp);
				trainWednesday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.THURSDAY, timestamp);
				trainThursday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.FRIDAY, timestamp);
				trainFriday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.SATURDAY, timestamp);
				trainSaturday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.SUNDAY, timestamp);
				trainSunday.add(train);
				timestamp = r.nextLong();
				if (timestamp < 0)
					timestamp *=(-1);
			}
		
		trainPlanning.put(Day.MONDAY, trainMonday);
		trainPlanning.put(Day.TUESDAY, trainTuesday);
		trainPlanning.put(Day.WEDNESDAY, trainWednesday);
		trainPlanning.put(Day.THURSDAY, trainThursday);
		trainPlanning.put(Day.FRIDAY, trainFriday);
		trainPlanning.put(Day.SATURDAY, trainSaturday);
		trainPlanning.put(Day.SUNDAY, trainSunday);
		
		for( Day day : Day.values( ) )
			for( TrainPlanning planning : trainPlanning.get( day ) )
			{
				planning.reset( );
				assertFalse(planning.isGone());
			}
				
	}

	@Test
	/**
	 * Test for the method cancelPreviousTrain()
	 */
	public void testCancelPreviousTrain() {
		HashMap<Day, ArrayList<TrainPlanning>> trainPlanning = new HashMap<Day, ArrayList<TrainPlanning>>( );
		long timestamp;
		Random r = new Random();
		timestamp = r.nextLong();
		if (timestamp < 0)
			timestamp *=(-1);
		ArrayList<TrainPlanning> trainMonday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainTuesday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainWednesday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainThursday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainFriday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainSaturday = new ArrayList<TrainPlanning>();
		ArrayList<TrainPlanning> trainSunday = new ArrayList<TrainPlanning>();
		
		TrainPlanning train;
			for(int i = 0; i <=50; i++)
			{
				train = new TrainPlanning(0, 0, null, null, null, Day.MONDAY, timestamp);
				trainMonday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.TUESDAY, timestamp);
				trainTuesday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.WEDNESDAY, timestamp);
				trainWednesday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.THURSDAY, timestamp);
				trainThursday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.FRIDAY, timestamp);
				trainFriday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.SATURDAY, timestamp);
				trainSaturday.add(train);
				train = new TrainPlanning(0, 0, null, null, null, Day.SUNDAY, timestamp);
				trainSunday.add(train);
				timestamp = r.nextLong();
				if (timestamp < 0)
					timestamp *=(-1);
			}
		
		trainPlanning.put(Day.MONDAY, trainMonday);
		trainPlanning.put(Day.TUESDAY, trainTuesday);
		trainPlanning.put(Day.WEDNESDAY, trainWednesday);
		trainPlanning.put(Day.THURSDAY, trainThursday);
		trainPlanning.put(Day.FRIDAY, trainFriday);
		trainPlanning.put(Day.SATURDAY, trainSaturday);
		trainPlanning.put(Day.SUNDAY, trainSunday);
		
		for( Day day : Day.values( ) )
			for( TrainPlanning planning : trainPlanning.get( day ) )
			{
				
				// Departure timestamp before timestamp
				if( planning.getDepartureTimestamp( ) < timestamp * 1000 )
				{
					// Cancel train
					planning.leave( );
					assertTrue(planning.isGone());
				}
			}
	}

}
