/**
 * 
 */
package railnrail.test.network.section;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;

import railnrail.network.section.Station;
import railnrail.network.section.rail.RailDirection;
import railnrail.train.Train;
import railnrail.train.prevision.ArrivalPrevision;

/**
 * @author marilou
 *
 */
public class StationTest {

	/**
	 * Test method for {@link railnrail.network.section.Station#addArrivalPrevision(railnrail.train.Train, railnrail.train.prevision.ArrivalPrevision)}.
	 */
	@Test
	public void testAddArrivalPrevision() {
		String travelCode = "test";
		int id = 42;
		boolean isTerminus = false;
		Random r = new Random();
		Station c = new Station(null,null,0,travelCode,id,isTerminus,null,null,r.nextInt(),null);
		RailDirection direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
		Train train = new Train(null, travelCode, null, direction, 0, id);
		ArrivalPrevision prevision = new ArrivalPrevision(c, train,0,0,0);

		c.addArrivalPrevision(train, prevision);
		assertEquals(travelCode,c.getPrevision()[0].getTrain().getTravelCode());
		assertEquals(id,c.getPrevision()[0].getTrain().getTrainId());
		assertEquals(direction,c.getPrevision()[0].getTrain().getDirection());
	}

	/**
	 * Test method for {@link railnrail.network.section.Station#getPrevision()}.
	 */


}
