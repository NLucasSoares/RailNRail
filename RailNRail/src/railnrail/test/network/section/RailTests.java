package railnrail.test.network.section;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.network.section.rail.RailConnectionTest;
import railnrail.test.network.section.rail.RailTest;

@RunWith(Suite.class)
@SuiteClasses({ RailConnectionTest.class, RailTest.class })
/**
 *  TestSuite of the package network.section.rail
 * @author marilou
 *
 */
public class RailTests {

}
