package railnrail.test.network;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.network.section.RailTests;
import railnrail.test.network.section.SectionTest;

@RunWith(Suite.class)
@SuiteClasses({ RailTests.class, SectionTest.class })
/**
 * TestSuite of the package network.section
 * @author marilou
 *
 */
public class SectionTests {

}
