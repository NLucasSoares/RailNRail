package railnrail.test.network.subdivision;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import railnrail.network.subdivision.SubdivisionLimit;

/**
 * 
 * @author marilou
 * TestCase for the class SubdivisionLimit of the package network.subdivision
 * 
 */
public class SubdivisionLimitTest {

	@Test
	/**
	 * test for the constructor SubdivisionLimit()
	 */
	public void testSubdivisionLimit() {
		int begin;
		int end;
		//SubdivisionLimit sl1 = new SubdivisionLimit(begin,end);
		//assertNotNull(sl1.getBeginID());
		//assertNotNull(sl1.getEndID());
		//assertEquals(begin, sl1.getBeginID());
		//assertEquals(end, sl1.getEndID());
		Random r = new Random();
		begin = r.nextInt();
		end = r.nextInt();
		SubdivisionLimit sl2 = new SubdivisionLimit(begin,end);
		assertNotNull(sl2.getBeginID());
		assertNotNull(sl2.getEndID());
		assertEquals(begin, sl2.getBeginID());
		assertEquals(end, sl2.getEndID());
	}

	@Test
	/**
	 * test for the method getBeginID()
	 */
	public void testGetBeginID() {
		Random r = new Random();
		int begin = r.nextInt();
		int end = r.nextInt();
		SubdivisionLimit sl = new SubdivisionLimit(begin, end);
		assertEquals(begin, sl.getBeginID());
	}

	@Test
	/**
	 * test for the method getEndID();
	 */
	public void testGetEndID() {
		Random r = new Random();
		int begin = r.nextInt();
		int end = r.nextInt();
		SubdivisionLimit sl = new SubdivisionLimit(begin, end);
		assertEquals(end, sl.getEndID());
	}

}
