package railnrail.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.file.HelperTest;

@RunWith(Suite.class)
@SuiteClasses({ HelperTest.class })
/**
 * TestSuite for the package file
 * @author marilou
 *
 */
public class FileTests {

}
