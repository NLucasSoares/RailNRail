package railnrail.test;

/**
 * Test failed exception
 * 
 * @author Lucas SOARES
 */
public class TestFailedException extends Exception
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -7803422908586986395L;
	
}
