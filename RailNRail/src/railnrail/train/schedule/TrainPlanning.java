package railnrail.train.schedule;

import railnrail.simulation.time.CurrentTime;
import railnrail.simulation.time.Day;

/**
 * Train planning
 * 
 * @author Lucas SOARES
 */
public class TrainPlanning
{
	/**
	 * Time when train leaves station
	 */
	private int hourToLeave;
	private int minuteToLeave;
	
	/**
	 * Day when train is plannified
	 */
	private Day dayToLeave;
	
	/**
	 * Departure timestamp
	 */
	private long departureTimestamp;
	
	/**
	 * Station from where to leave
	 */
	private String stationName;
	
	/**
	 * Destination
	 */
	private String destination;
	
	/**
	 * Travel code
	 */
	private String travelCode;
	
	/**
	 * Is gone
	 */
	private boolean isGone;
	
	/**
	 * Construct train planning
	 * 
	 * @param hourToLeave
	 * 		The hour when train leaves
	 * @param minuteToLeave
	 * 		The minute when train leaves
	 * @param stationName
 	 *		The name of the station
 	 * @param direction
 	 * 		The direction
 	 * @param destination
 	 * 		The destination
 	 * @param travelCode
 	 * 		The travel code of this train
 	 * @param initialTimestamp
 	 * 		The initial timestamp of the simulation
	 */
	public TrainPlanning( int hourToLeave,
			int minuteToLeave,
			String stationName,
			String destination,
			String travelCode,
			Day day,
			long initialTimestamp )
	{
		// Save
		this.hourToLeave = hourToLeave;
		this.minuteToLeave = minuteToLeave;
		this.stationName = stationName;
		this.destination = destination;
		this.travelCode = travelCode;
		this.dayToLeave = day;
		
		// Calculate departure timestamp
		departureTimestamp = calculateDepartureTimestamp( initialTimestamp );

		// Zero
		isGone = false;
	}

	
	/**
	 * @return the hour to leave
	 */
	public int getHourToLeave( )
	{
		return hourToLeave;
	}

	/**
	 * @return the minute to leave
	 */
	public int getMinuteToLeave( )
	{
		return minuteToLeave;
	}

	/**
	 * @return the station name
	 */
	public String getDepartureStation( )
	{
		return stationName;
	}

	/**
	 * @return the travel code
	 */
	public String getTravelCode( )
	{
		return travelCode;
	}
	
	/**
	 * @return the destination
	 */
	public String getDestination( )
	{
		return destination;
	}

	/**
	 * @return the day to leave
	 */
	public Day getDayToLeave( )
	{
		return dayToLeave;
	}
	
	/**
	 * Calculate the timestamp for departure
	 * 
	 * @param initialTimestamp
	 * 		The initial timestamp
	 * 
	 * @return the timestamp when to leave
	 */
	private long calculateDepartureTimestamp( long initialTimestamp )
	{
		return ( initialTimestamp
				+ ( dayToLeave.ordinal( ) * CurrentTime.SECONDS_IN_DAY )
				+ hourToLeave * CurrentTime.SECONDS_IN_HOUR
				+ minuteToLeave * CurrentTime.SECONDS_IN_MINUTE ) * 1000;
	}
	
	/**
	 * Get departure timestamp
	 * 
	 * @return the departure timestamp
	 */
	public long getDepartureTimestamp( )
	{
		return departureTimestamp;
	}
	
	/**
	 * Is train gone?
	 * 
	 * @return if train is gone
	 */
	public boolean isGone( )
	{
		return isGone;
	}
	
	/**
	 * Train leaves
	 */
	public void leave( )
	{
		// Train is now gone
		isGone = true;
	}
	
	/**
	 * Reset train state
	 */
	public void reset( )
	{
		// Train reset
		isGone = false;
	}
}
